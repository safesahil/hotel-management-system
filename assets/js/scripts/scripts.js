$(document).on("click", "#delete", function (e) {
    var data_path = $(document).find(this).attr('data-path');
    $(document).find('.yes_i_want_delete').removeClass("btn-success");
    $(document).find('.yes_i_want_delete').addClass("btn-danger");
    $(document).find("#deleteConfirm").modal('show');
    $(document).on("click", ".yes_i_want_delete", function (e) {
        var val = $(this).val();
        if (val === 'yes') {
            jQuery('#form').attr('action', data_path).submit();
        }
    });
});

$(document).on("click", "#restore", function (e) {
    var data_path = $(document).find(this).attr('data-path');
    $(document).find("#restoreConfirm").modal('show');
    $(document).on("click", ".yes_restore", function (e) {
        var val = $(this).val();
        if (val === '1') {
            jQuery('#form').attr('action', data_path).submit();
        }
    });
});

$(document).on('click', '.delete_record', function () {
    var delete_url = $(document).find(this).attr('data-path');
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#FF7043",
        confirmButtonText: "Yes, delete it!"
    }, function () {
        jQuery('#form').attr('action', delete_url).submit();
    });
});

$(document).on('click', '.recover_record', function () {
    var recover_url = $(document).find(this).attr('data-path');
    swal({
        title: "Are you sure?",
        text: "Your data will be restored!",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#66bb6a",
        confirmButtonText: "Yes, restore it!"
    }, function () {
        jQuery('#form').attr('action', recover_url).submit();
    });
});

$(document).on('mouseenter', '.tooltip-show', function () {
    $(this).tooltip('show');
});

function enableControl(selector) {
    $(document).find(selector).attr('disabled', false);
}

function disableControl(selector) {
    $(document).find(selector).attr('disabled', true);
}

function generateSelec2Dropdown(selector, data, defaultLabel, defaultValue, selectedValue) {
    var _selectedValue = selectedValue;
    if (!_selectedValue) {
        _selectedValue = defaultValue;
    }
    $(document).find(selector + ' option').remove();
    $(document).find(selector)
            .append($("<option value='" + defaultValue + "'>" + defaultLabel + "</option>"));
    for (var k in data) {
        var element = data[k];
        $(document).find(selector)
                .append($("<option value='" + k + "'>" + element + "</option>"));
    }
    $(document).find(selector).val(_selectedValue).trigger('change.select2');
}
function cleanSelec2Dropdown(selector, defaultLabel, defaultValue) {
    var _defaultValue = defaultValue;
    if (!_defaultValue) {
        _defaultValue = '';
    }
    $(document).find(selector + ' option').remove();
    $(document).find(selector)
            .append($("<option value='" + _defaultValue + "'>" + defaultLabel + "</option>"));
    $(document).find(selector).val(_defaultValue).trigger('change.select2');
}

function ajaxGet(url) {
    var promise = new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            method: 'GET',
            success: function (response) {
                resolve(response);
            },
            error: function (error) {
                reject(error);
            }
        });
    });
    return promise;
}

$(function () {
    $('.daterange-basic-datatable').daterangepicker({
        ranges: {
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last 3 Months': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Last 12 Months': [moment().subtract(12, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        applyClass: 'bg-slate-600',
        cancelClass: 'btn-default',
        showDropdowns: true,
        autoUpdateInput: false,
        locale: {
            format: 'MM/DD/YYYY'
        }
    });

    $('.daterange-basic-datatable').on('apply.daterangepicker', function (ev, picker) {
        $(this)
                .val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'))
                .trigger('change');
    });

    $('.daterange-basic-datatable').on('cancel.daterangepicker', function (ev, picker) {
        $(this)
                .val('')
                .trigger('change');
    });

    $('.daterange-single-basic').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'MM/DD/YYYY'
        }
    });

    $('.select2-basic').select2({minimumResultsForSearch: Infinity});
    $('.select2-search').select2();
});

$('.file-input').fileinput({
    browseLabel: 'Select',
    removeLabel: '',
    browseIcon: '<i class="icon-file-plus"></i>',
    removeIcon: '<i class="icon-cross3"></i>',
    initialCaption: "No file selected",
    allowedFileExtensions: ["jpg", "jpeg", "png"],
});

function get_dd_mm_yyyy_Date(date, seperator) {
    if (date === null) {
        return '';
    }
    // date is split to get only date from date string
    date = date.split(' ')[0];
    date = new Date(date);
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return day + seperator + month + seperator + year;
}

function get_yyyy_mm_dd_Date(date, seperator) {
    if (date === null) {
        return '';
    }
    // date is split to get only date from date string
    date = date.split(' ')[0];
    date = new Date(date);
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return year + seperator + month + seperator + day;
}

function get_mm_dd_yyyy_Date(date, seperator) {
    if (date === null) {
        return '';
    }
    // date is split to get only date from date string
    date = date.split(' ')[0];
    date = new Date(date);
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + seperator + day + seperator + year;
}
function number_format_js_amount(amount, decimals, dec_point, thousands_sep) {
    amount = parseFloat(amount);
    amount = amount.toFixed(decimals);
    var nstr = amount.toString();
    nstr += '';
    x = nstr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? dec_point + x[1] : '';
    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + thousands_sep + '$2');

    return x1 + x2;
}