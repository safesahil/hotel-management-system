<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Base_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function get($raw_data) {
        $resource = $this->_query_builder($raw_data);
        return $resource->result_array();
    }

    public function get_one($raw_data) {
        $resource = $this->_query_builder($raw_data);
        return $resource->row_array();
    }

    public function count($raw_data) {
        $resource = $this->_query_builder($raw_data);
        return count($resource->result_array());
    }

    function _query_builder($raw_data) {
        // Select Fields
        if (isset($raw_data['fields']) && !empty($raw_data['fields'])) {
            $this->db->select($raw_data['fields']);
        }
        
        // Where array
        if (isset($raw_data['where']) && !empty($raw_data['where'])) {
            $this->db->where($raw_data['where']);
        }

        // Where array with operators
        if (isset($raw_data['where_with_operators']) && !empty($raw_data['where_with_operators'])) {
            $this->db->where($raw_data['where_with_operators']);
        }

        // Where string
        if (isset($raw_data['where_string']) && !empty($raw_data['where_string'])) {
            $this->db->where($raw_data['where_array']);
        }

        // OR Where array
        if (isset($raw_data['or_where']) && !empty($raw_data['or_where'])) {
            $this->db->or_where($raw_data['or_where']);
        }

        // OR Where array with operators
        if (isset($raw_data['or_where_with_operators']) && !empty($raw_data['or_where_with_operators'])) {
            $this->db->or_where($raw_data['or_where_with_operators']);
        }

        // OR Where string
        if (isset($raw_data['or_where_string']) && !empty($raw_data['or_where_string'])) {
            $this->db->or_where($raw_data['or_where_string']);
        }

        // Where in array
        if (isset($raw_data['where_in']) && !empty($raw_data['where_in'])) {
            foreach ($raw_data['where_in'] as $column => $values) {
                $this->db->where_in($column, $values);
            }
        }

        // OR Where in array
        if (isset($raw_data['or_where_in']) && !empty($raw_data['or_where_in'])) {
            foreach ($raw_data['or_where_in'] as $column => $values) {
                $this->db->or_where_in($column, $values);
            }
        }

        // Where not in array
        if (isset($raw_data['where_not_in']) && !empty($raw_data['where_not_in'])) {
            foreach ($raw_data['where_not_in'] as $column => $values) {
                $this->db->where_not_in($column, $values);
            }
        }

        // OR Where not in array
        if (isset($raw_data['or_where_not_in']) && !empty($raw_data['or_where_not_in'])) {
            foreach ($raw_data['or_where_not_in'] as $column => $values) {
                $this->db->or_where_not_in($column, $values);
            }
        }

        // Like array
        if (isset($raw_data['like']) && !empty($raw_data['like'])) {
            $this->db->like($raw_data['like']);
        }

        // Like with wildcard place array
        if (isset($raw_data['like_with_wildcard_place']) && !empty($raw_data['like_with_wildcard_place'])) {
            foreach ($raw_data['like_with_wildcard_place'] as $condition) {
                if (isset($condition['column']) && isset($condition['value']) && !empty($condition['column']) && !empty($condition['value'])) {
                    $card_place = DEFAULT_QUERY_LIKE_WILDCARD_PLACE;
                    if (isset($condition['card_place']) && !empty($condition['card_place'])) {
                        $card_place = $condition['card_place'];
                    }
                    $this->db->like($condition['column'], $condition['value'], $card_place);
                }
            }
        }

        // OR Like array
        if (isset($raw_data['or_like']) && !empty($raw_data['or_like'])) {
            $this->db->or_like($raw_data['or_like']);
        }

        // OR Like with wildcard place array
        if (isset($raw_data['or_like_with_wildcard_place']) && !empty($raw_data['or_like_with_wildcard_place'])) {
            foreach ($raw_data['or_like_with_wildcard_place'] as $condition) {
                if (isset($condition['column']) && isset($condition['value']) && !empty($condition['column']) && !empty($condition['value'])) {
                    $card_place = DEFAULT_QUERY_LIKE_WILDCARD_PLACE;
                    if (isset($condition['card_place']) && !empty($condition['card_place'])) {
                        $card_place = $condition['card_place'];
                    }
                    $this->db->or_like($condition['column'], $condition['value'], $card_place);
                }
            }
        }

        // Not Like array
        if (isset($raw_data['not_like']) && !empty($raw_data['not_like'])) {
            $this->db->not_like($raw_data['not_like']);
        }

        // Not Like with wildcard place array
        if (isset($raw_data['not_like_with_wildcard_place']) && !empty($raw_data['not_like_with_wildcard_place'])) {
            foreach ($raw_data['not_like_with_wildcard_place'] as $condition) {
                if (isset($condition['column']) && isset($condition['value']) && !empty($condition['column']) && !empty($condition['value'])) {
                    $card_place = DEFAULT_QUERY_LIKE_WILDCARD_PLACE;
                    if (isset($condition['card_place']) && !empty($condition['card_place'])) {
                        $card_place = $condition['card_place'];
                    }
                    $this->db->not_like($condition['column'], $condition['value'], $card_place);
                }
            }
        }

        // OR not Like array
        if (isset($raw_data['or_not_like']) && !empty($raw_data['or_not_like'])) {
            $this->db->or_not_like($raw_data['or_not_like']);
        }

        // OR not Like with wildcard place array
        if (isset($raw_data['or_not_like_with_wildcard_place']) && !empty($raw_data['or_not_like_with_wildcard_place'])) {
            foreach ($raw_data['or_not_like_with_wildcard_place'] as $condition) {
                if (isset($condition['column']) && isset($condition['value']) && !empty($condition['column']) && !empty($condition['value'])) {
                    $card_place = DEFAULT_QUERY_LIKE_WILDCARD_PLACE;
                    if (isset($condition['card_place']) && !empty($condition['card_place'])) {
                        $card_place = $condition['card_place'];
                    }
                    $this->db->or_not_like($condition['column'], $condition['value'], $card_place);
                }
            }
        }

        // Having array
        if (isset($raw_data['having']) && !empty($raw_data['having'])) {
            foreach ($raw_data['having'] as $column => $value) {
                $this->db->having($column, $value);
            }
        }

        // Having with operands array
        if (isset($raw_data['having_with_operands']) && !empty($raw_data['having_with_operands'])) {
            $this->db->having($raw_data['having_with_operands']);
        }

        // Having string array
        if (isset($raw_data['having_string']) && !empty($raw_data['having_string'])) {
            foreach ($raw_data['having_string'] as $condition) {
                $this->db->having($condition);
            }
        }

        // OR Having array
        if (isset($raw_data['or_having']) && !empty($raw_data['or_having'])) {
            foreach ($raw_data['or_having'] as $column => $value) {
                $this->db->or_having($column, $value);
            }
        }

        // OR Having with operands array
        if (isset($raw_data['or_having_with_operands']) && !empty($raw_data['or_having_with_operands'])) {
            $this->db->or_having($raw_data['or_having_with_operands']);
        }

        // OR Having string array
        if (isset($raw_data['or_having_string']) && !empty($raw_data['or_having_string'])) {
            foreach ($raw_data['or_having_string'] as $condition) {
                $this->db->or_having($condition);
            }
        }

        // Order by
        if (isset($raw_data['order_by']) && !empty($raw_data['order_by'])) {
            foreach ($raw_data['order_by'] as $column => $value) {
                $this->db->order_by($column, $value);
            }
        }

        // Group by
        if (isset($raw_data['group_by']) && !empty($raw_data['group_by'])) {
            $this->db->group_by($raw_data['group_by']);
        }

        // Limit and start
        if (isset($raw_data['limit']) && !empty($raw_data['limit'])) {
            if (isset($raw_data['start']) && !empty($raw_data['start'])) {
                $this->db->limit($raw_data['limit'], $raw_data['start']);
            } else {
                $this->db->limit($raw_data['limit']);
            }
        }

        // Join
        if (isset($raw_data['join']) && !empty($raw_data['join'])) {
            foreach ($raw_data['join'] as $join) {
                if (isset($join['table']) && isset($join['condition']) && !empty($join['table']) && !empty($join['condition'])) {
                    $join_type = 'left';
                    if (isset($join['type']) && !empty($join['type'])) {
                        $join_type = $join['type'];
                    }
                    $this->db->join($join['table'], $join['condition'], $join_type);
                }
            }
        }

        $resource = $this->db->get($raw_data['table']);
        return $resource;
    }

    public function insert($table, $data) {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function insert_batch($table, $data) {
        $this->db->insert_batch($table, $data);
        return $this->db->insert_id();
    }

    public function update($table, $data, $where) {
        $this->db->update($table, $data, $where);
        return $this->db->affected_rows();
    }

    public function update_batch($table, $data, $where_key) {
        $this->db->update_batch($table, $data, $where_key);
        return $this->db->affected_rows();
    }

    public function delete($table, $where) {
        $this->db->delete($table, $where);
        return $this->db->affected_rows();
    }
    
    /**
     * This method is used to get filter records for data tables
     * 
     * @added by sb
     * @on 07-04-2017
     * 
     * @param string $table_name
     * @param array $filter_array array containing filter records
     * @param int $do_count if we want to count records
     * @return result
     */
    function get_filtered_records($table_name, $filter_array, $do_count = 0) {
        if (isset($filter_array['select'])) {
            $this->db->select($table_name . '.id,' . $filter_array['select']);
        }
        $length = $start = 0;

        if (count($filter_array) > 0) {
            $length = (isset($filter_array['length']) && $filter_array['length'] != '') ? $filter_array['length'] : 0;
            $start = (isset($filter_array['start']) && $filter_array['start'] != '') ? $filter_array['start'] : 0;
            $eq_cols = (isset($filter_array['col_eq']) && !empty($filter_array['col_eq'])) ? $filter_array['col_eq'] : array();

            if (isset($filter_array['column_search']) && !empty($filter_array['column_search'])) {
                foreach ($filter_array['column_search'] as $column => $value) {
                    $is_like_col = 1;
                    if (in_array($column, $eq_cols)) {
                        $is_like_col = 0;
                    }
                    if (strpos($column, '|') > -1) {
                        $cols = explode('|', $column);
                        $cols_count = count($cols);
                        foreach ($cols as $col_index => $col) {
                            if ($col_index === 0) {
                                $this->db->group_start();
                                ($is_like_col) ? $this->db->like($col, $value) : $this->db->where($col, $value);
                            } else {
                                ($is_like_col) ? $this->db->or_like($col, $value) : $this->db->or_where($col, $value);
                            }
                            if ($cols_count - 1 == $col_index) {
                                $this->db->group_end();
                            }
                        }
                    } else {
                        ($is_like_col) ? $this->db->like($column, $value) : $this->db->where($column, $value);
                    }
                }
            }

            if (isset($filter_array['column_date_ranges']) && !empty($filter_array['column_date_ranges'])) {
                foreach ($filter_array['column_date_ranges'] as $range_data) {
                    if (!empty($range_data['column']) && !empty($range_data['start_date']) && !empty($range_data['end_date'])) {
                        $date_range_where_str = $range_data['column'] . ' BETWEEN \'' . $range_data['start_date'] . '\' AND \'' . $range_data['end_date'] . '\'';
                        $this->db->where($date_range_where_str);
                    }
                }
            }

            if (isset($filter_array['search']) && $filter_array['search'] != '') {
                $searchable_columns_count = count($filter_array['searchable_columns']);

                foreach ($filter_array['searchable_columns'] as $col_index => $column) {
                    if ($col_index === 0) {
                        $this->db->group_start();
                        $this->db->like($column, $filter_array['search']);
                    } else {
                        $this->db->or_like($column, $filter_array['search']);
                    }
                    if ($searchable_columns_count - 1 == $col_index) {
                        $this->db->group_end();
                    }
                }
            }

            if (isset($filter_array['order']) && !empty($filter_array['order'])) {
                foreach ($filter_array['order'] as $column => $value) {
                    if (strpos($column, '|') > -1) {
                        $column_arr = explode('|', $column);
                        foreach ($column_arr as $order_column) {
                            $this->db->order_by($order_column, $value);
                        }
                    } else {
                        $this->db->order_by($column, $value);
                    }
                }
            }

            if (isset($filter_array['custom_or_where']) && !empty($filter_array['custom_or_where'])) {

                foreach ($filter_array['custom_or_where'] as $column_name => $values) {
                    $values_count = count($values);
                    if ($values_count == 1) {
                        $this->db->where($column_name, $values[0]);
                    } else if ($values_count > 1) {
                        foreach ($values as $index => $value) {
                            if ($index === 0) {
                                $this->db->group_start();
                                $this->db->where($column_name, $value);
                            } else {
                                $this->db->or_where($column_name, $value);
                            }
                            if ($values_count - 1 == $index) {
                                $this->db->group_end();
                            }
                        }
                    }
                }
            }

            if (isset($filter_array['join']) && !empty($filter_array['join'])) {
                foreach ($filter_array['join'] as $index => $join_values) {
                    if (isset($join_values['select_columns'])) {
                        $this->db->select($join_values['select_columns']);
                    }
                    $join_values['join_type'] = isset($join_values['join_type']) ? $join_values['join_type'] : 'left';
                    $this->db->join($join_values['table'], $join_values['condition'], $join_values['join_type']);
                }
            }

            if (isset($filter_array['where']) && !empty($filter_array['where'])) {
                $condition = $filter_array['where'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->where($key, $wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['or_where']) && !empty($filter_array['or_where'])) {
                $condition = $filter_array['or_where'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->or_where($key, $wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['where_with_sign']) && !empty($filter_array['where_with_sign'])) {
                $condition = $filter_array['where_with_sign'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->where($wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['or_where_with_sign']) && !empty($filter_array['or_where_with_sign'])) {
                $condition = $filter_array['or_where_with_sign'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->or_where($wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['like']) && !empty($filter_array['like'])) {
                $condition = $filter_array['like'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->like($key, $wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['like_with_sign']) && !empty($filter_array['like_with_sign'])) {
                $condition = $filter_array['like_with_sign'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->where($wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['or_like']) && !empty($filter_array['or_like'])) {
                $condition = $filter_array['or_like'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->or_like($key, $wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['or_like_with_sign']) && !empty($filter_array['or_like_with_sign'])) {
                $condition = $filter_array['or_like_with_sign'];
                $this->db->group_start();
                foreach ($condition as $key => $wh) {
                    $this->db->or_like($wh);
                }
                $this->db->group_end();
            }

            if (isset($filter_array['fields']) && !empty($filter_array['fields'])) {
                $this->db->select($filter_array['fields'], FALSE);
            }

            if (isset($filter_array['group_by']) && !empty($filter_array['group_by'])) {
                $this->db->group_by($filter_array['group_by']);
            }
        }

        if ($do_count) {
            $query = $this->db->get($table_name);
            return count($query->result_array());
        } else {
            if ($length != 0) {
                $this->db->limit($filter_array['length'], $filter_array['start']);
            }
            $query = $this->db->get($table_name);
//            echo $this->db->last_query();die;
            return $query->result_array();
        }
    }
    function get_bill_no() {
        $this->db->select('bill_no');
        $this->db->from(TBL_TRANSACTIONS);
        $this->db->order_by('bill_no', 'desc');
        $query = $this->db->get();
        if(empty($query->row())){
            $qry=0;
        }
        else{
            $qry = $query->row()->bill_no;
        }
        $qry1 = $qry + 1;
        return $qry1;
    }

}
