<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="restaurants_items/save/<?php echo (isset($prev_data) && isset($prev_data['id'])) ? $prev_data['id'] : '' ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Item category <span class="text-danger">*</span></label>
                                <?php
                                $i_c_options = array('' => 'Select category');
                                if (isset($item_categories) && count($item_categories) > 0) {
                                    $i_c_options = $item_categories;
                                }
                                $selected_restaurants_item_category = (isset($prev_data) && isset($prev_data['category_id'])) ? $prev_data['category_id'] : set_value('restaurants_item_category');
                                $attributes = array('id' => 'restaurants_item_category', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('restaurants_item_category', $i_c_options, $selected_restaurants_item_category, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Name <span class="text-danger">*</span></label>
                                <input
                                    id="category_name"
                                    name="item_name"
                                    type="text"
                                    class="form-control"
                                    placeholder="Name"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['name'])) ? $prev_data['name'] : set_value('item_name') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Sale price <span class="text-danger">*</span></label>
                                <input
                                    id="sale_price"
                                    name="sale_price"
                                    type="text"
                                    class="form-control"
                                    placeholder="Sale price"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['sale_price'])) ? $prev_data['sale_price'] : set_value('sale_price') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Unit category <span class="text-danger">*</span></label>
                                <?php
                                $u_c_options = array('' => 'Select unit category');
                                if (isset($unit_category_options) && count($unit_category_options) > 0) {
                                    $u_c_options = $unit_category_options;
                                }
                                $selected_unit_category = (isset($prev_data) && isset($prev_data['unit_category'])) ? $prev_data['unit_category'] : set_value('unit_category');
                                $attributes = array('id' => 'unit_category', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('unit_category', $u_c_options, $selected_unit_category, $attributes); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <?php
                                $status_options = array('1' => 'Active', '0' => 'Inactive');
                                $selected_status = (isset($prev_data) && isset($prev_data['status'])) ? $prev_data['status'] : set_value('status');
                                $attributes = array('id' => 'status', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="restaurants_items" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            item_name: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            restaurants_item_category: {
                required: true
            },
            sale_price: {
                required: true,
                number: true,
                min: 0,
                max: 1000000,
            },
            unit_category: {
                required: true
            },
            status: {
                required: true
            }
        };
        initValidation('.save-form', rules);
    });
</script>