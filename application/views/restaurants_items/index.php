<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <a href="restaurants_items" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="restaurants_items/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add item</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Sale</th>
                                    <th>Category</th>
                                    <th>Unit Category</th>
                                    <th>Status</th>
                                    <th>Deleted ?</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                                <tr>
                                    <th>Date Added</th>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Sale</th>
                                    <th>Category</th>
                                    <th>Unit Category</th>
                                    <th>Status</th>
                                    <th>Deleted ?</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var statusOptions = {1: "Active", 0: "Inactive"};
        var isDeleted = {1: "Yes", 0: "No"};
        var unitCatOptions = JSON.parse('<?php echo $unit_category_options ?>');

        // Setup - add a text input to each footer cell
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions') {
                if (title === 'Date Added') {
                    $(this).html('<input type="text" class="form-control daterange-basic-datatable" placeholder="' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                }
            }
        });
         var d = new Date();
        //datatables
        var table = $('#dttable').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                    title: "Restaurants items Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3, 4,5,6]
                    },
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                    title: "Restaurants items Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3, 4,5,6]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                    title: "Restaurants items Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3, 4,5,6]
                    },
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                    title: "Restaurants items Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3, 4,5,6]
                    },

                }

            ],
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            columns: [
                {
                    data: 'restaurants_items_created_date',
                    visible: true,
                    name: 'restaurants_items.created_date',
                    render: function (data, type, full, meta) {
                        return get_mm_dd_yyyy_Date(full.restaurants_items_created_date, '/');
                    }
                },
                {
                    data: 'restaurants_items_id',
                    visible: true,
                    name: 'restaurants_items.id',
                },
                {
                    data: 'restaurants_items_name',
                    visible: true,
                    name: 'restaurants_items.name',
                },
                {
                    data: 'restaurants_items_sale_price',
                    visible: true,
                    name: 'restaurants_items.sale_price',
                    render: function (data, type, full, meta) {
                        if (typeof full.restaurants_items_sale_price !== 'undefined') {
                            return  '$ ' + full.restaurants_items_sale_price;
                        }
                        return '-----';
                    }
                },
                {
                    data: 'restaurants_items_category_name',
                    visible: true,
                    name: 'restaurants_items_category.name',
                },
                {
                    data: 'restaurants_items_unit_category',
                    visible: true,
                    name: 'restaurants_items.unit_category',
                    render: function (data, type, full, meta) {
                        if (unitCatOptions.hasOwnProperty(full.restaurants_items_unit_category)) {
                            return unitCatOptions[full.restaurants_items_unit_category];
                        }
                        return '-----';
                    }
                },
                {
                    data: 'restaurants_items_status',
                    visible: true,
                    name: 'restaurants_items.status',
                    render: function (data, type, full, meta) {
                        var status = 'Unknown';
                        if (full.restaurants_items_status === '0') {
                            status = '<span class="label label-warning label-rounded"> Inactive</span>';
                        } else if (full.restaurants_items_status === '1') {
                            status = '<span class="label label-success label-rounded"> Active</span>';
                        }
                        return status;
                    }
                },
                {
                    data: 'restaurants_items_is_deleted',
                    visible: true,
                    name: 'restaurants_items.is_deleted',
                    render: function (data, type, full, meta) {
                        var status = "";
                        if (full.restaurants_items_is_deleted === '1') {
                            status = '<span class="label label-danger label-rounded"> Yes</span>';
                        } else {
                            status = '<span class="label label-success label-rounded"> No</span>';
                        }
                        return status;
                    }
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        actionBtns = '<a href="restaurants_items/save/' + full.id + '" class="btn btn-primary btn-rounded btn-sm action-btns tooltip-show" title="Edit"><i class="fa fa-pencil"></i></a>';
                        if (full.restaurants_items_is_deleted === '1') {
                            actionBtns += '<a href="javascript:void(0)" class="recover_record btn bg-teal btn-rounded btn-sm action-btns tooltip-show" title="Recover" data-path="restaurants_items/recover/' + full.id + '"><i class="fa fa-undo"></i></a>';
                        } else {
                            actionBtns += '<a href="javascript:void(0)" class="delete_record btn btn-danger btn-rounded btn-sm action-btns tooltip-show" title="Delete" data-path="restaurants_items/delete/' + full.id + '"><i class="fa fa-trash"></i></a>';
                        }
                        return actionBtns;
                    }
                }
            ],
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
                this.api().columns().every(function (index) {
                    if (tableColumns[index].name == 'restaurants_items.status' || tableColumns[index].name == 'restaurants_items.is_deleted' || tableColumns[index].name == 'restaurants_items.unit_category') {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">Select</option></select>')
                                .appendTo($('th:nth-child(' + (index + 1) + '):first').empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? val : '', true, false)
                                            .draw();
                                });
                        if (tableColumns[index].name == 'restaurants_items.status') {
                            for (var key in statusOptions) {
                                if (statusOptions.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + statusOptions[key] + '</option>');
                                }
                            }
                        } else if (tableColumns[index].name == 'restaurants_items.is_deleted') {
                            for (var key in isDeleted) {
                                if (isDeleted.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + isDeleted[key] + '</option>');
                                }
                            }
                        } else if (tableColumns[index].name == 'restaurants_items.unit_category') {
                            for (var key in unitCatOptions) {
                                if (unitCatOptions.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + unitCatOptions[key] + '</option>');
                                }
                            }
                        }
                    }
                });
            },
            fnServerData: function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['col_eq'] = ['restaurants_items.status', 'restaurants_items.is_deleted', 'restaurants_items.unit_category'];
                req_obj['datatable_date_range'] = [
                    {'column': 'restaurants_items.created_date', 'filter_format': 'Y-m-d', 'range_deliminator': '-'}
                ];
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: "<?php echo base_url() . 'restaurants_items/filter' ?>",
                    data: req_obj,
                    success: function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>