<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="stores_return/save/<?php echo (isset($prev_data) && isset($prev_data['id'])) ? $prev_data['id'] : '' ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><span class="text-highlight bg-primary">Balance : <?php echo $total_balance ?></span></li>
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Item <span class="text-danger">*</span></label>
                                <?php
                                $selected_item_id = (isset($prev_data) && isset($prev_data['item_id'])) ? $prev_data['item_id'] : set_value('item_id');
                                $attributes = array('id' => 'item_id', 'class' => 'select2-search');
                                ?>
                                <?php echo form_dropdown('item_id', $items_options, $selected_item_id, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>In Date <span class="text-danger">*</span></label>
                                <input
                                    id="log_date"
                                    name="log_date"
                                    type="text"
                                    class="form-control daterange-single-basic"
                                    placeholder="In Date"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['log_date'])) ? $prev_data['log_date'] : set_value('log_date') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Quantity <span class="text-danger">*</span><span class="text-muted max_qty hidden">( Max Available :  <span class="max_qty_count"></span>  )</span></label>
                                <input
                                    id="quantity"
                                    name="quantity"
                                    type="text"
                                    class="form-control"
                                    placeholder="Quantity"
                                    disabled="disabled"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['quantity'])) ? $prev_data['quantity'] : set_value('quantity') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Units <span class="text-danger">*</span></label>
                                <?php
                                $selected_unit = (isset($prev_data) && isset($prev_data['unit'])) ? $prev_data['unit'] : set_value('unit');
                                $attributes = array('id' => 'unit', 'class' => 'select2-basic', 'disabled' => 'disabled');
                                ?>
                                <?php echo form_dropdown('unit', array('' => 'Select Units'), $selected_unit, $attributes); ?>
                            </div>
                        </div>



                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea
                                    id="description"
                                    name="description"
                                    rows="5"
                                    cols="5"
                                    class="form-control resize-ver"
                                    placeholder="Enter description here"><?php echo (isset($prev_data) && isset($prev_data['details'])) ? $prev_data['details'] : set_value('description') ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <input type="hidden" class="purchase_price" name="purchase_price">
                        <input type="hidden" class="item_category_id" name="item_category_id">
                        <a href="stores" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            log_date: {
                required: true,
            },
            item_id: {
                required: true,
            },
            unit: {
                required: true,
            },
            quantity: {
                required: true,
                min: 1,
            },
            price: {
                required: true,
                min: 1,
                max: 100000,
            },
            status: {
                required: true
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);
        var itemIdControl = $(document).find('#item_id');
        var itemId = itemIdControl.val();
        if (itemId !== '') {
            itemIdControl.val(itemId).trigger('change');
        }
    });

    $(document).on('change', '#item_id', function () {
        var targetSelector = '#unit';
        var selectedItemId = $(this).val();
        if (selectedItemId !== '') {
            var url = '<?php base_url() ?>stores_return/get_items_units/' + selectedItemId;
            ajaxGet(url).then(function (response) {
                if (response) {
                    var obj = JSON.parse(response);
                    if (obj && obj.status && obj.status === 1) {
                        var unitsOptions = obj.data.units_options;
                        var itemDetails = obj.data.item;
                        var defaultUnit = obj.data.default_unit;
                        var $inventory_rec=obj.data.inventory_rec;
                        enableControl(targetSelector);
                        enableControl('#quantity');
                        enableControl('#price');
                        generateSelec2Dropdown(targetSelector, unitsOptions, 'Select units', '', '');
                        var selectedUnit = $inventory_rec['unit'];
                        $(document).find('#unit').val(selectedUnit).trigger('change.select2');
                        $(document).find('#price-per-qty-lbl').html('$' + itemDetails.purchase_price + '/' + defaultUnit);
                        $("#quantity").val($inventory_rec['stock_qty']);
                        $(".purchase_price").val(itemDetails.purchase_price);
                        $(".item_category_id").val($inventory_rec.item_category_id);
                        $(document).find('.max_qty').removeClass("hidden");
                        $(".max_qty_count").html($inventory_rec['stock_qty']);
                        $("#quantity").attr('max',$inventory_rec['stock_qty']);
                    } else {
                        disableControl(targetSelector);
                        disableControl('#quantity');
                        disableControl('#price');
                        cleanSelec2Dropdown(targetSelector, 'Select units', '');
                        $(document).find('#item_id').val('').trigger('change.select2');
                        $(document).find('#price-per-qty-lbl').html('');
                        $(document).find('.max_qty_count').html('');
                        $(document).find('.max_qty').addClass("hidden");
                        $(".item_category_id").val('')
                        $(".purchase_price").val('');
                    }
                } else {
                    disableControl(targetSelector);
                    disableControl('#quantity');
                    disableControl('#price');
                    cleanSelec2Dropdown(targetSelector, 'Select units', '');
                    $(document).find('#item_id').val('').trigger('change.select2');
                    $(document).find('#price-per-qty-lbl').html('');
                    $(document).find('.max_qty_count').html('');
                    $(document).find('.max_qty').addClass("hidden");
                    $(".purchase_price").val('');
                    $(".item_category_id").val('')
                }
            }, function (error) {
                disableControl(targetSelector);
                disableControl('#quantity');
                disableControl('#price');
                cleanSelec2Dropdown(targetSelector, 'Select units', '');
                $(document).find('#item_id').val('').trigger('change.select2');
                $(document).find('#price-per-qty-lbl').html('');
            });
        } else {
            disableControl(targetSelector);
            disableControl('#quantity');
            disableControl('#price');
            cleanSelec2Dropdown(targetSelector, 'Select units', '');
            $(document).find('#item_id').val('').trigger('change.select2');
            $(document).find('#price-per-qty-lbl').html('');
        }
    });
</script>