<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <a href="orders" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="orders/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add
                        order</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Order No</th>
                                    <th>Customer</th>
                                    <th>Price</th>
                                    <th>Status</th>
    <!--                                <th class="sticky-col">Actions</th>-->
                                </tr>
                            </thead>

                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var status_arr = {
            '<?php echo ENABLE ?>': "<?php echo ucwords("on going") ?>",
            '<?php echo DISABLE ?>': "<?php echo ucwords("completed") ?>",
        };
        var is_deleted_arr = {
            '<?php echo IS_DELETED_YES ?>': "Yes",
            '<?php echo IS_DELETED_NO ?>': "No",
        };

        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions' && title !== 'Price' && title !== 'Status') {
                if (title === 'Date Added') {
                    $(this).html('<input type="text" class="form-control daterange-basic-datatable" placeholder="' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                }
            }
        });
        var d = new Date();
        var table = $('#dttable').DataTable({
            dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                    title: "Sales Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    },
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                    title: "Sales Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                    title: "Sales Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    },
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                    title: "Sales Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                        columns: [0, 1, 2, 3]
                    },
                }

            ],
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            "columns": [
                {
                    data: 'order_master_created',
                    visible: true,
                    name: 'order_master.created',
                    render: function (data, type, full, meta) {
                        return get_mm_dd_yyyy_Date(full.order_master_created, '/');
                    }
                },
                {
                    'data': 'order_master_id',
                    "visible": true,
                    "name": 'order_master.id',
                },
                {
                    'data': 'order_master_customer_name',
                    "visible": true,
                    "name": 'order_master.customer_name',
                },
                {
                    'data': 'SUM(orders_price)',
                    "visible": true,
                    sortable: false,
                    searchable: false,
                    "name": 'SUM(orders.price)',
                },
                {
                    'data': 'order_master_type',
                    "visible": true,
                    sortable: false,
                    searchable: false,
                    "name": 'order_master.type',
                    "render": function (data, type, full, meta) {
                        var status = '<span class="label label-danger label-rounded"> ----- </span>';
                        if (full.order_master_type === '1') {
                            status = '<span class="label label-success label-rounded"> ' + status_arr[full.order_master_type] + '</span>';
                        } else if (full.order_master_type === '0') {
                            status = '<span class="label label-default label-rounded"> ' + status_arr[full.order_master_type] + '</span>';
                        }
                        return status;
                    }
                },
                /*{
                 "visible": true,
                 "sortable": false,
                 "searchable": false,
                 render: function (data, type, full, meta) {
                 var actionBtns = '';
                 /!*actionBtns = '<a href="orders/save/' + full.id + '" class="btn bg-teal btn-rounded btn-sm action-btns tooltip-show " title="Extra order" data-path="orders/save/' + full.id + '"><i class="fa fa-pencil-square-o"></i></a>';*!/
                 /!* actionBtns += '<a href="javascript:void(0)" class="delete_record btn btn-danger btn-rounded btn-sm action-btns tooltip-show" title="Cancel order" data-path="orders/delete/' + full.id + '"><i class="fa fa-trash"></i></a>';*!/
                 return actionBtns;
                 }
                 },*/
            ],
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
                this.api().columns().every(function (index) {
                    if (tableColumns[index].name == 'order.status' || tableColumns[index].name == 'order.is_deleted') {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">Select</option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? val : '', true, false)
                                            .draw();
                                });
                        if (tableColumns[index].name == 'order.status') {
                            for (var key in status_arr) {
                                if (status_arr.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + status_arr[key] + '</option>');
                                }
                            }

                        } else if (tableColumns[index].name == 'order.is_deleted') {
                            for (var key in is_deleted_arr) {
                                if (is_deleted_arr.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + is_deleted_arr[key] + '</option>');
                                }
                            }
                        }
                    }
                });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['col_eq'] = ['order_master.type'];
                req_obj['datatable_date_range'] = [
                    {'column': 'order_master.created', 'filter_format': 'Y-m-d', 'range_deliminator': '-'}
                ];
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': "<?php echo base_url() . 'orders/filter' ?>",
                    'data': req_obj,
                    'success': function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>