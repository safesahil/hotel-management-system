<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group col-md-12">
                        <label>Customer Name <span class="text-danger">*</span></label>
                        <input
                                id="customer_name"
                                name="customer_name"
                                type="text"
                                class="form-control"
                                placeholder="Name"
                                value="<?php echo isset($orders[0]['customer_name']) ? $orders[0]['customer_name'] : set_value('customer_name') ?>"/>
                    </div>

                    <div class="form-group col-md-6 hidden">
                        <label>Item category <span class="text-danger">*</span></label>
                        <?php
                        $i_c_options = array('' => 'Select category');
                        if (isset($order_categories) && count($order_categories) > 0) {
                            $i_c_options = $order_categories;
                        }
                        $selected_item_category = (isset($order_data) && isset($order_data['order_category'])) ? $order_data['order_category'] : set_value('order_category');
                        $attributes = array('id' => 'order_category', 'class' => 'select2-basic');
                        ?>
                        <?php echo form_dropdown('order_category', $i_c_options, $selected_item_category, $attributes); ?>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Item<span class="text-danger">*</span></label>
                        <?php
                        $i_c_options = array('' => 'Select Item');
                        if (isset($order_item) && count($order_item) > 0) {
                            $i_c_options = $order_item;
                        }
                        $selected_item = (isset($order_data) && isset($order_data['items'])) ? $order_data['items'] : set_value('items');
                        $attributes = array('id' => 'items', 'class' => 'select2-search');
                        ?>
                        <?php echo form_dropdown('items', $i_c_options, $selected_item, $attributes); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group col-md-6">
                        <label>Quantity<span class="text-danger">*</span></label>
                        <input
                                id="quantity"
                                name="quantity"
                                type="number"
                                class="form-control"
                                placeholder="Quantity"
                                value="<?php echo isset($order_data['quantity']) ? $order_data['quantity'] : set_value('quantity') ?>"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Price <span class="text-danger">*</span></label>
                        <input
                                id="price"
                                name="price"
                                type="number"
                                class="form-control"
                                placeholder="Price"
                                readonly
                                value="<?php echo isset($order_data['price']) ? $order_data['price'] : set_value('price') ?>"/>
                    </div>
                    <div class="text-left display-block col-md-12">

                        <button type="submit" id="add_order_item" class="btn btn-primary">Add More<i class=""></i>
                        </button>
                    </div>
                </div>

            </div>


        </form>

        <form action="orders/save_order/<?php echo isset($edit_id) ? $edit_id : "" ?>" method="post">
            <div class="table-responsive">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <table id="order_record_tbl" class=" table-striped custom_dt width-100-per">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Dish name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $btn_bool = false;
                            if (isset($orders) && !empty($orders)) {
                                $btn_bool = true;
                                $i = 1;
                                foreach ($orders as $order) {

                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $i++ ?>
                                        </td>
                                        <td>
                                            <input type="hidden" name="order_details[category_id][]"
                                                   value="<?php echo $order['category_id'] ?>"><input type="hidden"
                                                                                                      name="order_details[item_id][]"
                                                                                                      value="<?php echo $order['item_id'] ?>">
                                            <?php echo $order['item_name'] ?>
                                        </td>
                                        <td>
                                            <input type="hidden" name="order_details[qty][]"
                                                   value="<?php echo $order['qty'] ?>">
                                            <?php echo $order['qty'] ?>
                                        </td>
                                        <td>
                                            <input type="hidden" class="count_total" name="order_details[price][]"
                                                   value="<?php echo $order['price'] ?>">
                                            <?php echo $order['price'] ?>
                                            <label class="total_print "><input type="hidden" name="total" class="total">
                                                <label class="total_amount "><?php ?></label></label>
                                        </td>
                                        <td>
                                            <button type="button" class="remove_row btn btn-danger">Remove</button>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="customer_name" class="customer_name">
                        <input type="hidden" value="" id="hidden_price">
                        <?php
                        $btn_style = '';
                        if ($btn_bool) {
                            $btn_style = "display:block;";
                        }
                        ?>
                        <button type="submit" style="<?php echo $btn_style ?>"
                                class="btn btn-success btn-lg submit_order margin-top-20"
                                name="submit_order" value="1">
                            Place Order
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        if ($(".count_total").length) {
            total_price();
        }
        var rules = {
            customer_name: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
          /*  order_category: {
                required: true
            },*/
            items: {
                required: true
            },
            quantity: {
                required: true
            },

        };
        initValidation('.save-form', rules);
        jQuery('#items').change(function () {
            var c_val = jQuery(this).val();
            if (c_val != null) {
                jQuery("#items-error").remove();
            }
        });
        jQuery('#order_category').change(function () {
            targetSelector = '#items';
            $this = jQuery(this);
            var current_val = $this.val();
            $.ajax({
                url: "<?php echo base_url('orders/get_details')?>",
                type: "POST",
                dataType: "JSON",
                data: {"category_id": current_val},
                success: function (data) {
                    $(document).find(targetSelector).html('');
                    if (data) {
                        enableControl(targetSelector);
                        $(document).find(targetSelector).html(data);
                        $(document).find(targetSelector).trigger('change.select2');
                    }
                    else {
                        $(document).find(targetSelector).trigger('change.select2');
                    }
                }
            });
        });

        jQuery('#items').change(function () {
            $this = jQuery(this);
            var current_val = $this.val();
            if (current_val) {
                $.ajax({
                    url: "<?php echo base_url('orders/get_order_price')?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {"item_id": current_val},
                    success: function (data) {
                        if (data) {
                            $('#hidden_price').val(data);
                        }
                        else {
                        }
                    }
                });
            }

        });
        $('body').on('change paste keyup', '#quantity', function () {
            $this = $(this);
            var value = $this.val();
            var hidden_field_price = $("#hidden_price").val();
            var total_price = value * hidden_field_price;
            $("#price").val(Math.abs(total_price));

        });
        $('#add_order_item').click(function (event) {
            event.preventDefault();
            var count = jQuery("#order_record_tbl tbody").children('tr').length + 1;
            $(".total_print").remove();
            append =
                '<tr>' +
                '<td>' + count + '</td>' +
                '<td><input type="hidden" name="order_details[category_id][]" value="' + $('#order_category').val() + '"><input type="hidden" name="order_details[item_id][]" value="' + $('#items').val() + '">' + $('#items option:selected').text() + '</td>' +
                '<td><input type="hidden" name="order_details[qty][]" value="' + $('#quantity').val() + '">' + $('#quantity').val() + '</td>' +
                '<td><input type="hidden" name="order_details[price][]" class="count_total" value="' + $('#price').val() + '">' + $('#price').val() + '<label class="total_print "><input type="hidden" name="total" class="total"> <label class="total_amount "></label></label></td>' +
                '<td><button type="button" class="remove_row btn btn-danger">Remove</button> </td>' +
                '</tr>';
            if (jQuery('.save-form').valid()) {
                $("#order_record_tbl tbody").append(append);
                clear_box();
                total_price();
            }


        });
        $('body').on('click', '.remove_row', function () {
            var count = jQuery("#order_record_tbl tbody").children('tr').length;
            $this = $(this);
            $this.parents('tr').remove();
            total_price();
            if (count == 0) {
                jQuery('.submit_order').hide();
            }

        });

        function total_price() {
            var sum = 0;
            $(".count_total").each(function () {
                sum += +$(this).val();
            });
            $(".total").val(sum);
            $(".total_amount").html(sum);
        }

        function clear_box() {
            var customer_name = jQuery("#customer_name").val();
            $("#save")[0].reset();
            jQuery("#customer_name").val(customer_name);
            jQuery(".customer_name").val(customer_name);
            $(document).find('#items').val("");
            $(document).find('#items').trigger('change.select2');
            $(document).find('#order_category').val("");
            $(document).find('#order_category').trigger('change.select2');
            jQuery('.submit_order').show();
        }
    });
</script>