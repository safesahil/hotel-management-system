<?php
/**
 * Created by PhpStorm.
 * User: Sarfaraj Kazi
 * Date: 07-01-2019
 * Time: 08:21
 */
?>

<div class="row">
    <div class="col-md-12">
        <form class="save-form" action="orders/print_bill_save" method="post">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>Select order <span class="text-danger">*</span></label>
                        <?php
                        $i_c_options = array('' => 'Select order');
                        if (isset($orders) && count($orders) > 0) {
                            $i_c_options = $orders;
                        }
                        $selected_item_category =array();
                        $attributes = array('id' => 'order_id', 'class' => 'select2-search');
                        ?>
                        <?php echo form_dropdown('order_id', $i_c_options, $selected_item_category, $attributes); ?>
                    </div>
                    <table id="order_record_tbl" class=" table-striped custom_dt width-100-per">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Dish name</th>
                            <th>Quantity</th>
                            <th>Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>

                        <tfoot>

                        </tfoot>
                    </table>
                    <div class="clearfix"></div>
                    <div class="price_print text-right">
                        <button type="submit" class="btn btn-success" style="display: none" onclick="printPage();" id="print_bill_btn">Print bill</button>
                    </div>

                </div>
            </div>

        </form>
    </div>
</div>
<script>
    $(function () {
        $('#order_id').change(function (e) {
            $this = jQuery(this);
            $(".please_wait").remove();
            var current_val = $this.val();
            if (current_val) {
                $("#order_record_tbl").before('<label class="please_wait"> <span> Please wait <i class="fa fa-spinner fa-spin"></i>  </span> </label>');
                $.ajax({
                    url: "<?php echo base_url('orders/get_order_details')?>",
                    type: "POST",
                    dataType: "JSON",
                    data: {"order_id": current_val},
                    success: function (data) {
                        $(".please_wait").remove();
                        if (data) {
                            jQuery('tbody').html('');
                            jQuery('tbody').html(data.return_html);
                            jQuery('tfoot').html('');
                            jQuery('tfoot').html(data.return_html_price);
                            jQuery("#print_bill_btn").show();
                        }
                        else {
                        }
                    }
                });
            }
        });

    });
    function printPage(){
        var css='#customers {' +
            '  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;' +
            '  border-collapse: collapse;' +
            '  width: 100%;' +
            '}' +
            '' +
            '#customers td, #customers th {' +
            '  border: 1px solid #ddd;' +
            '  padding: 8px;' +
            '}' +
            '' +
            '#customers tr:nth-child(even){background-color: #f2f2f2;}' +
            '' +
            '#customers tr:hover {background-color: #ddd;}' +
            '' +
            '#customers th {' +
            '  padding-top: 12px;' +
            '  padding-bottom: 12px;' +
            '  text-align: left;' +
            '  background-color: #4CAF50;' +
            '  color: white;' +
            '}';
        var tableData = '<br><br><table id="customers" border="1">'+document.getElementsByTagName('table')[0].innerHTML+'</table><br><br>';
        var data = '<button onclick="window.print()">Print this page</button>'+tableData;
        myWindow=window.open('','','width=600,height=400');
        myWindow.innerWidth = screen.width;
        myWindow.innerHeight = screen.height;
        myWindow.screenX = 0;
        myWindow.screenY = 0;
        myWindow.document.write(data);
        myWindow.focus();
    };
</script>