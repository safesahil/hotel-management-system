<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Hotel Management System</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/icons/icomoon/styles.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/core.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/components.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/colors.css') ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/pace.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/jquery.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/core/libraries/bootstrap.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/loaders/blockui.min.js') ?>"></script>
        <!-- /core JS files -->

    </head>

    <body class="login-container">

        <!-- Main navbar -->
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo base_url() ?>">
                    <label class="header_brand_name">Lake View Hotel</label>
                </a>
            </div>
        </div>
        <!-- /main navbar -->


        <!-- Page container -->
        <div class="page-container">

            <!-- Page content -->
            <div class="page-content">

                <!-- Main content -->
                <div class="content-wrapper">

                    <!-- Content area -->
                    <div class="content">

                        <!-- Simple login form -->
                        <form method="POST" class="login-form-validate">
                            <div class="panel panel-body login-form">
                                <div class="text-center">
                                    <div class="icon-object border-slate-300 text-slate-300"><i class="icon-reading"></i></div>
                                    <h5 class="content-group">Login to your account <small class="display-block">Enter your credentials below</small></h5>
                                </div>

                                <?php if (validation_errors()) { ?>
                                    <div class="alert alert-danger alert-bordered">
                                        <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
                                        <?php echo validation_errors(); ?>
                                    </div>
                                <?php } ?>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="text" id="email_id" name="email_id" class="form-control" placeholder="Email">
                                    <div class="form-control-feedback">
                                        <i class="icon-user text-muted"></i>
                                    </div>
                                </div>

                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                                    <div class="form-control-feedback">
                                        <i class="icon-lock2 text-muted"></i>
                                    </div>
                                </div>
                                
                                

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block">Sign in <i class="icon-circle-right2 position-right"></i></button>
                                </div>
                                <div class="form-group has-feedback has-feedback-left">
                                    <a href="forget">Lost password ?</a>
                                </div>
                            </div>
                        </form>
                        <!-- /simple login form -->


                        <!-- Footer -->
                        <div class="footer text-muted text-center">
                            &copy; <?php echo date('Y') ?>. <a href="<?php echo base_url() ?>">Hotel management system</a> by <a href="http://www.whiteinfotech.com/" target="_blank">White Infotech</a>
                        </div>
                        <!-- /footer -->

                    </div>
                    <!-- /content area -->

                </div>
                <!-- /main content -->

            </div>
            <!-- /page content -->

        </div>
        <!-- /page container -->
        <!-- Theme JS files -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/forms/validation/validate.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/scripts/my-validation.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/core/app.js') ?>"></script>
        <!-- /theme JS files -->
    </body>
</html>
