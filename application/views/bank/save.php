<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="bank/save/<?php echo (isset($bank_data) && isset($bank_data['id'])) ? $bank_data['id'] : '' ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input
                            id="name"
                            name="name"
                            type="text"
                            class="form-control"
                            placeholder="Name"
                            value="<?php echo isset($bank_data['name'])?$bank_data['name']:set_value('name') ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea
                            id="description"
                            name="description"
                            rows="5"
                            cols="5"
                            class="form-control resize-ver"
                            placeholder="Enter description here"><?php echo isset($bank_data['details'])?$bank_data['details']:set_value('description') ?></textarea>
                    </div>
                    <div class="form-group">
                        <label>Status <span class="text-danger">*</span></label>
                        <?php
                        $status_options = array('1' => 'Active', '0' => 'Inactive');
                        $selected_status = isset($bank_data['status'])?$bank_data['status']:set_value('status');
                        $attributes = array('id' => 'status', 'class' => 'select2-basic');
                        ?>
                        <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                    </div>

                    

                    <div class="text-right">
                        <a href="bank" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            name: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            status: {
                required: true
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);
        
        jQuery('#type').change(function(){
            var c_val=jQuery(this).val();
            if(c_val!=null){
                jQuery("#type-error").remove();
            }
        });
        
    });
</script>