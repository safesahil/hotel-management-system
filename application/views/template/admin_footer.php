<div class="footer text-muted text-center">
    &copy; <?php echo date('Y') ?>. <a href="dashboard">Hotel management system</a> by <a href="http://www.whiteinfotech.com/" target="_blank">White Infotech</a>
</div>