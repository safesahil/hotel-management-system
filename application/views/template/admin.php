<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('template/admin_common_head'); ?>
        <!-- Core JS files -->
        <!-- /Core JS files -->
    </head>

    <body>
        <!-- Main navbar -->
        <?php $this->load->view('template/admin_navbar'); ?>
        <!-- /Main navbar -->

        <div class="page-container">
            <div class="page-content">
                <!-- Main sidebar -->
                <?php $this->load->view('template/admin_sidebar'); ?>
                <!-- /main sidebar -->

                <div class="content-wrapper">
                    <div class="page-header page-header-default">
                        <div class="page-header-content">
                            <div class="page-title">
                                <h4><span class="text-semibold"><?php echo $page_header ?></span></h4>
                                <a class="heading-elements-toggle"><i class="icon-more"></i></a>
                            </div>

                            <?php
                            if ($this->session->flashdata('success_msg')) {
                                ?>
                                <div class="alert alert-success alert-styled-right alert-arrow-right alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <?php echo $this->session->flashdata('success_msg') ?>
                                </div>
                                <?php
                            }

                            if ($this->session->flashdata('warning_msg')) {
                                ?>
                                <div class="alert alert-warning alert-styled-right alert-arrow-right alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <?php echo $this->session->flashdata('warning_msg') ?>
                                </div>
                                <?php
                            }

                            if ($this->session->flashdata('error_msg')) {
                                ?>
                                <div class="alert alert-danger alert-styled-right alert-arrow-right alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <?php echo $this->session->flashdata('error_msg') ?>
                                </div>
                                <?php
                            }

                            if ($this->session->flashdata('info_msg')) {
                                ?>
                                <div class="alert alert-info alert-styled-right alert-arrow-right alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    <?php echo $this->session->flashdata('info_msg') ?>
                                </div>
                                <?php
                            }

                            if (!empty(validation_errors())) {
                                ?>
                                <div class="alert alert-danger alert-bordered">
                                    <?php
                                    echo validation_errors();
                                    ?>
                                </div>        
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="content">
                        <?php echo $body; ?>

                        <?php $this->load->view('template/admin_footer'); ?>
                    </div> 
                </div>
            </div>
        </div>
      

        <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

        <script type="text/javascript" src="assets/js/plugins/uploaders/fileinput.min.js"></script>
        
        <script type="text/javascript" src="assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/notifications/bootbox.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/notifications/sweet_alert.min.js"></script>
        <script type="text/javascript" src="assets/js/plugins/forms/validation/validate.min.js"></script>
        <script type="text/javascript" src="assets/js/scripts/my-validation.js"></script>
        <script type="text/javascript" src="assets/js/scripts/scripts.js"></script>
    </body>
</html>
