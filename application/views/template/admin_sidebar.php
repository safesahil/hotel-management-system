<?php
$uri_segment_1 = $this->uri->segment(1);
$uri_segment_2 = $this->uri->segment(2);
$logged_user = $this->session->userdata(ADMINS_STR_KEY);
?>
<!-- Main sidebar -->
<div class="sidebar sidebar-main">
    <div class="sidebar-content">

        <!-- User menu -->
        <div class="sidebar-user">
            <div class="category-content">
                <div class="media">
                    <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                    <div class="media-body">
                        <span class="media-heading text-semibold"><?php echo $logged_user['first_name']; ?> <?php echo $logged_user['last_name']; ?></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- /user menu -->


        <!-- Main navigation -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">

                    <!-- Main -->
                    
                    <li class="<?php echo ($uri_segment_1 === 'dashboard') ? 'active' : '' ?>"><a href="dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                    
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'users') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-users"></i> <span>Users</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Users" style="display: none;">
                            <li class="nav-item"><a href="users/save/customers" class="nav-link">Add New Customer</a></li>
                            <li class="nav-item"><a href="users/listing/customers" class="nav-link">Customers List</a></li>
                            <li class="nav-item"><a href="users/save/agents" class="nav-link">Add Agents</a></li>
                            <li class="nav-item"><a href="users/listing/agents" class="nav-link">Agent List</a></li>
                            <li class="nav-item"><a href="users/save/tours" class="nav-link">Add Tour operator</a></li>
                            <li class="nav-item"><a href="users/listing/tours" class="nav-link disabled">List Tour operator</a></li>
                            <li class="nav-item"><a href="users/save/admins" class="nav-link">Add Employee</a></li>
                            <li class="nav-item"><a href="users/listing/admins" class="nav-link disabled">List Employee</a></li>
                        </ul>
                    </li>
                  
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'checking') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'checkout') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-drawer-in"></i> <span>Booking</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Booking" style="display: none;">
                            <li class="nav-item"><a href="checking" class="nav-link">Check In</a></li>
                            <li class="nav-item"><a href="checkout" class="nav-link">Check Out</a></li>
                        </ul>
                    </li>
                    
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'rooms_category') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'rooms') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'room_services') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-home7"></i> <span>Room Master</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Room Master" style="display: none;">
                            <li class="nav-item"><a href="rooms_category" class="nav-link">Room category list</a></li>
                            <li class="nav-item"><a href="rooms_category/save" class="nav-link">Add Room category</a></li>
                            <li class="nav-item"><a href="rooms" class="nav-link">Rooms list</a></li>
                            <li class="nav-item"><a href="rooms/save" class="nav-link">Add Room</a></li>
                            
                            
                            <li class="nav-item"><a href="room_services" class="nav-link">Room services</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'items') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'items_category') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'stores') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'stock_in') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-price-tags"></i> <span>Store (Inventory)</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Store (Inventory)" style="display: none;">
                            <li class="nav-item"><a href="items" class="nav-link">Items</a></li>
                            <li class="nav-item"><a href="items/save" class="nav-link">Add Item</a></li>
                            <li class="nav-item"><a href="items_category/save" class="nav-link">Add Item Category</a></li>
                            <li class="nav-item"><a href="items_category" class="nav-link">Item Category</a></li>
                             <li class="<?php echo ($uri_segment_1 === 'stores') ? 'active' : '' ?>"><a href="stores"><i class="icon-store"></i> <span>Stock In</span></a></li>
                            <li class=""><a href="stores_return"><i class="icon-store"></i> <span>Stock remove</span></a></li>
                             <li class="<?php echo ($uri_segment_1 === 'stock_in') ? 'active' : '' ?>"><a href="stock_in"><i class="icon-clipboard2"></i> <span>Store</span></a></li>
                        </ul>
                    </li>

                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'restaurant') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-cart5"></i> <span>Kitchen Inventory</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Restaurants" style="display: none;">
                            <li class="nav-item"><a href="kitchen" class="nav-link">Items</a></li>
                            <li class="nav-item"><a href="kitchen/save" class="nav-link">Add Item</a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'mini_bar') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-coffee"></i> <span>Mini Bar</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Mini Bar" style="display: none;">
                            <li class="nav-item"><a href="mini_bar" class="nav-link">Items</a></li>
                            <li class="nav-item"><a href="mini_bar/save" class="nav-link">Add Item</a></li>
                             <li class="<?php echo ($uri_segment_1 === 'mini_bar_store') ? 'active' : '' ?>"><a href="mini_bar_store"><i class="icon-coffee"></i> <span>Mini Bar Store</span></a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'transactions') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-clipboard6"></i> <span>Reports</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Reports" style="display: none;">
                            <li class="nav-item"><a href="transactions/checking" class="nav-link">Check in list</a></li>
                            <li class="nav-item"><a href="transactions/checkout" class="nav-link">Check out list</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo ($uri_segment_1 === 'petty_cash') ? 'active' : '' ?>"><a href="petty_cash"><i class="icon-cash"></i> <span>Petty Cash</span></a></li>
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'income') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'expense') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-clipboard5"></i> <span>Transactions</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Transactions" style="display: none;">
                             <li class="<?php echo ($uri_segment_1 === 'incomes') ? 'active' : '' ?>"><a href="incomes"><i class="icon-arrow-down-left32"></i> <span>Income</span></a></li>
                            <li class="<?php echo ($uri_segment_1 === 'expense') ? 'active' : '' ?>"><a href="expense"><i class="icon-arrow-up-right32"></i> <span>Expense</span></a></li>
                        </ul>
                    </li>
                    <li class="nav-item nav-item-submenu <?php echo ($uri_segment_1 === 'restaurants_items_category' || $uri_segment_1 === 'restaurants_items') ? 'active' : '' ?> <?php echo ($uri_segment_1 === 'orders' || $uri_segment_1 === 'orders') ? 'active' : '' ?>">
                        <a href="#" class="nav-link"><i class="icon-clipboard2"></i> <span>Restaurants</span></a>

                        <ul class="nav nav-group-sub" data-submenu-title="Restaurants" style="display: none;">
                            <li class="nav-item"><a href="restaurants_items_category" class="nav-link">Item Category</a></li>
                            <li class="nav-item"><a href="restaurants_items_category/save" class="nav-link">Add Item Category</a></li>
                            <li class="nav-item"><a href="restaurants_items" class="nav-link">Items</a></li>
                            <li class="nav-item"><a href="restaurants_items/save" class="nav-link">Add Item</a></li>
                            <li class="nav-item"><a href="orders" class="nav-link">Sales report</a></li>
                            <li class="nav-item"><a href="orders/save" class="nav-link">Add Sale report</a></li>
                            <li class="nav-item"><a href="orders/print_bill" class="nav-link">Print bill</a></li>
                        </ul>
                    </li>
<!--                    <li class="<?php // echo ($uri_segment_1 === 'users') ? 'active' : '' ?>"><a href="users/listing/all"><i class="icon-users"></i> <span>Users</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'incomes') ? 'active' : '' ?>"><a href="incomes"><i class="icon-arrow-down-left32"></i> <span>Income</span></a></li>
                    <li class="<?php // echo ($uri_segment_1 === 'expense') ? 'active' : '' ?>"><a href="expense"><i class="icon-arrow-up-right32"></i> <span>Expense</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'rooms_category') ? 'active' : '' ?>"><a href="rooms_category"><i class="icon-stack3"></i> <span>Rooms category</span></a></li>
                    <li class="<?php // echo ($uri_segment_1 === 'rooms') ? 'active' : '' ?>"><a href="rooms"><i class="icon-home7"></i> <span>Rooms</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'items_category') ? 'active' : '' ?>"><a href="items_category"><i class="icon-list-unordered"></i> <span>Item category</span></a></li>
                    <li class="<?php // echo ($uri_segment_1 === 'items') ? 'active' : '' ?>"><a href="items"><i class="icon-price-tags"></i> <span>Items</span></a></li>-->
                    <!--<li class="<?php // echo ($uri_segment_1 === 'stores') ? 'active' : '' ?>"><a href="stores"><i class="icon-store"></i> <span>Stock In</span></a></li>-->
                    <!--<li class="<?php // echo ($uri_segment_1 === 'kitchen') ? 'active' : '' ?>"><a href="kitchen"><i class="icon-cart5"></i> <span>Kitchen</span></a></li>-->
                    <!--<li class="<?php // echo ($uri_segment_1 === 'mini_bar') ? 'active' : '' ?>"><a href="mini_bar"><i class="icon-store2"></i> <span>Mini Bar Stock In</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'mini_bar_store') ? 'active' : '' ?>"><a href="mini_bar_store"><i class="icon-coffee"></i> <span>Mini Bar Store</span></a></li>-->
                    <!--<li class="<?php // echo ($uri_segment_1 === 'stock_in') ? 'active' : '' ?>"><a href="stock_in"><i class="icon-clipboard2"></i> <span>Store</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'checking') ? 'active' : '' ?>"><a href="checking"><i class="icon-drawer-in"></i> <span>Checking</span></a></li>
                    <li class="<?php // echo ($uri_segment_1 === 'checkout') ? 'active' : '' ?>"><a href="checkout"><i class="icon-drawer-out"></i> <span>Checkout</span></a></li>
                    <li class="<?php // echo ($uri_segment_1 === 'transactions') ? 'active' : '' ?>"><a href="transactions"><i class="icon-clipboard6"></i> <span>Transactions</span></a></li>-->
<!--                    <li class="<?php // echo ($uri_segment_1 === 'room_services') ? 'active' : '' ?>"><a href="room_services"><i class="icon-playlist-add"></i> <span>Room services</span></a></li>-->
                    <li class="<?php echo ($uri_segment_1 === 'bank') ? 'active' : '' ?>"><a href="bank"><i class="icon-office"></i> <span>Banks</span></a></li>
                    
                </ul>
            </div>
        </div>
        <!-- /main navigation -->

    </div>
</div>
<!-- /main sidebar -->