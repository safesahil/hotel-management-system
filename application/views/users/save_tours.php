<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="users/save/<?php echo TOURS_STR_KEY; ?>/<?php echo (isset($prev_data) && isset($prev_data['id'])) ? $prev_data['id'] : '' ?>" method="POST" class="save-form" enctype="multipart/form-data">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First name <span class="text-danger">*</span></label>
                                <input
                                    id="first_name"
                                    name="first_name"
                                    type="text"
                                    class="form-control"
                                    placeholder="First name"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['first_name'])) ? $prev_data['first_name'] : set_value('first_name') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Last name</label>
                                <input
                                    id="last_name"
                                    name="last_name"
                                    type="text"
                                    class="form-control"
                                    placeholder="Last name"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['last_name'])) ? $prev_data['last_name'] : set_value('last_name') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Mobile no.</label>
                                <input
                                    id="mobile_number"
                                    name="mobile_number"
                                    type="text"
                                    class="form-control"
                                    placeholder="Mobile no."
                                    value="<?php echo (isset($prev_data) && isset($prev_data['mobile_number'])) ? $prev_data['mobile_number'] : set_value('mobile_number') ?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Proof Method <span class="text-danger">*</span></label>
                                <?php
                                $proof_options = array(
                                    "" => "Select",
                                    PROOF_ID_CARD => PROOF_ID_CARD_STR,
                                    PROOF_PASSPORT => PROOF_PASSPORT_STR,
                                    PROOF_DRIVING_LIC => PROOF_DRIVING_LIC_STR
                                );
                                $selected_proof = (isset($prev_data) && isset($prev_data['proof_method'])) ? $prev_data['proof_method'] : set_value('proof_method');
                                $attributes = array('id' => 'proof_method', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('proof_method', $proof_options, $selected_proof, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Number <span class="text-danger">*</span></label>
                                <input
                                    id="proof_number"
                                    name="proof_number"
                                    type="text"
                                    class="form-control"
                                    placeholder="Number"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['proof_number'])) ? $prev_data['proof_number'] : set_value('proof_number') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Issue Month <span class="text-danger">*</span></label>
                                <?php
                                $month_options = array('' => 'Month');
                                for ($i = 1; $i <= 12; $i++) {
                                    $month_options[$i] = $i;
                                }
                                $selected_month = (isset($prev_data) && isset($prev_data['proof_month'])) ? $prev_data['proof_month'] : set_value('proof_month');
                                $attributes = array('id' => 'proof_month', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('proof_month', $month_options, $selected_month, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Issue Year <span class="text-danger">*</span></label>
                                <?php
                                $year_options = array('' => 'Year');
                                $current_year = date('Y');
                                $prev_year = ($current_year - 150);
                                for ($i = $current_year; $i > $prev_year; $i--) {
                                    $year_options[$i] = $i;
                                }
                                $selected_year = (isset($prev_data) && isset($prev_data['proof_year'])) ? $prev_data['proof_year'] : set_value('proof_year');
                                $attributes = array('id' => 'proof_year', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('proof_year', $year_options, $selected_year, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <label>Proof Country <span class="text-danger">*</span></label>
                                <input
                                    id="proof_country"
                                    name="proof_country"
                                    type="text"
                                    class="form-control"
                                    placeholder="Proof Country"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['proof_country'])) ? $prev_data['proof_country'] : set_value('proof_country') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Proof <span class="text-danger">*</span> <span class="text-muted text-size-mini">Only jpg, jpeg and png is allowed</span></label>
                                <input type="file" id="proof" name="proof" class="file-input" data-show-preview="false" data-show-upload="false" data-show-caption="true" />
                            </div>
                        </div>
                        <?php if (isset($prev_data) && isset($prev_data['proof'])) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Proof copy</label>
                                    <a href="<?php echo $prev_data['proof'] ?>" target="_blank" class="btn btn-block btn-default">Click here!</a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input
                                    id="email_id"
                                    name="email_id"
                                    type="text"
                                    class="form-control"
                                    placeholder="Email"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['email'])) ? $prev_data['email'] : set_value('email_id') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Gender <span class="text-danger">*</span></label>
                                <?php
                                $gender_options = array(MALE_INT => MALE, FEMALE_INT => FEMALE);
                                $selected_gender = (isset($prev_data) && isset($prev_data['gender'])) ? $prev_data['gender'] : set_value('gender');
                                $attributes = array('id' => 'gender', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('gender', $gender_options, $selected_gender, $attributes); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <?php
                                $status_options = array('1' => 'Active', '0' => 'Inactive');
                                $selected_status = (isset($prev_data) && isset($prev_data['status'])) ? $prev_data['status'] : set_value('status');
                                $attributes = array('id' => 'status', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Address:</label>
                                <textarea
                                    id="address"
                                    name="address"
                                    rows="5"
                                    cols="5"
                                    class="form-control resize-ver"
                                    placeholder="Enter address"><?php echo (isset($prev_data) && isset($prev_data['address'])) ? $prev_data['address'] : set_value('address') ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="users/listing/<?php echo TOURS_STR_KEY ?>" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {};
<?php if (isset($prev_data) && isset($prev_data['id'])) { ?>
            rules = {
                first_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 15,
                },
                last_name: {
                    minlength: 2,
                    maxlength: 15,
                },
                proof_method: {
                    required: true
                },
                proof_number: {
                    required: true,
                    minlength: 2,
                    maxlength: 32,
                },
                proof_month: {
                    required: true,
                    min: 1,
                    max: 12
                },
                proof_year: {
                    required: true
                },
                proof_country: {
                    required: true
                },
                email_id: {
                    email: true,
                    minlength: 5,
                    maxlength: 100,
                },
                gender: {
                    required: true,
                },
                status: {
                    required: true
                },
                address: {
                    maxlength: 250
                }
            };
<?php } else { ?>
            rules = {
                first_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 15,
                },
                last_name: {
                    minlength: 2,
                    maxlength: 15,
                },
                proof_method: {
                    required: true
                },
                proof_number: {
                    required: true,
                    minlength: 2,
                    maxlength: 32,
                },
                proof_month: {
                    required: true,
                    min: 1,
                    max: 12
                },
                proof_year: {
                    required: true
                },
                proof_country: {
                    required: true
                },
                proof: {
                    required: true,
                },
                email_id: {
                    email: true,
                    minlength: 5,
                    maxlength: 100,
                },
                gender: {
                    required: true,
                },
                status: {
                    required: true
                },
                address: {
                    maxlength: 250
                }
            };
<?php } ?>
        initValidation('.save-form', rules);
    });
</script>