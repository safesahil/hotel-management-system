<div class="row">
    <div class="col-xs-12">
        <div class="btn-group pull-right ml-5">
            <button type="button" class="btn btn-primary btn-labeled dropdown-toggle" data-toggle="dropdown"><b><i class="icon-plus22"></i></b>Add User<span class="caret"></span></button>
            <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="users/save/customers"><i class="fa fa-user"></i>Customer</a></li>
                <li><a href="users/save/agents"><i class="fa fa-user-md"></i>Agent</a></li>
                <li><a href="users/save/tours"><i class="fa fa-street-view"></i>Tour</a></li>
                <li><a href="users/save/admins"><i class="fa fa-user-secret"></i>Admin</a></li>
            </ul>
        </div>
        <a href="<?php echo base_url('users/listing/all'); ?>" class="pull-right btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
    </div>

    <form id="form" method="post">
        <div class="table-responsive popular_list col-xs-12 pt-20">
            <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                <thead>
                    <tr>
                        <th>Date Added</th>
                        <th>Mobile Phone</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Deleted ?</th>
                        <th class="sticky-col">Actions</th>
                    </tr>
                    <tr>
                        <th>Date Added</th>
                        <th>Mobile Phone</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Status</th>
                        <th>Deleted ?</th>
                        <th class="sticky-col">Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var roles = {1: "Customer", 2: "Agent", 3: "Tour", 99: "Admin"};
        var statusOptions = {1: "Active", 0: "Inactive"};
        var isDeleted = {1: "Yes", 0: "No"};

        // Setup - add a text input to each footer cell
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions') {
                if (title === 'Date Added') {
                    $(this).html('<input type="text" class="form-control daterange-basic-datatable" placeholder="' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                }
            }
        });

        //datatables
          var d = new Date();
        var table = $('#dttable').DataTable({
        dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                    title: "Users Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1]
                    },
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                    title: "Users Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                    title: "Users Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1]
                    },
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                    title: "Users Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1]
                    },
                    
                }

            ],
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            columns: [
                {
                    data: 'users_created_date',
                    visible: true,
                    name: 'users.created_date',
                    render: function (data, type, full, meta) {
                        return get_mm_dd_yyyy_Date(full.users_created_date, '/');
                    }
                },
                {
                    data: 'users_mobile_number',
                    visible: true,
                    name: 'users.mobile_number',
                },
                {
                    data: 'users_first_name',
                    visible: true,
                    name: 'users.first_name',
                },
                {
                    data: 'users_last_name',
                    visible: true,
                    name: 'users.last_name',
                },
                {
                    data: 'users_email',
                    visible: true,
                    name: 'users.email',
                },
                {
                    data: 'users_role',
                    visible: true,
                    name: 'users.role',
                    render: function (data, type, full, meta) {
                        if (roles.hasOwnProperty(full.users_role)) {
                            return roles[full.users_role];
                        } else {
                            return "-----";
                        }
                    }
                },
                {
                    data: 'users_status',
                    visible: true,
                    name: 'users.status',
                    render: function (data, type, full, meta) {
                        var status = 'Unknown';
                        if (full.users_status === '0') {
                            status = '<span class="label label-warning label-rounded"> Inactive</span>';
                        } else if (full.users_status === '1') {
                            status = '<span class="label label-success label-rounded"> Active</span>';
                        }
                        return status;
                    }
                },
                {
                    data: 'users_is_deleted',
                    visible: true,
                    name: 'users.is_deleted',
                    render: function (data, type, full, meta) {
                        var status = "";
                        if (full.users_is_deleted === '1') {
                            status = '<span class="label label-danger label-rounded"> Yes</span>';
                        } else {
                            status = '<span class="label label-success label-rounded"> No</span>';
                        }
                        return status;
                    }
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        var role = null;
                        if (full.users_role === '<?php echo CUSTOMERS ?>') {
                            role = '<?php echo CUSTOMERS_STR_KEY ?>';
                        } else if (full.users_role === '<?php echo AGENTS ?>') {
                            role = '<?php echo AGENTS_STR_KEY ?>';
                        } else if (full.users_role === '<?php echo TOURS ?>') {
                            role = '<?php echo TOURS_STR_KEY ?>';
                        } else if (full.users_role === '<?php echo ADMINS ?>') {
                            role = '<?php echo ADMINS_STR_KEY ?>';
                        }
                        actionBtns = '<a href="users/save/' + role + '/' + full.id + '" class="btn btn-primary btn-rounded btn-sm action-btns"><i class="fa fa-pencil"></i></a>';
                        if (full.users_is_deleted === '1') {
                            actionBtns += '<a href="javascript:void(0)" class="recover_record btn bg-teal btn-rounded btn-sm action-btns tooltip-show" title="Recover" data-path="users/recover/' + role + '/' + full.id + '"><i class="fa fa-undo"></i></a>';
                        } else {
                            actionBtns += '<a href="javascript:void(0)" class="delete_record btn btn-danger btn-rounded btn-sm action-btns tooltip-show" title="Delete" data-path="users/delete/' + role + '/' + full.id + '"><i class="fa fa-trash"></i></a>';
                        }
                        return actionBtns;
                    }
                }
            ],
            columnDefs: [
                {width: "12%", targets: 7}
            ],
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
                this.api().columns().every(function (index) {
                    if (tableColumns[index].name == 'users.role' || tableColumns[index].name == 'users.status' || tableColumns[index].name == 'users.is_deleted') {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">Select</option></select>')
                                .appendTo($('th:nth-child(' + (index + 1) + '):first').empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? val : '', true, false)
                                            .draw();
                                });
                        if (tableColumns[index].name == 'users.role') {
                            for (var key in roles) {
                                if (roles.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + roles[key] + '</option>');
                                }
                            }
                        } else if (tableColumns[index].name == 'users.status') {
                            for (var key in statusOptions) {
                                if (statusOptions.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + statusOptions[key] + '</option>');
                                }
                            }
                        } else if (tableColumns[index].name == 'users.is_deleted') {
                            for (var key in isDeleted) {
                                if (isDeleted.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + isDeleted[key] + '</option>');
                                }
                            }
                        }
                    }
                });
            },
            fnServerData: function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['col_eq'] = ['users.role', 'users.status', 'users.is_deleted'];
                req_obj['datatable_date_range'] = [
                    {'column': 'users.created_date', 'filter_format': 'Y-m-d', 'range_deliminator': '-'}
                ];
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: "<?php echo base_url() . 'users/filter/' . $role ?>",
                    data: req_obj,
                    success: function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>
