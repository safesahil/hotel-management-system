<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
            </div>

            <div class="panel-body pt-0 pb-0">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        <?php foreach ($roles as $name => $url) { ?>
                            <li class="<?php echo (strcasecmp($role, str_replace(' ', '_', $name))) ? '' : 'active' ?>"><a href="<?php echo $url ?>"><?php echo $name; ?></a></li>
                        <?php } ?>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="highlighted-tab1">
                            <?php echo $tab_content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>