<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="checkout/save" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Room No.</label>
                                    </div>
                                    <div class="col-md-10">
                                        <?php
                                        $selected_status = array();
                                        $attributes = array('id' => 'room_no', 'class' => 'select2-search');
                                        ?>
                                        <?php echo form_dropdown('room_no', $room_lists, $selected_status, $attributes); ?>

                                    </div>


                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label">Rent Type.</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="hidden">
                                        <input placeholder="Rent Type"
                                               class="form-control ignore"
                                               type="text" readonly id="renttype">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label block">Arr Date.</label>
                                    </div>
                                    <div class="col-md-7">

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right"
                                                   id="checkindate" readonly>

                                        </div>

                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label block">Pax.</label>
                                    </div>

                                    <div class="col-md-2">
                                        <input type="text" placeholder="Pax"
                                               class="form-control" readonly id="pax_amt">

                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label block">Dep Date.</label>
                                    </div>
                                    <div class="col-md-7">

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right"
                                                   id="checkoutdate" name="checkoutdate"/>


                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <label class="control-label">Days</label>
                                    </div>
                                    <div class="col-md-2">
                                        <input placeholder="Days"
                                               class="form-control"
                                               type="text" id="tvalue" readonly>

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Name</label>
                                    </div>

                                    <div class="col-md-5">
                                        <input
                                            id="guest_first_name"
                                            name="guest_first_name"
                                            type="text"
                                            class="form-control"
                                            placeholder="First Name"
                                            readonly
                                            />
                                    </div>
                                    <div class="col-md-5">

                                        <input
                                            id="guest_last_name"
                                            name="guest_last_name"
                                            type="text"
                                            class="form-control"
                                            placeholder="Last Name"
                                            readonly
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Address</label>
                                    </div>
                                    <div class="col-md-10">

                                        <textarea
                                            id="address"
                                            rows="4"
                                            cols="5"
                                            class="form-control resize-ver"
                                            placeholder="Address here" readonly>
                                        </textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-10">

                                        <input
                                            id="email"
                                            type="text"
                                            class="form-control"
                                            placeholder="Email"
                                            readonly
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>-->

                            <div class="form-group">
                                <div class="row">


                                    <div class="col-md-2">
                                        <label class="control-label block">Gender</label>
                                    </div>
                                    <div class="col-md-4">

                                        <input type="text"  placeholder="Gender" class="form-control" id="gender" readonly>



                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label block">Mobile No.</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input placeholder="Mobile No"
                                               id="mobile"
                                               class="form-control"
                                               type="number" readonly
                                               >

                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label block">Guest
                                            Identity.</label>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row photo" id="photo_view" style="display: none;">
                                            <span class="help-block"></span>
                                            <div class="col-md-12" style="height: 150px;width: 150px;">
                                                <img height="150" width="150" id="proof_img">
                                                <label id="label-photo1"></label>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div id="result_bill" class="">
                                <div class="row">
                                    <div class="col-md-8">
                                        <label class="control-label block">* SR. No.</label>&nbsp;
                                        <input type="text"  id="billno" name="bill_no" readonly>
                                        <input type="hidden" name="transaction_id" id="transaction_id">
                                    </div>
                                    <div class="col-md-2 align"><label class="control-label block">Per
                                            Day</label></div>
                                    <div class="col-md-2 align"><label
                                            class="control-label block">Total</label>
                                    </div>

                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">Room
                                            Rent</label></div>
                                    <div class="col-md-2 align"><label
                                            class="total_rent">0.00</label></div>
                                    <div class="col-md-2 align"><label
                                            class="total_rent_lbl">0.00</label>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">Ex.
                                            Bed Rent</label></div>
                                    <div class="col-md-2 align"><label
                                            class="extra_bed_lbl">0.00</label>
                                    </div>
                                    <div class="col-md-2 align"><label
                                            class="extra_bed_lbl_total">0.00</label>
                                    </div>
                                </div>
                                <div class="row" style="color: red;">
                                    <div class="col-md-8"><label class="control-label block">
                                            Discont (-)</label></div>
                                    <div class="col-md-2 align"><label
                                            class="total_dis_lbl">0.00</label>
                                    </div>
                                    <div class="col-md-2 align"><label
                                            class="total_dis_lbl_dy">0.00</label>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label
                                            class="control-label block">Net
                                            Rent</label></div>
                                    <div class="col-md-offset-2 col-md-2 align " style="border-left:none ">
                                        <label
                                            class="net_rent">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">
                                            Service Tax on Room Tariff (%)</label></div>
                                    <div class="col-md-2 align"><label
                                            class="stax_lbl">0.00</label></div>
                                    <div class="col-md-2 align"><label
                                            class="stax_total_lbl">0.00</label>
                                    </div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">Luxury
                                            Tax</label></div>

                                    <div class="col-md-2 align"><label class="lx_tax_lbl">0.00</label>
                                    </div>
                                    <div class="col-md-2 align"><label
                                            class="lx_tax_lbl">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">Ex.Bed
                                            Tax</label></div>
                                    <div class="col-md-2 align"><label
                                            class="extrabed_tax_lbl">0.00</label>
                                    </div>
                                    <div class="col-md-2 align"><label
                                            class="extrabed_total_tax_lbl">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-10"><label class="control-label block">Total
                                            Rent Charge</label></div>
                                    <div class="col-md-2 align"><label
                                            class="total_rent_g_charge_lbl">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label class="control-label block">Room
                                            Service Amount</label></div>
                                    
                                    <div class="col-md-offset-2 col-md-2 align"><label
                                            class="room_service_g_lbl">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8 "><label class="control-label block">
                                            Service Tax on Room Service (%)</label></div>
                                    <div class="col-md-2 align"><label
                                            class="stax_room_service_lbl">0.00</label>
                                    </div>
                                    <div class="col-md-2 align"><label
                                            class="stax_room_service_g_lbl">0.00</label></div>
                                </div>
                                <div class="row" >
                                    <div class="col-md-8"><label
                                            class="control-label block">Total
                                            Bill Amount</label></div>
                                    <div class="col-md-offset-2 col-md-2 align" style="border-left:none "><label
                                            class="total_bill_amount">0.00</label></div>

                                </div>
                                <div class="row" style="color: red;">
                                    <div class="col-md-8"><label
                                            class="control-label block">Advance</label>
                                    </div>
                                    <div class="col-md-offset-2 col-md-2 align" style="border-left:none "><label
                                            id="advance_lbl">0.00</label>
                                    </div>
                                </div>
                                <div class="row no-border-bottom" >
                                    <div class="col-md-8"><label class="control-label block">Final
                                            Receivable</label></div>
                                    <div class="col-md-offset-2 col-md-2 align" style="border-left:none "><label
                                            class="final_amount">0.00</label></div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="text-right">
                        <a href="checking/checkout" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <input type="hidden" name="final_receive_amount" id="final_receive_amount" value="0">
                        
                        <input type="hidden" name="service_total_amount" id="service_total_amount" value="0">
                        
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    var amount_arr=[];
    var service_arr=[];
    var service_total_amount=0;
    var advance_amount=0;
    var fetch_rec= [];
    var bill_rec= [];
    jQuery(function () {
        jQuery("#room_no").change(function () {
        var obj = jQuery(this);
        var room_id = obj.val();
        if(room_id==''){
            clear_data();
            return;
        }
        
        start_loading(obj);
        $.ajax({
            url: "<?php echo base_url('checkout/check_transaction') ?>",
            type: "GET",
            dataType: "JSON",
            data: {"room_id": room_id},
            success: function (data) {
                stop_loading(obj);
                if (data.type == 'found')
                {
                    fetch_rec = data.record;
                    bill_rec=data.bill_rec;
                    filldata(fetch_rec);
                    amount_arr=[];
                    $.each(bill_rec, function (key,val) {
                        if(val.priority<=4){
                            amount_arr[val.priority]=val.price;
                        }
                        if(val.priority>4){
                            service_arr[val.description]=val.price;
                            service_total_amount = parseFloat(service_total_amount) +  parseFloat(val.price);
                        }
                    });
                    
                    $('#service_total_amount').val(service_total_amount);
                    total_days_db=calculate_days(fetch_rec.checkin_date,fetch_rec.checkout_date)
                    fill_bill_rec(total_days_db);
                } else
                {
                }
            }
        });
    });
    $('#checkoutdate').daterangepicker({
        "singleDatePicker": true,
        "showDropdowns": true,
        "showWeekNumbers": true,
        "timePicker": true,
        "autoApply": true,
        locale: {
            format: 'YYYY-MM-DD HH:mm'
        }
    },
    function (start, end) {
        var start_date=$("#checkindate").val();
        var end_date=start;
        total_days_db=calculate_days(start_date,end_date)
        fill_bill_rec(total_days_db);
    });
    });
    
    
    function filldata(prev_data) {
        $('#transaction_id').val(prev_data.id);
        $('#billno').val(prev_data.bill_no);
        $('#renttype').val(prev_data.name);
        $('#checkindate').val(prev_data.checkin_date);
        $('#checkoutdate').val(prev_data.checkout_date);
        calculate_days(prev_data.checkin_date,prev_data.checkout_date);
        var drp = $('#checkoutdate').data('daterangepicker');
        drp.startDate = moment(prev_data.checkout_date);
        drp.updateView();
        drp.updateCalendars();
        $('#pax_amt').val(prev_data.pax);
        $('#mobile').val(prev_data.mobile_number);
        $('#guest_first_name').val(prev_data.first_name);
        $('#guest_last_name').val(prev_data.last_name);
        $('#address').val(prev_data.address);
        $('#email').val(prev_data.email);
        if (prev_data.gender == 1) {
            $('#gender').val("Male");
        } else {
            $('#gender').val("Female");
        }

        $("#photo_view").show();
        $("#proof_img").attr("src", prev_data.proof);
    }
    function fill_bill_rec(total_days_default){
       
        if(typeof(amount_arr[4])==='undefined'){
            advance_amount=0;
        }
        else{
            advance_amount=amount_arr[4];
        }
        var room_amount= fetch_rec.price;
        var total_days= total_days_default;        
        var extrabedcharge= parseFloat(amount_arr[1]);
        var totaldis= parseFloat(amount_arr[2]) * parseFloat(total_days) ;
        var net_bal = (((parseFloat(room_amount)+ parseFloat(extrabedcharge)) * parseInt(total_days)) - parseFloat(totaldis));
        var final_amount_before_advance=net_bal+service_total_amount;
        var final_amount_pr=net_bal+service_total_amount;
        var final_amount=final_amount_before_advance-parseFloat(advance_amount);
        $(".total_rent").html(fetch_rec.price);
        $(".total_rent_lbl").html(fetch_rec.price*total_days);
        $(".extra_bed_lbl").html(extrabedcharge);
        $(".extra_bed_lbl_total").html(extrabedcharge*total_days);
        $(".total_dis_lbl").html(parseFloat(amount_arr[2]));
        $(".total_dis_lbl_dy").html(totaldis);
        $(".net_rent").html(net_bal);
        $(".room_service_g_lbl").html(service_total_amount);
        $(".total_bill_amount").html(final_amount_pr);
        $("#advance_lbl").html(parseFloat(advance_amount));
        $(".final_amount").html(final_amount);
        $("#final_receive_amount").val(final_amount);
    }
    function clear_data(){
        jQuery("#save")[0].reset();
    }
    function calculate_days(start, end) {
        var a = moment(start);
        var b = moment(end);
        var duration = moment.duration(b.diff(a));
        var days = duration.asHours();
        totalHours = parseInt(Math.abs(Math.ceil(days)));
        daysCount = totalHours / 24;
        daysCount=Math.round(daysCount);
        if (daysCount <= 0)
        {
            daysCount = 1;
        }
        jQuery("#tvalue").val(Math.round(daysCount));
        return daysCount;
    }
    function start_loading(obj)
    {
        obj.addClass("textbox_loading");
        obj.parents('form').find("button[type='submit']").prop("disabled", true);
    }
    function stop_loading(obj) {
        obj.removeClass("textbox_loading");
        obj.parents('form').find("button[type='submit']").prop("disabled", false);
    }
    
</script>