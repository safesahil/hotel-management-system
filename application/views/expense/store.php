<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="expense/store" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                     
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                       
                </div>
               

                <div class="panel-body">
                     <div class="form-group text-right">
                    <label class="text-right label-info label la-3x" style="font-size: 14px;">
                    Remaining Balance :
                    <strong>
                        <u>
                        <?php
                        echo number_format($total_sum_amount_income,2);
                        ?>
                        </u>
                        </strong>
                </label> 
                </div>
                    <div class="form-group">
                        <label>Amount <span class="text-danger">*</span></label>
                        <input
                            id="amount"
                            name="amount"
                            type="number"
                            class="form-control"
                            placeholder="Amount"
                            
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea
                            id="description"
                            name="description"
                            rows="5"
                            cols="5"
                            class="form-control resize-ver"
                            placeholder="Enter description here"></textarea>
                    </div>
                    <div class="text-right">
                        <a href="expense" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>

                        <button type="submit" class="btn btn-primary" name="saveClose" value="1" >Save<i class="icon-sync position-right"></i></button>
                        <button type="submit" class="btn btn-primary" >Save and Close <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
    </div>
</form>
<!-- /basic layout -->

</div>
</div>
<script>
    $(function () {
        var rules = {
            amount: {
                required: true,
                maxlength:<?php echo $total_sum_amount_income ?>
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);

        jQuery('#type').change(function () {
            var c_val = jQuery(this).val();
            if (c_val != null) {
                jQuery("#type-error").remove();
            }
        });

    });
</script>