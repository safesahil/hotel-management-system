<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>

                <div class="heading-elements">
                    <label class="label-info label la-3x" style="font-size: 14px;padding:5px 10px 5px 10px;margin-top: 2px">
                        Remaining Balance :
                        <strong>
                            <u>
                                <?php
                                echo number_format($total_sum_amount_income, 2);
                                ?>
                            </u>
                        </strong>
                    </label>    
                    <a href="expense" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="expense/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Cash Expense</a>
                    <a href="expense/store" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Transfer Cash to store</a>
                    <a href="expense/save?transfer=bank" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Transfer Cash to bank</a>
                </div>

            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>

                                <tr>
                                    <th>Date</th>
                                    <th>Amount</th>
                                    <th>Details</th>
                                    <th>Type</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="">
                                        Total
                                    </th>
                                    <th>
                                        <?php
                                        echo number_format($total_sum_amount, 2);
                                        ?>
                                    </th>
                                    <th colspan="2">
                                        &nbsp;
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions') {
                if (title === 'Date') {
                    $(this).html('<input type="text" class="form-control daterange-basic-datatable" placeholder="' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                }
            }
        });
        var d = new Date();
        var table = $('#dttable').DataTable({
            dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                        title: "Expense Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                        title: "Expense Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                        title: "Expense Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                        title: "Expense Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    }

                ],
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            ordering:false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            "columns": [
                {
                    data: 'payments_created',
                    visible: true,
                    name: 'payments.created',
                },
                {
                    'data': 'payments_amount',
                    "visible": true,
                    "name": 'payments.amount',
                    render: function (data, type, full, meta) {
                        return number_format_js_amount(full.payments_amount, 2, '.', ',');
                    }
                },
                {
                    'data': 'payments_description',
                    "visible": true,
                    "name": 'payments.description',
                },
                
                {
                    'data': 'payments_name',
                    "visible": true,
                    "name": 'bank.name',
                    render:function (data, type, full, meta){
                        if(full.bank_name==null){
                            return '<span class="label label-success label-rounded">BY CASH</span>';
                        }
                        else{
                            return '<span class="label label-primary label-rounded">BY BANK ( '+full.bank_name +' )</span>';
                        }
                    }
                },
            ],
            
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });

                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': "<?php echo base_url() . 'expense/filter' ?>",
                    'data': req_obj,
                    'success': function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                console.log(this.value);
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });
        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>