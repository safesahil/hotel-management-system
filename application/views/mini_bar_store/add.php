<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="mini_bar_store/add/<?php echo $room['id'] ?>/<?php echo $item['id'] ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Room <span class="text-danger">*</span></label>
                                <input
                                    id="room_id"
                                    name="room_id"
                                    type="text"
                                    class="form-control"
                                    placeholder="Room"
                                    readonly="readonly"
                                    value="<?php echo isset($room['name']) ? $room['name'] : '' ?>"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Item <span class="text-danger">*</span></label>
                                <input
                                    id="item_id"
                                    name="item_id"
                                    type="text"
                                    class="form-control"
                                    placeholder="Item"
                                    readonly="readonly"
                                    value="<?php echo isset($item['item_name']) ? $item['item_name'] : '' ?>"/>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>In Date <span class="text-danger">*</span></label>
                                <input
                                    id="log_date"
                                    name="log_date"
                                    type="text"
                                    class="form-control daterange-single-basic"
                                    placeholder="In Date"
                                    value="<?php echo set_value('log_date') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Quantity <span class="text-danger">*</span> <span id="stock_qty_lbl" class="text-danger text-size-mini">Max : <?php echo $item_stock . ' ' . $item_unit?></span></label>
                                <input
                                    id="quantity"
                                    name="quantity"
                                    type="text"
                                    class="form-control"
                                    placeholder="Quantity"
                                    value="<?php echo set_value('quantity') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Units <span class="text-danger">*</span></label>
                                <?php
                                $selected_unit = set_value('unit');
                                $attributes = array('id' => 'unit', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('unit', $units_options, $selected_unit, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Price</label>
                                <input
                                    id="price"
                                    name="price"
                                    type="text"
                                    class="form-control"
                                    placeholder="Price"
                                    readonly="readonly"
                                    value="<?php echo isset($item['purchase_price']) ? $item['purchase_price'] : '0' ?>"/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <?php
                                $status_options = array('1' => 'Active', '0' => 'Inactive');
                                $selected_status = set_value('status');
                                $attributes = array('id' => 'status', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea
                                    id="description"
                                    name="description"
                                    rows="5"
                                    cols="5"
                                    class="form-control resize-ver"
                                    placeholder="Enter description here"><?php echo set_value('description') ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="mini_bar_store/details/<?php echo $room['id'] ?>" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            log_date: {
                required: true,
            },
            room_id: {
                required: true,
            },
            item_id: {
                required: true,
            },
            unit: {
                required: true,
            },
            quantity: {
                required: true,
                min: 1,
                max: 100000,
            },
            status: {
                required: true
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);
    });
</script>