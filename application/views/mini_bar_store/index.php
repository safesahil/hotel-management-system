<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <a href="mini_bar_store" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>Room id</th>
                                    <th>Room no.</th>
                                    <th>Availability.</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                                <tr>
                                    <th>Room id</th>
                                    <th>Room no.</th>
                                    <th>Availability.</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var avai_arr = {
            '<?php echo ROOM_AVAILABLE ?>': '<?php echo ROOM_AVAILABLE_STR ?>',
            '<?php echo ROOM_OCCUPIED ?>' : '<?php echo ROOM_OCCUPIED_STR ?>',
        };
        // Setup - add a text input to each footer cell
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions') {
                $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
            }
        });

        //datatables
        var table = $('#dttable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            columns: [
                {
                    data: 'rooms_id',
                    visible: true,
                    name: 'rooms.id',
                },
                {
                    data: 'rooms_name',
                    visible: true,
                    name: 'rooms.name',
                },
                {
                    'data': 'rooms_availability_status',
                    "visible": true,
                    "name": 'rooms.availability_status',
                    "render": function (data, type, full, meta) {
                        var status = '<span class="label label-danger label-rounded"> ----- </span>';
                        if (full.rooms_availability_status === '1') {
                            status = '<span class="label label-success label-rounded"> Available </span>';
                        } else if (full.rooms_availability_status === '0') {
                            status = '<span class="label label-warning label-rounded"> Occupied </span>';
                        }
                        return status;
                    }
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        actionBtns = '<a href="mini_bar_store/details/' + full.id + '" class="btn btn-primary btn-rounded btn-sm action-btns tooltip-show" title="Show bar details"><i class="fa fa-info-circle"></i></a>';
                        return actionBtns;
                    }
                }
            ],
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
                this.api().columns().every(function (index) {
                    if (tableColumns[index].name == 'rooms.availability_status') {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">Select</option></select>')
                                .appendTo($('th:nth-child(' + (index + 1) + '):first').empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? val : '', true, false)
                                            .draw();
                                });
                        if (tableColumns[index].name == 'rooms.availability_status') {
                            for (var key in avai_arr) {
                                if (avai_arr.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + avai_arr[key] + '</option>');
                                }
                            }
                        }
                    }
                });
            },
            fnServerData: function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['col_eq'] = ['rooms.availability_status'];
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: "<?php echo base_url() . 'mini_bar_store/filter' ?>",
                    data: req_obj,
                    success: function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>