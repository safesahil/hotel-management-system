<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <a href="mini_bar_store" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                    <a href="mini_bar_store/details/<?php echo $room['id'] ?>" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="mini_bar/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add New Item</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>GRN</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Quantity</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                                <tr>
                                    <th>GRN</th>
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Quantity</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions' && title !== 'Quantity') {
                $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
            }
        });

        //datatables
        var table = $('#dttable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            columns: [
                {
                    data: 'items_id',
                    visible: true,
                    name: 'items.id',
                },
                {
                    data: 'items_name',
                    visible: true,
                    name: 'items.name',
                },
                {
                    data: 'items_category_name',
                    visible: true,
                    name: 'items_category.name',
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var qtyStr = full['stock_qty'];
                        qtyStr += ' ' + full.item_inventory_converted_unit;
                        return qtyStr;
                    }
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        actionBtns = '<a href="mini_bar_store/add/<?php echo $room['id'] ?>/' + full.items_id + '" class="btn btn-primary btn-rounded btn-sm action-btns tooltip-show" title="Add quantity in bar"><i class="fa fa-plus"></i></a>';
<?php if ($room['availability_status'] === ROOM_OCCUPIED) { ?>
                            actionBtns += '<a href="mini_bar_store/consume/<?php echo $room['id'] ?>/' + full.items_id + '" class="btn btn-warning btn-rounded btn-sm action-btns tooltip-show" title="Consume from bar"><i class="fa fa-minus"></i></a>';
<?php } ?>
                        return actionBtns;
                    }
                }
            ],
            fnServerData: function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: "<?php echo base_url() . 'mini_bar_store/filter_items/' . $room['id'] ?>",
                    data: req_obj,
                    success: function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>