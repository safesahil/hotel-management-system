<?php
$trnasaction_type=$this->uri->segment(2);
?>
<form id="form" method="post">
    <div class="table-responsive ">
        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
            <thead>
                 <tr>
                    <th>SR.NO</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Number</th>
                    <th>Proof</th>
                    <th>Search Room</th>
                    <th>Checking</th>
                    <?php
                    if($trnasaction_type=='checkout'){
                        echo '<th>Checkout</th>';
                    }
                    else{
                        echo '<th>Expected Checkout</th>';
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                if($record):
                    foreach ($record as $single_row):
                    ?>
                    <tr>
                        <td>
                          <?php
                          echo $single_row['bill_no'];
                          ?>
                        </td>
                        
                        
                        <td>
                          <?php
                          echo $single_row['first_name'].' '.$single_row['last_name'];
                          ?>
                        </td>
                        
                        <td>
                          <?php
                          echo $single_row['address']
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $single_row['mobile_number']
                          ?>
                        </td>
                        <td>
                            <a href="<?php echo $single_row['proof'] ?>" target="_blank">
                                <img height="100" width="100" src="<?php echo $single_row['proof'] ?>" alt=""  />
                            </a>
                        </td>
                        <td>
                          <?php
                          echo $single_row['room_name'].'<span class="label label-success label-rounded">'.$single_row['name'].'</span>';
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $single_row['checkin_date']
                          ?>
                        </td>
                        <td>
                          <?php
                          echo $single_row['checkout_date']
                          ?>
                        </td>
                        
                    </tr>
                    <?php
                    endforeach;
                endif;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <th>SR.NO</th>                  
                    <th>Name</th>
                    <th>Address</th>
                    <th>Number</th>
                    <th>Proof</th>
                    <th>Room</th>
                    <th>Checking</th>
                    <?php
                    if($trnasaction_type=='checkout'){
                        echo '<th>Checkout</th>';
                    }
                    else{
                        echo '<th>Expected Checkout</th>';
                    }
                    ?>
                </tr>
            </tfoot>
        </table>
    </div>
</form>       
<script>
    $(function(){
         var d = new Date();
        var table = $('#dttable').DataTable({
            dom: 'Blfrtip',
                buttons: [
                    {
                        extend: 'csv',
                        text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                        title: "Transaction Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'excel',
                        text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                        title: "Transaction Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'pdfHtml5',
                        text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                        title: "Transaction Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    },
                    {
                        extend: 'print',
                        text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                        title: "Transaction Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate()
                    }

                ],
                 "order": [[ 0, "desc" ]],
                 "aoColumnDefs": [
                    { 'bSortable': false, 'aTargets': [ 1,2,3,4,5,6,7 ] }
                 ],
                
        });
        table.columns.adjust().draw();
         $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            title=title.trim();
            if (title !== 'SR.NO' && title!=='Proof' && title !== 'Checking' && title !== 'Expected Checkout' && title !== 'Checkout') {
                $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
            }
        });
         table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });
    })
    
    </script>