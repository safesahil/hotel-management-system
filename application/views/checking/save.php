<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <style>
            .div_heading .div_title{
                padding: 10px;
                border-right: 1px #333;
            }
        </style>
        <form id="save" name="save" action="checking/save" method="POST" enctype="multipart/form-data" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>SR. No <span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                        <input
                                            id="bill_no"
                                            name="bill_no"
                                            type="text"
                                            class="form-control"
                                            placeholder="Bill Number"
                                            readonly
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Mobile No<span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-10">

                                        <input
                                            id="mobile_number"
                                            name="mobile_number"
                                            type="number"
                                            class="form-control"
                                            placeholder="Enter Mobile Number and press enter"
                                            
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Name <span class="text-danger">*</span></label>
                                    </div>
                                    
                                    <div class="col-md-5">
                                        <input
                                            id="guest_first_name"
                                            name="guset_info[first_name]"
                                            type="text"
                                            class="form-control"
                                            placeholder="First Name"
                                            />
                                    </div>
                                    <div class="col-md-5">

                                        <input
                                            id="guest_last_name"
                                            name="guset_info[last_name]"
                                            type="text"
                                            class="form-control"
                                            placeholder="Last Name"
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Address<span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-10">

                                        <textarea
                                            id="address"
                                            name="guset_info[address]"
                                            rows="4"
                                            cols="5"
                                            class="form-control resize-ver"
                                            placeholder="Enter Address here"></textarea>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
<!--                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Email</label>
                                    </div>
                                    <div class="col-md-10">

                                        <input
                                            id="email"
                                            name="guset_info[email]"
                                            type="text"
                                            class="form-control"
                                            placeholder="Enter Email"
                                            />
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>-->
<!--                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Location<span class="text-danger">*</span></label>
                                    </div>
                                    <div class="col-md-10">
                                         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAyOr_bRsAdEvgUsMtaKo2FzB34rhS6_CM&sensor=false&libraries=places&language=en"></script>
                                        <input
                                            id="location"
                                            name="guset_info[location]"
                                            type="text"
                                            class="form-control"
                                            placeholder="Search Location"
                                            />
                                        <script>
                                           var input = document.getElementById('location');
                                           var autocomplete = new google.maps.places.Autocomplete(input);
                                        </script>
                                         
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Gender</label>
                                    </div>
                                    <div class="col-md-10">

                                         <?php
                                        $gender_options = array(1 => MALE, 2=> FEMALE);
//                                            $selected_status = (isset($prev_data) && isset($prev_data['status'])) ? $prev_data['status'] : set_value('status');
                                        $selected_status = array();
                                        $attributes = array('id' => 'gender', 'class' => 'select2-basic');
                                        ?>
                                        <?php echo form_dropdown('guset_info[gender]', $gender_options, $selected_status, $attributes); ?>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label block">Agent</label>
                                    </div>
                                    <div class="col-md-4">
                                        <?php
                                        $agent_options = array('' => 'Select Agent');
                                        if (isset($agents) && count($agents) > 0) {
                                            $agent_options  = $agents;
                                        }
                                        $selected_item_category = array();
                                        $attributes = array('id' => 'agent_id', 'class' => 'select2-basic');
                                        ?>
                                        <?php echo form_dropdown('agent_id', $agent_options, $selected_item_category, $attributes); ?>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label block">Tour</label>
                                    </div>
                                    <div class="col-md-4">
                                        <?php
                                        $tour_options = array('' => 'Select Tour Company');
                                        if (isset($tours) && count($tours) > 0) {
                                            $tour_options  = $tours;
                                        }
                                        $selected_item_category = array();
                                        $attributes = array('id' => 'tour_id', 'class' => 'select2-basic');
                                        ?>
                                        <?php echo form_dropdown('tour_id', $tour_options, $selected_item_category, $attributes); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Proof <span class="text-danger">*</span>
                                            
                                        </label>
                                
                                    </div>

                                    <div class="col-md-10">
                                        <input type="file" id="proof" name="proof" class="file-input" data-show-preview="false" data-show-upload="false" data-show-caption="true" />
                                        
                                        <div class="row photo" id="photo_view" style="display: none;">
                                            <span class="help-block"></span>
                                            <div class="col-md-12" style="height: 150px;width: 150px;">
                                                <img height="150" width="150" id="proof_img">
                                                <label id="label-photo1"></label>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        Room Type
                                    </div>
                                    <div class="col-md-10">
                                        <?php
                                        $r_c_options = array('' => 'Select Room Type');
                                        if (isset($rooms) && count($rooms) > 0) {
                                            $r_c_options = $rooms;
                                        }
                                        $selected_item_category = array();
                                        $attributes = array('id' => 'rooms', 'class' => 'select2-basic');
                                        ?>
                                        <?php echo form_dropdown('room_type_id', $r_c_options, $selected_item_category, $attributes); ?>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        Room number
                                    </div>
                                    <div class="col-md-10">
                                        <select id="rooms_no" name="room_id" class="form-control">
                                            <option>Select Room No.</option>
                                        </select>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label class="control-label block">No. of
                                            Pax.</label>
                                    </div>

                                    <div class="col-md-8">
                                        <input type="number" value="0" min="0" placeholder="Pax" class="form-control" name="total_pax" id="paxAmount">
                                        <span class="help-block"></span>
                                    </div>
                                    <div class="col-md-2">
                                        <label class="control-label block">
                                            <input type="checkbox" id="paxDetails"> Details
                                        </label>
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>       
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Checking Date</label>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control " id="checking_date" name="checking_date" readonly value="<?php echo date("Y-m-d H:i") ?>" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Checkout Date</label>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control" name="checkout_date" id="checkout_date" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        <label>Extra Bed</label>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="extra_bed" id="extra_bed" value="0">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>Extra Bed Rent</label>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="extra_bed_rent" id="extra_bed_rent" value="0">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-2">
                                        Discount per Day
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="discount_per_day" id="discount_per_day" value="0">
                                    </div>
                                    <div class="col-md-2">
                                        Advance
                                    </div>
                                    <div class="col-md-4">
                                        <input type="text"class="form-control" name="advance" id="advance" value="0">
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="panel panel-default">

                                    <div class="panel-body" style="padding: 20px 0;font-size: 12px;font-weight: bold;text-align: center">
                                        <div class="col-md-1">

                                            <div class="divTableCell">Rent</div>
                                            <span class="help-block"></span>

                                            <div class="divTableCell"><span
                                                    id="total_rent">0.00</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="divTableCell">Total Days</div>
                                            <span class="help-block"></span>
                                            <div class="divTableCell"><span id="total_dy">0</span>
                                            </div>

                                        </div>
                                        <div class="col-md-1">
                                            <div class="divTableCell">Discount</div>
                                            <span class="help-block"></span>
                                            <div class="divTableCell"><span
                                                    id="total_dis_lbl">0.00</span>

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="divTableCell">Ex.Bed</div>
                                            <span class="help-block"></span>
                                            <div class="divTableCell"><span
                                                    id="extra_bed_lbl">0.00</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="divTableCell">Ex.Bed Rent</div>
                                            <span class="help-block"></span>
                                            <div class="divTableCell"><span
                                                    id="extra_bed_rent_lbl">0.00</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="divTableCell">Advance</div>
                                            <span class="help-block"></span>
                                            <div class="divTableCell"><span
                                                    id="advance_amnt_lbl">0.00</span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="divTableCell">Total</div>
                                            <span class="help-block"></span>
                                            <input type="hidden" name="net_amt" id="net_amt">
                                            <div class="divTableCell"><span
                                                    id="net_rent">0.00</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right">
                        <a href="checking" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <input type="hidden" name="rent_amount" id="rent_amount" value="0">
                        <input type="hidden" name="total_days" id="total_days" value="1">
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog" style="width:850px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Pax Add</h4>
                        </div>
                        <div class="modal-body">
                            <input type='hidden' class='pax_user_id' name='pax_user_id'>
                            <div class='row text-center div_heading' style='background-color: #f9f9f9'>
                                <div class='col-md-3 div_title'>
                                    <label class='control-label'>First name</label>
                                </div>
                                <div class='col-md-3 div_title'>
                                    <label class='control-label'>Family name</label>
                                </div>
                                <div class='col-md-3 div_title'><label class='control-label'>Age</label></div>
                                <div class='col-md-3 div_title'><label class='control-label'>Gender</label></div>
                            </div>
                            <div class='clearfix'>&nbsp;</div>
                            <div class="result">
                            </div>
                        </div>
                        <div class="modal-footer">
                            
                            <button type="submit" id='btnaddpax' class='btn btn-primary'>Add Pax</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
    
</div>
<script>
    jQuery(function () {
        var price_val;
        getBillNo();
        
        $(document).on('change', '#rooms', function () {

            var room_cat_id = jQuery(this).val();
            var obj=jQuery(this);
            start_loading(obj)
            if (room_cat_id) {
                jQuery.ajax({
                    dataType: 'json',
                    url: '<?php echo base_url('checking/findRoom') ?>',
                    data: {"room_cat_id": room_cat_id},
                    type: 'POST',
                    success: (function (fetchData) {
                        stop_loading(obj);
                        jQuery("#rooms_no").html('');
                        jQuery("#rooms_no").append(fetchData);
                    }),
                });
            }
        });
        $('input[name="checkout_date"]').daterangepicker({
            minDate: moment(),
            "singleDatePicker": true,
            "showDropdowns": true,
            "showWeekNumbers": true,
            "timePicker": true,
            "autoApply": true,

            locale: {
                format: 'YYYY-MM-DD HH:mm'
            }
        },
                function (start, end) {
                    var st = jQuery("#checking_date").val();
                    var a = moment(st);
                    var b = moment(start);
                    var duration = moment.duration(b.diff(a));
                    var days = duration.asHours();
                    totalHours = parseInt(Math.abs(Math.ceil(days)));
                    daysCount=totalHours / 24;
                    console.log(daysCount);
                    if (daysCount <= 0)
                    {
                        daysCount=1;
                    }
                    jQuery("#total_days").val(Math.round(daysCount));
                    
                    total_rent_count();
                    
                });
        $('#checkoutdate').change(function () {
        });

        jQuery(document).on('change','#mobile_number',function(event){
            event.preventDefault();
            var obj=jQuery(this);
            var val=obj.val();
            
            start_loading(obj);
            $.ajax({
                url: "<?php echo base_url('checking/check_user_details')?>",
                type: "POST",
                dataType: "JSON",
                data:{"user_mobile_no":val},
                success: function (data) {
                    
                    if(data.type=='found'){
                    console.log(data.type);    
                        stop_loading(obj);
                        filldata(data.prev_rec);
                    }
                    else{
                        stop_loading(obj);
                        swal({
                            title: val+ " not found in records",
                            text: "Add Customer and update cheking info",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#66bb6a",
                            confirmButtonText: "Yes, Added!"
                        }, function () {
                           
                        });
                    }
                }
            });
        });
        
        
        $('#paxDetails').click(function () {
                    $('.result').html('');
                    if ($(this).is(':checked')) {
                        var pax_amt = $('#paxAmount').val();
                        if(pax_amt > 0){
                            $('#myModal').modal('show');
                            var res = $('.result');
                            for (i = 0; i < parseInt(pax_amt); i++) {
                                res.append("" +
                                    "<div class='row form-horizontal div_heading'>" +
                                    "<div class='col-md-3 div_title'>" +
                                    "<input required type='text' class='form-control' placeholder='Enter First name' name='user_pax[" + i + "][first_name]' id='user_pax_fname"+i+"' /></div>" +
                                     "<div class='col-md-3 div_title'>" +
                                    "<input required type='text' class='form-control' placeholder='Enter Family name' name='user_pax[" + i + "][last_name]' id='user_pax_lname"+i+"' /></div>" +
                                    "<div class='col-md-3 div_title'><input required type='text' class='form-control' placeholder='Age' name='user_pax[" + i + "][age]' id='user_pax_age"+i+"' /></div>" +
                                    "<div class='col-md-3 div_title'><select class='form-control' name='user_pax[" + i + "][gender]' >" +
                                    " <option value='1'>Male</option>" +
                                    "<option value='2'>Female</option>" +
                                    "</select> </div>" +
                                    "<div class='clearfix'>&nbsp;</div>");
                            }
                            $(this).prop('checked', false);
                        }
                        else{
                            swal("Error","Please add pax amount","error");
                             $(this).prop('checked', false);
                        }
                        
                    }
                    else {
                        $('#myModal').modal('hide');
                    }
                });
        $('#btnaddpax').click(function(e){
            e.preventDefault();
            $('#myModal').modal('hide');
        });
        
        $('#rooms_no').change(function(){
            get_room_price($(this));
        })
        $('#extra_bed , #extra_bed_rent , #discount_per_day , #advance').keyup(function(){
            total_rent_count();
        });
    });
    function getBillNo(){
    var obj=$('input[name="bill_no"]');
    start_loading(obj);
        $.ajax({
            url: "<?php echo base_url('checking/genrate_bill_no')?>",
            type: "GET",
            dataType: "JSON",
            success: function (data) {
                stop_loading(obj);
                $('input[name="bill_no"]').val(data);
            }
        });
    }
    function get_room_price(obj){
        var room_id=obj.val();
        var room_rec;
        start_loading(obj);
            $.ajax({
                url: "<?php echo base_url('checking/room_deatails')?>",
                type: "POST",
                dataType: "JSON",
                data:{"room_id":room_id},
                success: function (data) {
                     if(data.type=='found'){
                        stop_loading(obj);
                        reset_fields();
                        room_rec=data.prev_rec;
                        jQuery('#rent_amount').val(room_rec.price);
                        
                        total_rent_count();
                    }
                    else{
                        stop_loading(obj);
                    }
                }
        });
    }
    
    $(function () {
        var rules = {
            mobile_number: {
                required: true,
                minlength:6,
            },
            "guset_info[first_name]":{
                required: true,
            },
            "guset_info[address]":{
                required: true,
            },
            "guset_info[age]":{
                required: true,
            },
        };

        initValidation('.save-form', rules);
    });
    function filldata(prev_data){
        $('#guest_first_name').val(prev_data.first_name);
        $('#guest_last_name').val(prev_data.last_name);
        $('#address').val(prev_data.address);
        $('#email').val(prev_data.email);
        $('#gender').val(prev_data.gender).trigger("change.select2");
        $("#photo_view").show();
        $("#proof_img").attr("src",prev_data.proof);
    }
    function start_loading(obj)
    {
        obj.addClass("textbox_loading");
        obj.parents('form').find("button[type='submit']").prop("disabled",true);
    }
    function stop_loading(obj){
        obj.removeClass("textbox_loading");
        obj.parents('form').find("button[type='submit']").prop("disabled",false);
    }
    function reset_fields(){
        jQuery('#rent_amount').val('0');
        jQuery('#total_days').val('1');
        jQuery('#extra_bed').val('0');
        jQuery('#extra_bed_rent').val('0');
        jQuery('#discount_per_day').val('0');
        jQuery('#advance').val('0');
        jQuery("#net_amt").val('0');
    }
    function total_rent_count(){
        var room_amount= jQuery('#rent_amount').val();
        var total_days= jQuery('#total_days').val();
        var extra_bed= jQuery('#extra_bed').val();
        var extra_bed_rent= jQuery('#extra_bed_rent').val();
        var discount_per_day= jQuery('#discount_per_day').val();
        var advance= jQuery('#advance').val();

        var extrabedcharge,totaldis;
        var extrabedcharge=parseFloat(extra_bed) * parseFloat(extra_bed_rent);
        
        totaldis = parseFloat(discount_per_day) * parseFloat(total_days);
        var net_bal = (((parseFloat(room_amount)+ parseFloat(extrabedcharge)) * parseInt(total_days)) - parseFloat(totaldis)) - advance;

        jQuery("#net_amt").val(net_bal);
        set_labels(room_amount,total_days,extra_bed,extra_bed_rent,discount_per_day,advance,net_bal);
            
            

    }
    function set_labels(room_amount,days,extra_bed,extra_bed_rent,discount,advance,net_bal){
            $("#total_rent").html('0.00');
            $("#total_dy").html('0');
            $("#total_dis_lbl").html('0.00');
            $("#extra_bed_lbl").html('0');
            $("#extra_bed_rent_lbl").html('0.00');
            $("#advance_amnt_lbl").html('0.00');
            $("#net_rent").html('0.00');
            
            
            $("#total_rent").html(room_amount);
            $("#total_dy").html(days);
            $("#total_dis_lbl").html(discount);
            $("#extra_bed_lbl").html(extra_bed);
            $("#extra_bed_rent_lbl").html(extra_bed_rent);
            $("#advance_amnt_lbl").html(advance);
            $("#net_rent").html(net_bal);
    }
</script>