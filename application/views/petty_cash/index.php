<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body pt-0 pb-0">
                <div class="tabbable">
                    <div class="tab-content">
                        <div class="tab-pane active" id="highlighted-tab1">
                            <div class=" card-columns">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card bg-success-800">
                                            <div class="card-body ">
                                                <p class="card-text"><label class="">Petty cash Balance : <strong><u><?php echo $total_remainig_bal ?></u></strong></label></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Income</h4>
                                        <a href="incomes/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Cash Income </a>
                    <a href="incomes/save?transfer=bank" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Income from bank</a>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                    <h4>Expense</h4>
                                     <a href="expense/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Cash Expense</a>
                    <a href="expense/store" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Transfer Cash to store</a>
                    <a href="expense/save?transfer=bank" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Transfer Cash to bank</a>
                                    </div>
                                </div>
                                <hr>
                                



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>