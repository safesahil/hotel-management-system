<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="mini_bar/save/<?php echo (isset($prev_data) && isset($prev_data['id'])) ? $prev_data['id'] : '' ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Room <span class="text-danger">*</span></label>
                                <?php
                                $selected_room_id = (isset($prev_data) && isset($prev_data['room_id'])) ? $prev_data['room_id'] : set_value('room_id');
                                $attributes = array('id' => 'room_id', 'class' => 'select2-search');
                                ?>
                                <?php echo form_dropdown('room_id', $rooms_options, $selected_room_id, $attributes); ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Item <span class="text-danger">*</span></label>
                                <?php
                                $selected_item_id = (isset($prev_data) && isset($prev_data['item_id'])) ? $prev_data['item_id'] : set_value('item_id');
                                $attributes = array('id' => 'item_id', 'class' => 'select2-search');
                                ?>
                                <?php echo form_dropdown('item_id', $items_options, $selected_item_id, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>In Date <span class="text-danger">*</span></label>
                                <input
                                    id="log_date"
                                    name="log_date"
                                    type="text"
                                    class="form-control daterange-single-basic"
                                    placeholder="In Date"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['log_date'])) ? $prev_data['log_date'] : set_value('log_date') ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Quantity <span class="text-danger">*</span> <span id="stock_qty_lbl" class="text-danger text-size-mini"></span></label>
                                <input
                                    id="quantity"
                                    name="quantity"
                                    type="text"
                                    class="form-control"
                                    placeholder="Quantity"
                                    value="<?php echo (isset($prev_data) && isset($prev_data['quantity'])) ? $prev_data['quantity'] : set_value('quantity') ?>"/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Units <span class="text-danger">*</span></label>
                                <?php
                                $selected_unit = (isset($prev_data) && isset($prev_data['unit'])) ? $prev_data['unit'] : set_value('unit');
                                $attributes = array('id' => 'unit', 'class' => 'select2-basic', 'disabled' => 'disabled');
                                ?>
                                <?php echo form_dropdown('unit', array('' => 'Select Units'), $selected_unit, $attributes); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Price</label>
                                <input
                                    id="price"
                                    name="price"
                                    type="text"
                                    class="form-control"
                                    placeholder="Price"
                                    readonly="readonly"
                                    value=""/>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Status <span class="text-danger">*</span></label>
                                <?php
                                $status_options = array('1' => 'Active', '0' => 'Inactive');
                                $selected_status = (isset($prev_data) && isset($prev_data['status'])) ? $prev_data['status'] : set_value('status');
                                $attributes = array('id' => 'status', 'class' => 'select2-basic');
                                ?>
                                <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Description:</label>
                                <textarea
                                    id="description"
                                    name="description"
                                    rows="5"
                                    cols="5"
                                    class="form-control resize-ver"
                                    placeholder="Enter description here"><?php echo (isset($prev_data) && isset($prev_data['details'])) ? $prev_data['details'] : set_value('description') ?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="text-right">
                        <a href="mini_bar" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            log_date: {
                required: true,
            },
            room_id: {
                required: true,
            },
            item_id: {
                required: true,
            },
            unit: {
                required: true,
            },
            quantity: {
                required: true,
                min: 1,
                max: 100000,
            },
            status: {
                required: true
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);
        var itemIdControl = $(document).find('#item_id');
        var itemId = itemIdControl.val();
        if (itemId !== '') {
            itemIdControl.val(itemId).trigger('change');
        }
    });

    $(document).on('change', '#item_id', function () {
        var targetSelector = '#unit';
        var selectedItemId = $(this).val();
        if (selectedItemId !== '') {
            var url = '<?php base_url() ?>mini_bar/get_items_units/' + selectedItemId;
            ajaxGet(url).then(function (response) {
                if (response) {
                    var obj = JSON.parse(response);
                    if (obj && obj.status && obj.status === 1) {
                        var unitsOptions = obj.data.units_options;
                        var itemDetails = obj.data.item;
                        var stockQty = obj.data.stock_qty;
                        var stockUnit = obj.data.stock_unit;
                        var oldStock = '<?php echo (isset($prev_data) && isset($prev_data['quantity'])) ? $prev_data['quantity'] : set_value('quantity') ?>';
                        oldStock = (oldStock) ? parseFloat(oldStock) : 0;
                        var stockStr = 'Max : ' + (parseFloat(stockQty) + parseFloat(oldStock)) + ' ' + stockUnit;
                        enableControl(targetSelector);
                        generateSelec2Dropdown(targetSelector, unitsOptions, 'Select units', '', '');
                        var selectedUnit = '<?php echo (isset($prev_data) && isset($prev_data['unit'])) ? $prev_data['unit'] : set_value('unit') ?>';
                        $(document).find('#unit').val(selectedUnit).trigger('change.select2');
                        $(document).find('#price').val(itemDetails.purchase_price);
                        $(document).find('#stock_qty_lbl').html(stockStr);
                    } else {
                        disableControl(targetSelector);
                        cleanSelec2Dropdown(targetSelector, 'Select units', '');
                        $(document).find('#item_id').val('').trigger('change.select2');
                        $(document).find('#price').val('');
                        $(document).find('#stock_qty_lbl').html('');
                    }
                } else {
                    disableControl(targetSelector);
                    cleanSelec2Dropdown(targetSelector, 'Select units', '');
                    $(document).find('#item_id').val('').trigger('change.select2');
                    $(document).find('#price').val('');
                    $(document).find('#stock_qty_lbl').html('');
                }
            }, function (error) {
                disableControl(targetSelector);
                cleanSelec2Dropdown(targetSelector, 'Select units', '');
                $(document).find('#item_id').val('').trigger('change.select2');
                $(document).find('#price').val('');
                $(document).find('#stock_qty_lbl').html('');
            });
        } else {
            disableControl(targetSelector);
            cleanSelec2Dropdown(targetSelector, 'Select units', '');
            $(document).find('#item_id').val('').trigger('change.select2');
            $(document).find('#price').val('');
            $(document).find('#stock_qty_lbl').html('');
        }
    });
</script>