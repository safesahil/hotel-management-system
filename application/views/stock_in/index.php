<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <span class="text-highlight bg-primary">Balance : <?php echo $total_balance ?></span>
                    <a href="stock_in" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="stores/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Item</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>Category</th>
                                    <th>GRN</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                                <tr>
                                    <th>Category</th>
                                    <th>GRN</th>
                                    <th>Item</th>
                                    <th>Quantity</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        // Setup - add a text input to each footer cell
        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions' && title !== 'Quantity') {
                $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
            }
        });
        var d = new Date();
        //datatables
        var table = $('#dttable').DataTable({
        dom: 'Blfrtip',
            buttons: [
                {
                    extend: 'csv',
                    text: '<i class="fa fa-file-code-o"></i>&nbsp;&nbsp;CSV',
                    title: "Store Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    title: "Store Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3]
                    },
                },
                {
                    extend: 'excel',
                    text: '<i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Excel',
                    title: "Store Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3]
                    },
                },
                {
                    extend: 'pdfHtml5',
                    text: '<i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;PDF',
                    title: "Store Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3]
                    },
                },
                {
                    extend: 'print',
                    text: '<i class="fa fa-print"></i>&nbsp;&nbsp;Print',
                    title: "Store Report At_" + d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate(),
                    exportOptions: {
                    columns: [0,1, 2, 3]
                    },

                }

            ],
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            columns: [
                {
                    data: 'items_category_name',
                    visible: true,
                    name: 'items_category.name',
                },
                {
                    data: 'items_id',
                    visible: true,
                    name: 'items.id',
                },
                {
                    data: 'items_name',
                    visible: true,
                    name: 'items.name',
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var qtyStr = full['stock_qty'];
                        qtyStr += ' ' + full.item_inventory_converted_unit;
                        return qtyStr;
                    }
                },
                {
                    visible: true,
                    sortable: false,
                    searchable: false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        actionBtns = '<a href="stock_in/save/' + full.items_id + '" class="btn btn-primary btn-rounded btn-sm action-btns tooltip-show" title="Add Quantity"><i class="fa fa-plus-circle"></i></a>';
                        return actionBtns;
                    }
                }
            ],
            fnServerData: function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['extra_fields_select'] = ['items.id'];
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: "<?php echo base_url() . 'stock_in/filter' ?>",
                    data: req_obj,
                    success: function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>