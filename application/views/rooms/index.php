<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
                <div class="heading-elements">
                    <a href="rooms" class="btn bg-teal-400 btn-labeled"><b><i class="icon-sync"></i></b>Refresh</a>
                    <a href="rooms/save" class="btn btn-primary btn-labeled"><b><i class="icon-plus22"></i></b>Add Room</a>
                </div>
            </div>

            <form id="form" method="post">
                <div class="panel-body">
                    <div class="table-responsive popular_list">
                        <table id="dttable" class="table table-striped datatable-basic custom_dt width-100-per">
                            <thead>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Title</th>
                                    <th>Details</th>
                                    <th>Room Type</th>
                                    <th>Price</th>
                                    <th>Pax</th>
                                    <th>Available Status</th>
                                    <th>Status</th>
                                    <th>Deleted</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                                <tr>
                                    <th>Date Added</th>
                                    <th>Title</th>
                                    <th>Details</th>
                                    <th>Room Type</th>
                                    <th>Price</th>
                                    <th>Pax</th>
                                    <th>Available Status</th>
                                    <th>Status</th>
                                    <th>Deleted</th>
                                    <th class="sticky-col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(function () {
        var status_arr = {
            '<?php echo ENABLE ?>': "<?php echo ucwords(ENABLE_STR) ?>",
            '<?php echo DISABLE ?>': "<?php echo ucwords(DISABLE_STR) ?>",
        };
        var is_deleted_arr = {
            '<?php echo IS_DELETED_YES ?>': "Yes",
            '<?php echo IS_DELETED_NO ?>': "No",
        };

        $('#dttable thead tr:eq(0) th').each(function () {
            var title = $(this).text();
            if (title !== 'Actions') {
                if (title === 'Date Added') {
                    $(this).html('<input type="text" class="form-control daterange-basic-datatable" placeholder="' + title + '" />');
                } else {
                    $(this).html('<input type="text" class="form-control" placeholder="' + title + '" />');
                }
            }
        });

        var table = $('#dttable').DataTable({
            processing: true,
            serverSide: true,
            scrollX: true,
            scrollCollapse: true,
            orderCellsTop: false,
            aaSorting: [[0, 'desc']],
            fixedColumns: {
                leftColumns: 0,
                rightColumns: 1
            },
            language: {
                search: '<span>Filter :</span> _INPUT_',
                lengthMenu: '<span>Show :</span> _MENU_'
            },
            "columns": [
                {
                    data: 'rooms_created_date',
                    visible: true,
                    name: 'rooms.created_date',
                    render: function (data, type, full, meta) {
                        return get_mm_dd_yyyy_Date(full.rooms_created_date, '/');
                    }
                },
                {
                    'data': 'rooms_name',
                    "visible": true,
                    "name": 'rooms.name',
                },
                {
                    'data': 'rooms_details',
                    "visible": true,
                    "name": 'rooms.details',
                },
                {
                    'data': 'rooms_category_name',
                    "visible": true,
                    "name": 'rooms_category.name',
                },
                {
                    'data': 'rooms_price',
                    "visible": true,
                    "name": 'rooms.price',
                },
                {
                    'data': 'rooms_pax',
                    "visible": true,
                    "name": 'rooms.pax',
                },
                {
                    'data': 'rooms_availability_status',
                    "visible": true,
                    "name": 'rooms.availability_status',
                    "render": function (data, type, full, meta) {
                        var status = '<span class="label label-danger label-rounded"> ----- </span>';
                        if (full.rooms_availability_status === '1') {
                            status = '<span class="label label-success label-rounded"> Available </span>';
                        } else if (full.rooms_availability_status === '0') {
                            status = '<span class="label label-warning label-rounded"> Occupied </span>';
                        }
                        return status;
                    }
                },
                {
                    'data': 'rooms_status',
                    "visible": true,
                    "name": 'rooms.status',
                    "render": function (data, type, full, meta) {
                        var status = '<span class="label label-danger label-rounded"> ----- </span>';
                        if (full.rooms_status === '1') {
                            status = '<span class="label label-success label-rounded"> ' + status_arr[full.rooms_status] + '</span>';
                        } else if (full.rooms_status === '0') {
                            status = '<span class="label label-warning label-rounded"> ' + status_arr[full.rooms_status] + '</span>';
                        }
                        return status;
                    }
                },
                {
                    'data': 'rooms_is_deleted',
                    "visible": true,
                    "name": 'rooms.is_deleted',
                    "render": function (data, type, full, meta) {
                        var is_deleted = '<span class="label label-danger label-rounded"> ----- </span>';
                        if (full.rooms_is_deleted === '0') {
                            is_deleted = '<span class="label label-success label-rounded"> No </span>';
                        } else if (full.rooms_is_deleted === '1') {
                            is_deleted = '<span class="label label-warning label-rounded">Yes</span>';
                        }
                        return is_deleted;
                    }
                },
                {
                    "visible": true,
                    "sortable": false,
                    "searchable": false,
                    render: function (data, type, full, meta) {
                        var actionBtns = '';
                        actionBtns = '<a href="rooms/save/' + full.id + '" class="btn btn-primary btn-rounded btn-sm action-btns tooltip-show" title="Edit"><i class="fa fa-pencil"></i></a>';
                        if (full.rooms_is_deleted === '1') {
                            actionBtns += '<a href="javascript:void(0)" class="recover_record btn bg-teal btn-rounded btn-sm action-btns tooltip-show" title="Recover" data-path="rooms/recover/' + full.id + '"><i class="fa fa-undo"></i></a>';
                        } else {
                            actionBtns += '<a href="javascript:void(0)" class="delete_record btn btn-danger btn-rounded btn-sm action-btns tooltip-show" title="Delete" data-path="rooms/delete/' + full.id + '"><i class="fa fa-trash"></i></a>';
                        }
                        return actionBtns;
                    }
                },
            ],
            initComplete: function () {
                var tableColumns = table.settings().init().columns;
                this.api().columns().every(function (index) {
                    if (tableColumns[index].name == 'rooms.status' || tableColumns[index].name == 'rooms.is_deleted') {
                        var column = this;
                        var select = $('<select class="form-control"><option value="">Select</option></select>')
                                .appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex(
                                            $(this).val()
                                            );
                                    column
                                            .search(val ? val : '', true, false)
                                            .draw();
                                });
                        if (tableColumns[index].name == 'rooms.status') {
                            for (var key in status_arr) {
                                if (status_arr.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + status_arr[key] + '</option>');
                                }
                            }

                        } else if (tableColumns[index].name == 'rooms.is_deleted') {
                            for (var key in is_deleted_arr) {
                                if (is_deleted_arr.hasOwnProperty(key)) {
                                    select.append('<option value="' + key + '">' + is_deleted_arr[key] + '</option>');
                                }
                            }
                        }
                    }
                });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                var req_obj = {};
                aoData.forEach(function (data, key) {
                    req_obj[data['name']] = data['value'];
                });
                req_obj['col_eq'] = ['rooms.status', 'rooms.is_deleted'];
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': "<?php echo base_url() . 'rooms/filter' ?>",
                    'data': req_obj,
                    'success': function (data) {
                        fnCallback(data);
                    }
                });
            }
        });
        // Apply the search
        table.columns().every(function (index) {
            $('input', 'th:nth-child(' + (index + 1) + ')').on('keyup change', function () {
                table
                        .column(index)
                        .search(this.value)
                        .draw();
            });
        });

        $('.dataTables_length select').select2({
            minimumResultsForSearch: Infinity,
            width: 'auto'
        });
    });
</script>