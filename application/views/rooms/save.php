<div class="row">
    <div class="col-md-12">
        <!-- Basic layout-->
        <form id="save" name="save" action="rooms/save/<?php echo (isset($rooms_data) && isset($rooms_data['id'])) ? $rooms_data['id'] : '' ?>" method="POST" class="save-form">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title"><?php echo $page_title ?></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="form-group">
                        <label>Name <span class="text-danger">*</span></label>
                        <input
                            id="name"
                            name="name"
                            type="text"
                            class="form-control"
                            placeholder="Name"
                            value="<?php echo isset($rooms_data['name'])?$rooms_data['name']:set_value('name') ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Description:</label>
                        <textarea
                            id="description"
                            name="description"
                            rows="5"
                            cols="5"
                            class="form-control resize-ver"
                            placeholder="Enter description here"><?php echo isset($rooms_data['details'])?$rooms_data['details']:set_value('description') ?></textarea>
                    </div>
                     <div class="form-group">
                        <label>Room category <span class="text-danger">*</span></label>
                        <?php
                        $i_c_options = array('' => 'Select category');
                        if (isset($room_categories) && count($room_categories) > 0) {
                            $i_c_options = $room_categories;
                        }
                        $selected_room_category = (isset($rooms_data) && isset($rooms_data['type'])) ? $rooms_data['type'] : set_value('type');
                        $attributes = array('id' => 'type', 'class' => 'select2-basic');
                        ?>
                        <?php echo form_dropdown('type', $i_c_options, $selected_room_category, $attributes); ?>
                    </div>
                    <div class="form-group">
                        <label>Price <span class="text-danger">*</span></label>
                        <input
                            id="price"
                            name="price"
                            type="number"
                            class="form-control"
                            placeholder="Price"
                            value="<?php echo isset($rooms_data['price'])?$rooms_data['price']:set_value('price') ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Pax <span class="text-danger">*</span></label>
                        <input
                            id="pax"
                            name="pax"
                            type="number"
                            class="form-control"
                            placeholder="Pax"
                            value="<?php echo isset($rooms_data['pax'])?$rooms_data['pax']:set_value('pax') ?>"/>
                    </div>
                    <div class="form-group">
                        <label>Available Status <span class="text-danger">*</span></label>
                        <?php
                        $status_options = array('1' => 'Available', '0' => 'Occupied');
                        $selected_status = isset($rooms_data['availability_status'])?$rooms_data['availability_status']:set_value('availability_status');
                        $attributes = array('id' => 'availability_status', 'class' => 'select2-basic');
                        ?>
                        <?php echo form_dropdown('availability_status', $status_options, $selected_status, $attributes); ?>
                    </div>
                    <div class="form-group">
                        <label>Status <span class="text-danger">*</span></label>
                        <?php
                        $status_options = array('1' => 'Active', '0' => 'Inactive');
                        $selected_status = isset($rooms_data['status'])?$rooms_data['status']:set_value('status');
                        $attributes = array('id' => 'status', 'class' => 'select2-basic');
                        ?>
                        <?php echo form_dropdown('status', $status_options, $selected_status, $attributes); ?>
                    </div>

                    

                    <div class="text-right">
                        <a href="rooms" class="btn btn-default"><i class="icon-arrow-left13 position-left"></i> Back</a>
                        <button type="submit" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </div>
            </div>
        </form>
        <!-- /basic layout -->
    </div>
</div>
<script>
    $(function () {
        var rules = {
            name: {
                required: true,
                minlength: 2,
                maxlength: 50
            },
            price: {
                required: true,
            },
            type: {
                required: true
            },
            status: {
                required: true
            },
            pax: {
                required: true
            },
            availability_status: {
                required: true
            },
            description: {
                maxlength: 250
            }
        };
        initValidation('.save-form', rules);
        
        jQuery('#type').change(function(){
            var c_val=jQuery(this).val();
            if(c_val!=null){
                jQuery("#type-error").remove();
            }
        });
        
    });
</script>