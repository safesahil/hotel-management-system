<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-body pt-0 pb-0">
                <div class="tabbable">
                    <div class="tab-content">
                        <div class="tab-pane active" id="highlighted-tab1">
                            <div class="row card-columns">
                            
                            <div class="col-md-4">
                                <div class="card bg-success">
                                    <div class="card-body ">
                                        <p class="card-text"><label class="">Total Income : <strong><u><?php echo $total_sum_amount_income ?></u></strong></label></p>
                                    </div>
                                  </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="card bg-danger">
                                    <div class="card-body ">
                                        <p class="card-text"><label class="">Total Expense : <strong><u><?php echo $total_sum_amount ?></u></strong></label></p>
                                    </div>
                                  </div>
                                
                            </div>
                                <div class="col-md-4">
                                <div class="card bg-primary">
                                    <div class="card-body ">
                                        <p class="card-text"><label class="">Petty cash Balance : <strong><u><?php echo $total_remainig_bal ?></u></strong></label></p>
                                    </div>
                                  </div>
                            </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title"><?php echo $page_title; ?></h5>
            </div>

            <div class="panel-body pt-0 pb-0">
                <div class="tabbable">
                    <ul class="nav nav-tabs nav-tabs-highlight">
                        <?php foreach ($room_cats as $room_cat) { ?>
                            <li class="<?php echo ($room_cat_id == $room_cat['id']) ? 'active' : '' ?>">
                                <a href="<?php echo base_url('dashboard/' . $room_cat['id']) ?>">
                                    <?php echo ucfirst($room_cat['name']); ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="highlighted-tab1">
                            <?php echo $tab_content; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>