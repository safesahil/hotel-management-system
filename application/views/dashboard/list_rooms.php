<div class="row">
    <div class="col-md-10 no-padding">
        <?php if (isset($rooms) && count($rooms) > 0) { ?>
            <?php
            foreach ($rooms as $room) {
                $availability_class = 'default';
                $availability_str = 'Unknown';
                if ($room['status'] == DISABLE) {
                    $availability_class = 'danger';
                    $availability_str = DISABLE_STR;
                } else {
                    if ($room['availability_status'] == ROOM_AVAILABLE) {
                        $availability_class = 'success';
                        $availability_str = ROOM_AVAILABLE_STR;
                    } else if ($room['availability_status'] == ROOM_OCCUPIED) {
                        $availability_class = 'warning';
                        $availability_str = ROOM_OCCUPIED_STR;
                    }
                }
                ?>
                <div class="col-md-4">
                    <div class="panel panel-<?php echo $availability_class ?> panel-bordered">
                        <div class="panel-heading">
                            <h6 class="panel-title text-bold"><?php echo $room['name'] ?><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-lg">
                                    <tbody>
                                        <tr>
                                            <td class="text-left">Price</td>
                                            <td class="text-right"><?php echo '$' . $room['price'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Pax(s)</td>
                                            <td class="text-right"><?php echo $room['pax'] ?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Availi</td>
                                            <td class="text-right"><span class="label label-<?php echo $availability_class ?>"><?php echo $availability_str ?></span></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        <?php } else { ?>
            <div class="col-md-12 text-center">
                <h3>No rooms found!</h3>
            </div>
        <?php } ?>
    </div>
    <div class="col-md-2 no-padding">
        <div class="panel">
            <div class="panel-body no-padding">
                <div class="table-responsive">
                    <table class="table table-lg">
                        <tbody>
                            <tr>
                                <td class="text-left"><?php echo ROOM_AVAILABLE_STR ?></td>
                                <td class="text-right"><span class="badge badge-success">AV</span></td>
                            </tr>
                            <tr>
                                <td class="text-left"><?php echo ROOM_OCCUPIED_STR ?></td>
                                <td class="text-right"><span class="badge badge-warning">OC</span></td>
                            </tr>
                            <tr>
                                <td class="text-left"><?php echo DISABLE_STR ?></td>
                                <td class="text-right"><span class="badge badge-danger">DI</span></td>
                            </tr>
                            <tr>
                                <td class="text-left">Unknown</td>
                                <td class="text-right"><span class="badge badge-default">UN</span></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
