<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stores extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Stock in';
    }

    public function index() {
        $this->data['page_title'] = 'Stock in listing';
        $total_balance = $this->get_store_balance();
        $this->data['total_balance'] = number_format((float) $total_balance['total_balance'], 2, '.', '');
        $this->admin_template->load('admin', 'stores/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['where'][TBL_ITEMS_INVENTORY . '.type'] = INVENTORY_TYPE_IN;
        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS_CATEGORY,
                'condition' => TBL_ITEMS_INVENTORY . '.item_category_id = ' . TBL_ITEMS_CATEGORY . '.id'
            ),
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS,
                'condition' => TBL_ITEMS_INVENTORY . '.item_id = ' . TBL_ITEMS . '.id'
            ),
        );
        $filter_array['order'][TBL_ITEMS_INVENTORY . '.id'] = 'desc';

        $filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ITEMS_INVENTORY)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $prev_data['log_date'] = date("m/d/Y", strtotime($prev_data['log_date']));
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'item_id',
                'log_date',
                'quantity',
                'unit',
                'price',
                'status',
                'description',
            );
            $meta = array('id' => $id);
            if ($this->_validate_form($validate_fields, $meta)) {
                $logged_user = $this->session->userdata(ADMINS_STR_KEY);
                $item_id = $this->input->post('item_id');
                $item = $this->get_item_by_id($item_id);
                $unit = $this->input->post('unit');
                $base_unit = get_base_measurement_unit($unit);
                $quantity = $this->input->post('quantity');
                $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                $posted_log_date = $this->input->post('log_date');
                $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                $log_date = date_format($new_log_date, "Y-m-d");
                $price = $this->input->post('price');
                $new_data = array(
                    'item_category_id' => $item['category_id'],
                    'item_id' => $item['id'],
                    'quantity' => $quantity,
                    'unit' => $unit,
                    'price' => $price,
                    'base_quantity' => $base_quantity,
                    'base_unit' => $base_unit,
                    'log_date' => $log_date,
                    'type' => INVENTORY_TYPE_IN,
                    'managed_for' => INVENTORY_MANAGED_FOR_GENERAL,
                    'status' => $this->input->post('status'),
                    'details' => $this->input->post('description'),
                );
                $payment_data = array(
                    'amount' => $price,
                    'type' => DEBIT,
                    'managed_for' => PAYMENT_MANAGED_FOR_STORE_EXPENSE,
                    'description' => $item['name'] . ' for stock',
                    'created_transaction_by' => $logged_user['id'],
                );
                if ($id) {
                    $new_data['modified_date'] = date('Y-m-d H:i:s');
                    $where = array('id' => $id);
                    $payment_where = array('item_inventory_id' => $id);
                    $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                    $payment_affected_records = $this->BM->update(TBL_PAYMENTS, $payment_data, $payment_where);
                    if (isset($affected_records)) {
                        $this->session->set_flashdata('success_msg', 'Stock in saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                } else {
                    $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                    if (isset($inserted_id)) {
                        $payment_data['item_inventory_id'] = $inserted_id;
                        $inserted_id = $this->BM->insert(TBL_PAYMENTS, $payment_data);
                        if ($inserted_id) {
                            $this->session->set_flashdata('success_msg', 'Stock in saved.');
                        } else {
                            $this->session->set_flashdata('warning_msg', 'Stock in saved, but payment error please check with stock again');
                        }
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                }
                $url = base_url('stores');
                redirect($url);
            }
        }
        $select_condition = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'CONCAT(items.name, " [", item_cat.name, "] ") as item_name'
            ),
            'where' => array(
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as item_cat',
                    'condition' => 'item_cat.id = items.category_id',
                ),
            ),
        );
        $items = $this->BM->get($select_condition);
        $total_balance = $this->get_store_balance($id);
        $this->data['total_balance'] = number_format((float) $total_balance['total_balance'], 2, '.', '');
        $this->data['items_options'] = prepare_data_for_dropdown($items, 'item_name', 'id', true, 'Select Item', '');
        $this->data['page_title'] = 'Stock in store';
        $this->admin_template->load('admin', 'stores/save', $this->data);
    }

    public function delete($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Stock in deleted.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('stores');
        redirect($url);
    }

    public function recover($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Stock in recovered.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('stores');
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_ITEMS_INVENTORY,
            'fields' => array(
                'id', 'item_category_id', 'item_id', 'quantity', 'unit', 'price', 'log_date', 'details', 'status'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    public function get_items_units($item_id = '') {
        $response = array(
            'status' => 0,
            'message' => 'Something went wrong! please try again later.',
        );
        if (isset($item_id)) {
            $select_item_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'unit_category', 'purchase_price', 'sale_price'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_item_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units_options = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                $response_data = array(
                    'item' => $item,
                    'units_options' => $units_options,
                    'default_unit' => get_default_measurement_unit_by_category($item['unit_category']),
                );
                $response = array(
                    'status' => 1,
                    'message' => 'Success',
                    'data' => $response_data,
                );
            } else {
                $response['message'] = "Invalid item";
            }
        } else {
            $response['message'] = "Item id is required";
        }
        echo json_encode($response);
    }

    function get_item_by_id($item_id) {
        if (isset($item_id)) {
            $cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'category_id', 'name', 'unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE
                ),
            );
            $item = $this->BM->get_one($cond);
            if (isset($item)) {
                return $item;
            }
        }
        return NULL;
    }

    function get_store_balance($id = '') {
        $condition = array(
            'table' => TBL_PAYMENTS . ' as payments',
            'fields' => array(
                'SUM(CASE WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_RETURN . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_EXPENSE . ' THEN -payments.amount ELSE 0 END) as total_balance'
            ),
            'where' => array(
                'payments.type' => DEBIT
            ),
            'where_in' => array(
                'payments.managed_for' => array(PAYMENT_MANAGED_FOR_STORE, PAYMENT_MANAGED_FOR_STORE_EXPENSE, PAYMENT_MANAGED_FOR_STORE_RETURN)
            )
        );
        $store_balance = $this->BM->get_one($condition);
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $prev_price = $prev_data['price'];
            $store_balance['total_balance'] += $prev_price;
        }
        return $store_balance;
    }

    function _validate_form($validate_fields, $meta) {
        $validation_rules = array();

        if (in_array('item_id', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'item_id',
                'label' => 'Item',
                'rules' => 'trim|required|callback__validate_item'
            );
        }
        if (in_array('log_date', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'log_date',
                'label' => 'In Date',
                'rules' => 'trim|required'
            );
        }
        if (in_array('quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|numeric|greater_than_equal_to[1]|less_than_equal_to[100000]'
            );
        }
        if (in_array('unit', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit',
                'label' => 'Units',
                'rules' => 'trim|required|callback__validate_unit'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000]|callback__check_store_balance[' . json_encode($meta) . ']'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_item($value) {
        if ($value) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id'),
                'where' => array(
                    'id' => $value,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $cnt = $this->BM->count($select_cond);
            if (isset($cnt) && $cnt == 1) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_item', 'Invalid item.');
            return FALSE;
        }
    }

    function _validate_unit($value) {
        $item_id = $this->input->post('item_id');
        if ($value && $item_id) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                if (isset($units) && isset($units[$value])) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit', 'Invalid unit.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

    function _check_store_balance($value, $encoded_meta) {
        if ($value) {
            $meta = json_decode($encoded_meta, TRUE);
            $id = $meta['id'] ? $meta['id'] : '';
            $total_balance = $this->get_store_balance($id)['total_balance'];
            if ($total_balance >= $value) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_store_balance', 'Insufficient balance.');
            return FALSE;
        }
    }

}
