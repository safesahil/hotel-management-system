<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Admin_controller {

    public function index($type = 'checking') {
         $type_int=0;
        if($type=='checkout'){
           $type_int=1;
        }
    $this->data['title'] = $this->data['page_title'] = $this->data['page_header'] = 'Transactions';
       $condition = array(
            'table' => TBL_TRANSACTIONS,
           'fields' => array(
                TBL_TRANSACTIONS.'.id',TBL_TRANSACTIONS.'.bill_no',TBL_TRANSACTIONS.'.user_id',TBL_TRANSACTIONS.'.room_category_id',TBL_TRANSACTIONS.'.room_id',TBL_TRANSACTIONS.'.checkin_date',TBL_TRANSACTIONS.'.checkout_date',TBL_TRANSACTIONS.'.type', TBL_USERS.'.first_name', TBL_USERS.'.last_name', TBL_USERS.'.mobile_number',TBL_USERS.'.address', TBL_USERS.'.proof',TBL_ROOMS_CATEGORY.'.name',TBL_ROOMS.'.name as room_name',
            ),
            'where' => array(
                TBL_TRANSACTIONS.'.type' => $type_int
            ),
            'join'=>array(
                array(
                    'join_type' => 'left',
                    'table' => TBL_ROOMS,
                    'condition' => TBL_TRANSACTIONS. '.room_id= ' . TBL_ROOMS . '.id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_USERS,
                    'condition' => TBL_TRANSACTIONS. '.user_id= ' . TBL_USERS . '.id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_ROOMS_CATEGORY,
                    'condition' => TBL_TRANSACTIONS. '.room_category_id= ' . TBL_ROOMS_CATEGORY. '.id'
                ),
            )
        );
      
        $this->data['record']=$rec=$this->BM->get($condition);
        $this->data['type']=$type_int;
        
        $tab_content = $this->load->view('transactions/list_transaction', $this->data, true);
        
        $this->data['tab_content'] = $tab_content;
        $this->admin_template->load('admin', 'transactions/index', $this->data);
    }
}
