<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Room_services extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Room services';
    }

    public function index() {
        $this->data['page_title'] = 'Rooms listing';
        $this->admin_template->load('admin', 'room_services/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['where'][TBL_ROOMS . '.availability_status'] = ROOM_OCCUPIED;

        $filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ROOMS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function services($room_id) {
        $select_cond = array(
            'table' => TBL_ROOMS,
            'fields' => array('id', 'name', 'details', 'type', 'price', 'pax'),
            'where' => array(
                'id' => $room_id,
                'status' => ENABLE,
                'is_deleted' => DISABLE,
                'availability_status' => ROOM_OCCUPIED,
            ),
        );
        $room = $this->BM->get_one($select_cond);
        if (isset($room)) {
            $this->data['room'] = $room;
            $this->data['page_title'] = 'Services list';
            $this->admin_template->load('admin', 'room_services/services', $this->data);
        } else {
            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
            $url = base_url('room_services');
            redirect($url);
        }
    }

    public function filter_services() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['where'][TBL_ITEMS . '.status'] = ENABLE;
        $filter_array['where'][TBL_ITEMS . '.is_deleted'] = DISABLE;
        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS_CATEGORY,
                'condition' => TBL_ITEMS . '.category_id = ' . TBL_ITEMS_CATEGORY . '.id'
            )
        );

        $filter_records = $this->BM->get_filtered_records(TBL_ITEMS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ITEMS, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ITEMS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save_services($room_id, $item_id) {
        $last_transaction_data = $this->_get_last_transaction_data_by_room_id($room_id);
        $last_transaction_count = $this->_get_last_transactions_count_by_room_id($room_id);
        $room_data = $this->_get_room_data_by_id($room_id);
        if (isset($last_transaction_data) && isset($room_data)) {
            $item_data = $this->_get_item_data_by_id($item_id);
            if ($this->input->post()) {
                $validate_fields = array(
                    'log_date',
                    'quantity',
                    'unit',
                    'price',
                    'status',
                    'description',
                );
                $meta = array('item_id' => $item_id);
                if ($this->_validate_form($validate_fields, $meta)) {
                    $logged_user = $this->session->userdata(ADMINS_STR_KEY);
                    $price = $this->input->post('price');
                    $unit = $this->input->post('unit');
                    $base_unit = get_base_measurement_unit($unit);
                    $quantity = $this->input->post('quantity');
                    $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                    $posted_log_date = $this->input->post('log_date');
                    $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                    $log_date = date_format($new_log_date, "Y-m-d");
                    $new_data = array(
                        'item_category_id' => $item_data['category_id'],
                        'item_id' => $item_data['id'],
                        'room_id' => $room_data['id'],
                        'quantity' => $quantity,
                        'unit' => $unit,
                        'price' => $price,
                        'base_quantity' => $base_quantity,
                        'base_unit' => $base_unit,
                        'log_date' => $log_date,
                        'type' => INVENTORY_TYPE_OUT,
                        'managed_for' => INVENTORY_MANAGED_FOR_GENERAL,
                        'status' => $this->input->post('status'),
                        'details' => $this->input->post('description'),
                    );
                    $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                    if (isset($inserted_id)) {
                        $new_data = array(
                            'transaction_id' => $last_transaction_data['id'],
                            'item_inventory_id' => $inserted_id,
                            'price' => $price,
                            'type' => DEBIT,
                            'create_transaction_by' => isset($logged_user['id']) ? $logged_user['id'] : '0',
                            'room_id' => $room_id,
                            'description' => $item_data['item_name'],
                            'priority' => $last_transaction_count,
                        );
                        $inserted_id = $this->BM->insert(TBL_TRANSACTIONS_MANAGEMENT, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Room service saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong while room service! please try again later.');
                        }
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong while transaction! please try again later.');
                    }
                    $url = base_url('room_services/services/' . $room_id);
                    redirect($url);
                }
            }
            $all_units = json_decode(MEASUREMENT_UNITS, true);
            if (isset($item_data['unit_category']) && isset($all_units[$item_data['unit_category']])) {
                $select_inventory_cond = array(
                    'table' => TBL_ITEMS_INVENTORY,
                    'fields' => array(
                        'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                    ),
                    'where' => array(
                        'item_id' => $item_id,
                        'status' => ENABLE,
                        'is_deleted' => DISABLE,
                    ),
                    'group_by' => array(
                        'item_id'
                    ),
                );
                $inventory = $this->BM->get_one($select_inventory_cond);
                $units_options = $all_units[$item_data['unit_category']];
                $base_stock_qty = $inventory['stock_qty'];
                $base_unit = get_base_measurement_unit_by_category($item_data['unit_category']);
                $default_unit = get_default_measurement_unit_by_category($item_data['unit_category']);
                $stock_qty = convert_measurement_units($base_unit, $default_unit, $base_stock_qty, true, 2);

                $this->data['units_options'] = $units_options;
                $this->data['item_data'] = $item_data;
                $this->data['room_data'] = $room_data;
                $this->data['room_id'] = $room_id;
                $this->data['stock_qty'] = $stock_qty;
                $this->data['stock_unit'] = $default_unit;
                $this->data['page_title'] = 'Save room service';
                $this->admin_template->load('admin', 'room_services/save_service', $this->data);
            } else {
                $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                $url = base_url('room_services/services/' . $room_id);
                redirect($url);
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Room transaction was not found! please try again later.');
            $url = base_url('room_services');
            redirect($url);
        }
    }

    function _get_last_transaction_data_by_room_id($id) {
        $condition = array(
            'table' => TBL_TRANSACTIONS . ' as transactions',
            'fields' => array(
                'transactions.id', 'transactions.user_id',
                'transactions.room_id', 'transactions.room_category_id', 'transactions.agent_id',
                'transactions.tour_id', 'transactions.checkin_date', 'transactions.checkout_date', 'transactions.narration'
            ),
            'where' => array(
                'transactions.room_id' => $id,
                'transactions.is_deleted' => DISABLE,
            ),
            'order_by' => array(
                'transactions.id' => 'desc'
            )
        );
        return $this->BM->get_one($condition);
    }

    function _get_last_transactions_count_by_room_id($room_id) {
        $bill_condition = array(
            'table' => TBL_TRANSACTIONS_MANAGEMENT,
            'where' => array(
                TBL_TRANSACTIONS_MANAGEMENT . '.room_id' => $room_id,
                TBL_TRANSACTIONS_MANAGEMENT . '.type' => DEBIT
            ),
        );
        $count = $this->BM->count($bill_condition);
        $count +=1;
        return $count;
    }

    function _get_room_data_by_id($id) {
        $condition = array(
            'table' => TBL_ROOMS . ' as rooms',
            'fields' => array(
                'rooms.id', 'rooms.name'
            ),
            'where' => array(
                'rooms.id' => $id,
                'rooms.status' => ENABLE,
                'rooms.availability_status' => ROOM_OCCUPIED,
                'rooms.is_deleted' => DISABLE,
            )
        );
        return $this->BM->get_one($condition);
    }

    function _get_item_data_by_id($id) {
        $condition = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'items.name', 'items.category_id', 'items.unit_category', 'items.purchase_price', 'items.sale_price',
                'items.status', 'CONCAT(items.name, " [", item_cat.name, "] ") as item_name'
            ),
            'where' => array(
                'items.id' => $id,
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as item_cat',
                    'condition' => 'item_cat.id = items.category_id',
                ),
            ),
        );
        return $this->BM->get_one($condition);
    }

    function _validate_form($validate_fields, $meta) {
        $validation_rules = array();

        if (in_array('log_date', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'log_date',
                'label' => 'In Date',
                'rules' => 'trim|required'
            );
        }
        if (in_array('quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|greater_than_equal_to[1]|less_than_equal_to[100000]|callback__check_stock[' . json_encode($meta) . ']'
            );
        }
        $item_id = $meta['item_id'];
        if (in_array('unit', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit',
                'label' => 'Units',
                'rules' => 'trim|required|callback__validate_unit[' . $item_id . ']'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_unit($value, $item_id) {
        if ($value && $item_id) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                if (isset($units) && isset($units[$value])) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit', 'Invalid unit.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

    function _check_stock($value, $encode_meta) {
        if ($value) {
            $meta = json_decode($encode_meta, TRUE);
            $item_id = $meta['item_id'];
            $select_inventory_cond = array(
                'table' => TBL_ITEMS_INVENTORY,
                'fields' => array(
                    'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                ),
                'where' => array(
                    'item_id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
                'group_by' => array(
                    'item_id'
                ),
            );
            $inventory = $this->BM->get_one($select_inventory_cond);
            $unit = $this->input->post('unit');
            $base_unit = get_base_measurement_unit($unit);
            $quantity = $this->input->post('quantity');
            $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
            $prev_stock_qty = $inventory['stock_qty'];
            if ($base_quantity <= $prev_stock_qty) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_stock', '%s can not be more than stock.');
            return FALSE;
        }
    }

}
