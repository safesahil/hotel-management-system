<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_in extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Store';
    }

    public function index() {
        $this->data['page_title'] = 'Store items listing';
        $total_balance = $this->get_store_balance();
        $this->data['total_balance'] = number_format((float) $total_balance['total_balance'], 2, '.', '');
        $this->admin_template->load('admin', 'stock_in/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        /*$filter_array['fields'][] = 'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty';*/
        $filter_array['fields'][] = 'SUM(CASE items_inventory.type WHEN 1 THEN items_inventory.base_quantity WHEN 2 THEN -items_inventory.base_quantity WHEN 4 THEN -items_inventory.base_quantity ELSE 0 END) as stock_qty';
        $filter_array['fields'][] = TBL_ITEMS . '.unit_category,'.TBL_ITEMS_INVENTORY.'.is_deleted';
        $filter_array['group_by'][] = 'items_inventory.item_id';
        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS_CATEGORY,
                'condition' => TBL_ITEMS_INVENTORY . '.item_category_id = ' . TBL_ITEMS_CATEGORY . '.id'
            ),
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS,
                'condition' => TBL_ITEMS_INVENTORY . '.item_id = ' . TBL_ITEMS . '.id'
            ),
        );
        $filter_array['where']=array(TBL_ITEMS_INVENTORY.'.is_deleted'=>0,TBL_ITEMS_INVENTORY.'.status'=>1);
        $filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array, 1);
        $new_filter_records = array();
        foreach ($filter_records as $value) {
            $new_value = $value;
            if (isset($value['stock_qty'])) {
                $base_unit = get_base_measurement_unit_by_category($value['unit_category']);
                $default_unit = get_default_measurement_unit_by_category($value['unit_category']);
                $qty = convert_measurement_units($base_unit, $default_unit, $value['stock_qty'], true, 2);
                $new_value['stock_qty'] = $qty;
                $new_value['item_inventory_converted_unit'] = $default_unit;
            }
            $new_filter_records[] = $new_value;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ITEMS_INVENTORY)),
            "recordsFiltered" => $total_filter_records,
            "data" => $new_filter_records,
        );
        echo json_encode($output);
    }

    public function save($item_id) {
        $item_data = $this->_get_item_data_by_id($item_id);
        if ($this->input->post()) {
            $validate_fields = array(
                'log_date',
                'quantity',
                'unit',
                'price',
                'status',
                'description',
            );
            $meta = array('item_id' => $item_id);
            if ($this->_validate_form($validate_fields, $meta)) {
                $logged_user = $this->session->userdata(ADMINS_STR_KEY);
                $unit = $this->input->post('unit');
                $base_unit = get_base_measurement_unit($unit);
                $quantity = $this->input->post('quantity');
                $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                $posted_log_date = $this->input->post('log_date');
                $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                $log_date = date_format($new_log_date, "Y-m-d");
                $price = $this->input->post('price');
                $new_data = array(
                    'item_category_id' => $item_data['category_id'],
                    'item_id' => $item_data['id'],
                    'quantity' => $quantity,
                    'unit' => $unit,
                    'price' => $price,
                    'base_quantity' => $base_quantity,
                    'base_unit' => $base_unit,
                    'log_date' => $log_date,
                    'type' => INVENTORY_TYPE_IN,
                    'managed_for' => INVENTORY_MANAGED_FOR_GENERAL,
                    'status' => $this->input->post('status'),
                    'details' => $this->input->post('description'),
                );
                $payment_data = array(
                    'amount' => $price,
                    'type' => DEBIT,
                    'managed_for' => PAYMENT_MANAGED_FOR_STORE_EXPENSE,
                    'description' => $item_data['name'] . ' for stock',
                    'created_transaction_by' => $logged_user['id'],
                );
                $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                if (isset($inserted_id)) {
                    $payment_data['item_inventory_id'] = $inserted_id;
                    $inserted_id = $this->BM->insert(TBL_PAYMENTS, $payment_data);
                    if ($inserted_id) {
                        $this->session->set_flashdata('success_msg', 'Item quantity added.');
                    } else {
                        $this->session->set_flashdata('warning_msg', 'Item quantity added, but payment error please check with stock again');
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
                $url = base_url('stock_in');
                redirect($url);
            }
        }
        $all_units = json_decode(MEASUREMENT_UNITS, true);
        if (isset($item_data['unit_category']) && isset($all_units[$item_data['unit_category']])) {
            $this->data['units_options'] = $all_units[$item_data['unit_category']];
            $this->data['default_unit'] = get_default_measurement_unit_by_category($item_data['unit_category']);
            $this->data['item_data'] = $item_data;
            $total_balance = $this->get_store_balance();
            $this->data['total_balance'] = number_format((float) $total_balance['total_balance'], 2, '.', '');
            $this->data['page_title'] = 'Add item quantity';
            $this->admin_template->load('admin', 'stock_in/save', $this->data);
        } else {
            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
            $url = base_url('stock_in');
            redirect($url);
        }
    }

    function _get_item_data_by_id($id) {
        $condition = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'items.name', 'items.category_id', 'items.unit_category', 'items.purchase_price', 'items.sale_price',
                'items.status', 'CONCAT(items.name, " [", item_cat.name, "] ") as item_name'
            ),
            'where' => array(
                'items.id' => $id,
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as item_cat',
                    'condition' => 'item_cat.id = items.category_id',
                ),
            ),
        );
        return $this->BM->get_one($condition);
    }

    function get_store_balance() {
        $condition = array(
            'table' => TBL_PAYMENTS . ' as payments',
            'fields' => array(
                'SUM(CASE WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_RETURN . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_EXPENSE . ' THEN -payments.amount ELSE 0 END) as total_balance'
            ),
            'where' => array(
                'payments.type' => DEBIT
            ),
            'where_in' => array(
                'payments.managed_for' => array(PAYMENT_MANAGED_FOR_STORE, PAYMENT_MANAGED_FOR_STORE_EXPENSE, PAYMENT_MANAGED_FOR_STORE_RETURN)
            )
        );
        $store_balance = $this->BM->get_one($condition);
        return $store_balance;
    }

    function _validate_form($validate_fields, $meta) {
        $validation_rules = array();

        if (in_array('log_date', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'log_date',
                'label' => 'In Date',
                'rules' => 'trim|required'
            );
        }
        if (in_array('quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|numeric|greater_than_equal_to[1]|less_than_equal_to[100000]'
            );
        }
        $item_id = $meta['item_id'];
        if (in_array('unit', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit',
                'label' => 'Units',
                'rules' => 'trim|required|callback__validate_unit[' . $item_id . ']'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000]|callback__check_store_balance'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_unit($value, $item_id) {
        if ($value && $item_id) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                if (isset($units) && isset($units[$value])) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit', 'Invalid unit.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

    function _check_store_balance($value) {
        if ($value) {
            $total_balance = $this->get_store_balance()['total_balance'];
            if ($total_balance >= $value) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_store_balance', 'Insufficient balance.');
            return FALSE;
        }
    }

}
