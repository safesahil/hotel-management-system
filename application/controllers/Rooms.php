<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rooms extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Rooms';
    }

    public function index() {

        $this->data['page_title'] = 'Rooms';
        $this->data['url_new_record'] = base_url('rooms/');
        $this->data['url_refresh'] = base_url('rooms');
        $this->admin_template->load('admin', 'rooms/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
         $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ROOMS_CATEGORY,
                'condition' => TBL_ROOMS . '.type = ' . TBL_ROOMS_CATEGORY . '.id'
            )
        );
        $filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array, 1);
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array('table' => TBL_ROOMS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = null) {
        if ($this->input->post()) {
            $validate_fields = array(
                'name',
                'type',
                'price',
                'pax',
                'availability_status',
                'status',
                'description'
            );
            if ($this->_validate_form($validate_fields)) {
                
                
                
                $new_data = array(
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'price' => $this->input->post('price'),
                    'pax' => $this->input->post('pax'),
                    'availability_status' => $this->input->post('availability_status'),
                    'details' => $this->input->post('description'),
                    'status' => $this->input->post('status'),
                    
                );
                
                if (!$id) {
                    $inserted_id = $this->BM->insert(TBL_ROOMS, $new_data);
                    if (isset($inserted_id)) {
                        $this->session->set_flashdata('success_msg', 'Room detail saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                } else {
                    $where = array('id' => $id);
                    $new_data['modified_date']=date(DB_DATE_FORMAT);
                    $is_updated = $this->BM->update(TBL_ROOMS, $new_data, $where);
                    if ($is_updated) {
                        $this->session->set_flashdata('success_msg', "Room detail updated.");
                    }
                }

                $url = base_url('rooms');
                redirect($url);
            }
        }
         if ($id) {
            $select_data = array(
                'table' => TBL_ROOMS,
                'where' => array('id' => $id),
            );
            $this->data['rooms_data'] = $this->BM->get_one($select_data);
            $this->data['page_action'] = 'Update Room Detail';
        }
        $room_categories = $this->_get_room_categories();
        
        $this->data['room_categories'] = prepare_data_for_dropdown($room_categories, 'name', 'id', true, 'Select category', '');
        $this->data['back_url'] = 'Rooms';
        $this->data['post_url'] = 'rooms/save/' . $id;
        $this->data['page_title'] = 'Save Room';
        $this->admin_template->load('admin', 'rooms/save', $this->data);
    }

     public function delete($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date(DB_DATE_FORMAT));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ROOMS, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Room detail deleted.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('rooms');
        redirect($url);
    }

    public function recover($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date(DB_DATE_FORMAT));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ROOMS, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Room detail recovered.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('rooms');
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_ROOMS,
            'fields' => array(
                'id', 'name', 'details', 'status'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

     function _get_room_categories() {
        $condition = array(
            'table' => TBL_ROOMS_CATEGORY,
            'fields' => array(
                'id', 'name'
            )
        );
        return $this->BM->get($condition);
    }
    
    function _validate_form($validate_fields) {
        $validation_rules = array();
        if (in_array('name', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required|min_length[2]|max_length[50]'
            );
        }
        if (in_array('type', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'type',
                'label' => 'Category Id',
                'rules' => 'trim|required'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|required'
            );
        }
        if (in_array('pax', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'pax',
                'label' => 'Pax',
                'rules' => 'trim|required'
            );
        }
        if (in_array('available_status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'available_status',
                'label' => 'Room Available Status',
                'rules' => 'trim|required'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

}
