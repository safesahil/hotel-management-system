<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends Admin_controller {

    public function listing($_role = ALL_STR_KEY) {
        $role = strtolower($_role);
        $this->data['role'] = $role;

        $roles = array(
            ALL_STR_LABEL => 'users/listing/all',
            CUSTOMERS_STR_LABEL => 'users/listing/customers',
            AGENTS_STR_LABEL => 'users/listing/agents',
            TOURS_STR_LABEL => 'users/listing/tours',
            ADMINS_STR_LABEL => 'users/listing/admins',
        );
        $this->data['roles'] = $roles;
        $tab_content = "";
        if ($role === ALL_STR_KEY) {
            $this->data['title'] = $this->data['page_header'] = 'Users';
            $this->data['page_title'] = 'Users Listing';
            $tab_content = $this->load->view('users/list_all', $this->data, true);
        } else if ($role === CUSTOMERS_STR_KEY) {
            $this->data['title'] = $this->data['page_header'] = CUSTOMERS_STR_LABEL;
            $this->data['page_title'] = CUSTOMERS_STR_LABEL . ' Listing';
            $tab_content = $this->load->view('users/list_customers', $this->data, true);
        } else if ($role === AGENTS_STR_KEY) {
            $this->data['title'] = $this->data['page_header'] = AGENTS_STR_LABEL;
            $this->data['page_title'] = AGENTS_STR_LABEL . ' Listing';
            $tab_content = $this->load->view('users/list_agents', $this->data, true);
        } else if ($role === TOURS_STR_KEY) {
            $this->data['title'] = $this->data['page_header'] = TOURS_STR_LABEL;
            $this->data['page_title'] = TOURS_STR_LABEL . ' Listing';
            $tab_content = $this->load->view('users/list_tours', $this->data, true);
        } else if ($role === ADMINS_STR_KEY) {
            $this->data['title'] = $this->data['page_header'] = ADMINS_STR_LABEL;
            $this->data['page_title'] = ADMINS_STR_LABEL . ' Listing';
            $tab_content = $this->load->view('users/list_admins', $this->data, true);
        } else {
            redirect('users/listing/all');
        }

        $this->data['tab_content'] = $tab_content;
        $this->admin_template->load('admin', 'users/index', $this->data);
    }

    public function filter($role = ALL_STR_KEY) {
        $filter_array = create_datatable_request($this->input->post());
        if ($role === CUSTOMERS_STR_KEY) {
            $filter_array['custom_or_where'] = array(TBL_USERS . '.role' => array(CUSTOMERS));
        } else if ($role == AGENTS_STR_KEY) {
            $filter_array['custom_or_where'] = array(TBL_USERS . '.role' => array(AGENTS));
        } else if ($role == TOURS_STR_KEY) {
            $filter_array['custom_or_where'] = array(TBL_USERS . '.role' => array(TOURS));
        } else if ($role == ADMINS_STR_KEY) {
            $filter_array['custom_or_where'] = array(TBL_USERS . '.role' => array(ADMINS));
        }

        $filter_records = $this->BM->get_filtered_records(TBL_USERS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_USERS, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_USERS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save_user($type, $id = '') {
        $this->data['page_title'] = 'Save User';
        if ($type == CUSTOMERS_STR_KEY) {
            $this->_save_customer($type, $id);
        } elseif ($type == AGENTS_STR_KEY) {
            $this->_save_agent($type, $id);
        } elseif ($type == TOURS_STR_KEY) {
            $this->_save_tour($type, $id);
        } elseif ($type == ADMINS_STR_KEY) {
            $this->_save_admin($type, $id);
        }
    }

    function _save_customer($type, $id) {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'first_name',
                'last_name',
                'mobile_number',
                'email',
                'gender',
                'status',
                'address',
                'proof_method',
                'proof_number',
                'proof_month',
                'proof_year',
                'proof_country',
            );
            if ($id) {
                if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                    $validate_fields[] = 'proof';
                }
                if ($prev_data['email'] != $this->input->post('email_id')) {
                    $validate_fields[] = 'email_unique';
                }
                if ($prev_data['mobile_number'] != $this->input->post('mobile_number')) {
                    $validate_fields[] = 'mobile_unique';
                }
            } else {
                $validate_fields[] = 'proof';
                $validate_fields[] = 'mobile_unique';
                $validate_fields[] = 'email_unique';
            }
            if ($this->_validate_form($validate_fields)) {
                $upload_image = TRUE;
                $file_source = NULL;
                $file_source_relative = NULL;
                $uploaded_files = [];
                if ($id) {
                    if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                        if (isset($prev_data['proof_relative'])) {
                            delete_upload_files(array($prev_data['proof_relative']));
                        }
                        $upload_image = TRUE;
                    } else {
                        $file_source = (isset($prev_data['proof'])) ? $prev_data['proof'] : NULL;
                        $file_source_relative = (isset($prev_data['proof_relative'])) ? $prev_data['proof_relative'] : NULL;
                        $upload_image = FALSE;
                    }
                }
                if ($upload_image) {
                    $uploaded_files = upload_files($_FILES, 'proof', 'users');
                }
                $save_data = FALSE;
                if (isset($uploaded_files) && isset($uploaded_files[0]) && $uploaded_files[0]['status'] == 1) {
                    $save_data = TRUE;
                    $file_source = $uploaded_files[0]['destination'];
                    $file_source_relative = $uploaded_files[0]['destination_relative'];
                } else if (!$upload_image) {
                    $save_data = TRUE;
                }
                if ($save_data) {
                    $new_data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email_id'),
                        'gender' => $this->input->post('gender'),
                        'mobile_number' => $this->input->post('mobile_number'),
                        'address' => $this->input->post('address'),
                        'proof_method' => $this->input->post('proof_method'),
                        'proof_number' => $this->input->post('proof_number'),
                        'proof_month' => $this->input->post('proof_month'),
                        'proof_year' => $this->input->post('proof_year'),
                        'proof_country' => $this->input->post('proof_country'),
                        'proof' => (isset($file_source)) ? $file_source : NULL,
                        'proof_relative' => (isset($file_source_relative)) ? $file_source_relative : NULL,
                        'role' => CUSTOMERS,
                        'status' => $this->input->post('status'),
                    );
                    if ($id) {
                        $new_data['modified_date'] = date('Y-m-d H:i:s');
                        $where = array('id' => $id);
                        $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                        if (isset($affected_records)) {
                            $this->session->set_flashdata('success_msg', 'Customer saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    } else {
                        $inserted_id = $this->BM->insert(TBL_USERS, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Customer saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
                $url = base_url('users/listing/' . CUSTOMERS_STR_KEY);
                redirect($url);
            }
        }
        $this->data['title'] = $this->data['page_header'] = CUSTOMERS_STR_LABEL;
        $this->data['page_title'] = 'Save ' . CUSTOMERS_STR_LABEL;
        $this->admin_template->load('admin', 'users/save_customers', $this->data);
    }

    function _save_agent($type, $id) {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'first_name',
                'last_name',
                'mobile_number',
                'email',
                'gender',
                'status',
                'address',
                'proof_method',
                'proof_number',
                'proof_month',
                'proof_year',
                'proof_country',
            );
            if ($id) {
                if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                    $validate_fields[] = 'proof';
                }
                if ($prev_data['email'] != $this->input->post('email_id')) {
                    $validate_fields[] = 'email_unique';
                }
                if ($prev_data['mobile_number'] != $this->input->post('mobile_number')) {
                    $validate_fields[] = 'mobile_unique';
                }
            } else {
                $validate_fields[] = 'proof';
                $validate_fields[] = 'mobile_unique';
                $validate_fields[] = 'email_unique';
            }
            if ($this->_validate_form($validate_fields)) {
                $upload_image = TRUE;
                $file_source = NULL;
                $file_source_relative = NULL;
                $uploaded_files = [];
                if ($id) {
                    if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                        if (isset($prev_data['proof_relative'])) {
                            delete_upload_files(array($prev_data['proof_relative']));
                        }
                        $upload_image = TRUE;
                    } else {
                        $file_source = (isset($prev_data['proof'])) ? $prev_data['proof'] : NULL;
                        $file_source_relative = (isset($prev_data['proof_relative'])) ? $prev_data['proof_relative'] : NULL;
                        $upload_image = FALSE;
                    }
                }
                if ($upload_image) {
                    $uploaded_files = upload_files($_FILES, 'proof', 'agents');
                }
                $save_data = FALSE;
                if (isset($uploaded_files) && isset($uploaded_files[0]) && $uploaded_files[0]['status'] == 1) {
                    $save_data = TRUE;
                    $file_source = $uploaded_files[0]['destination'];
                    $file_source_relative = $uploaded_files[0]['destination_relative'];
                } else if (!$upload_image) {
                    $save_data = TRUE;
                }
                if ($save_data) {
                    $new_data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email_id'),
                        'gender' => $this->input->post('gender'),
                        'mobile_number' => $this->input->post('mobile_number'),
                        'address' => $this->input->post('address'),
                        'proof_method' => $this->input->post('proof_method'),
                        'proof_number' => $this->input->post('proof_number'),
                        'proof_month' => $this->input->post('proof_month'),
                        'proof_year' => $this->input->post('proof_year'),
                        'proof_country' => $this->input->post('proof_country'),
                        'proof' => (isset($file_source)) ? $file_source : NULL,
                        'proof_relative' => (isset($file_source_relative)) ? $file_source_relative : NULL,
                        'role' => AGENTS,
                        'status' => $this->input->post('status'),
                    );
                    if ($id) {
                        $new_data['modified_date'] = date('Y-m-d H:i:s');
                        $where = array('id' => $id);
                        $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                        if (isset($affected_records)) {
                            $this->session->set_flashdata('success_msg', 'Agent saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    } else {
                        $inserted_id = $this->BM->insert(TBL_USERS, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Agent saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
                $url = base_url('users/listing/' . AGENTS_STR_KEY);
                redirect($url);
            }
        }
        $this->data['title'] = $this->data['page_header'] = AGENTS_STR_LABEL;
        $this->data['page_title'] = 'Save Agent';
        $this->admin_template->load('admin', 'users/save_agents', $this->data);
    }

    function _save_tour($type, $id) {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'first_name',
                'last_name',
                'mobile_number',
                'email',
                'gender',
                'status',
                'address',
                'proof_method',
                'proof_number',
                'proof_month',
                'proof_year',
                'proof_country',
            );
            if ($id) {
                if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                    $validate_fields[] = 'proof';
                }
                if ($prev_data['email'] != $this->input->post('email_id')) {
                    $validate_fields[] = 'email_unique';
                }
                if ($prev_data['mobile_number'] != $this->input->post('mobile_number')) {
                    $validate_fields[] = 'mobile_unique';
                }
            } else {
                $validate_fields[] = 'proof';
                $validate_fields[] = 'mobile_unique';
                $validate_fields[] = 'email_unique';
            }
            if ($this->_validate_form($validate_fields)) {
                $upload_image = TRUE;
                $file_source = NULL;
                $file_source_relative = NULL;
                $uploaded_files = [];
                if ($id) {
                    if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                        if (isset($prev_data['proof_relative'])) {
                            delete_upload_files(array($prev_data['proof_relative']));
                        }
                        $upload_image = TRUE;
                    } else {
                        $file_source = (isset($prev_data['proof'])) ? $prev_data['proof'] : NULL;
                        $file_source_relative = (isset($prev_data['proof_relative'])) ? $prev_data['proof_relative'] : NULL;
                        $upload_image = FALSE;
                    }
                }
                if ($upload_image) {
                    $uploaded_files = upload_files($_FILES, 'proof', 'tours');
                }
                $save_data = FALSE;
                if (isset($uploaded_files) && isset($uploaded_files[0]) && $uploaded_files[0]['status'] == 1) {
                    $save_data = TRUE;
                    $file_source = $uploaded_files[0]['destination'];
                    $file_source_relative = $uploaded_files[0]['destination_relative'];
                } else if (!$upload_image) {
                    $save_data = TRUE;
                }
                if ($save_data) {
                    $new_data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email_id'),
                        'gender' => $this->input->post('gender'),
                        'mobile_number' => $this->input->post('mobile_number'),
                        'address' => $this->input->post('address'),
                        'proof_method' => $this->input->post('proof_method'),
                        'proof_number' => $this->input->post('proof_number'),
                        'proof_month' => $this->input->post('proof_month'),
                        'proof_year' => $this->input->post('proof_year'),
                        'proof_country' => $this->input->post('proof_country'),
                        'proof' => (isset($file_source)) ? $file_source : NULL,
                        'proof_relative' => (isset($file_source_relative)) ? $file_source_relative : NULL,
                        'role' => TOURS,
                        'status' => $this->input->post('status'),
                    );
                    if ($id) {
                        $new_data['modified_date'] = date('Y-m-d H:i:s');
                        $where = array('id' => $id);
                        $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                        if (isset($affected_records)) {
                            $this->session->set_flashdata('success_msg', 'Tour saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    } else {
                        $inserted_id = $this->BM->insert(TBL_USERS, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Tour saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
                $url = base_url('users/listing/' . TOURS_STR_KEY);
                redirect($url);
            }
        }
        $this->data['title'] = $this->data['page_header'] = TOURS_STR_LABEL;
        $this->data['page_title'] = 'Save Tour';
        $this->admin_template->load('admin', 'users/save_tours', $this->data);
    }

    function _save_admin($type, $id) {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'first_name',
                'last_name',
                'mobile_number',
                'email',
                'email_required',
                'password',
                'confirm_password',
                'gender',
                'status',
                'address',
                'proof_method',
                'proof_number',
                'proof_month',
                'proof_year',
                'proof_country',
            );
            if ($id) {
                if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                    $validate_fields[] = 'proof';
                }
                if ($prev_data['email'] != $this->input->post('email_id')) {
                    $validate_fields[] = 'email_unique';
                }
                if ($prev_data['mobile_number'] != $this->input->post('mobile_number')) {
                    $validate_fields[] = 'mobile_unique';
                }
            } else {
                $validate_fields[] = 'proof';
                $validate_fields[] = 'mobile_unique';
                $validate_fields[] = 'email_unique';
                $validate_fields[] = 'password_required';
                $validate_fields[] = 'confirm_password_required';
            }
            if ($this->_validate_form($validate_fields)) {
                $upload_image = TRUE;
                $file_source = NULL;
                $file_source_relative = NULL;
                $uploaded_files = array();
                if ($id) {
                    if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                        if (isset($prev_data['proof_relative'])) {
                            delete_upload_files(array($prev_data['proof_relative']));
                        }
                        $upload_image = TRUE;
                    } else {
                        $file_source = (isset($prev_data['proof'])) ? $prev_data['proof'] : NULL;
                        $file_source_relative = (isset($prev_data['proof_relative'])) ? $prev_data['proof_relative'] : NULL;
                        $upload_image = FALSE;
                    }
                }
                if ($upload_image) {
                    $uploaded_files = upload_files($_FILES, 'proof', 'admins');
                }
                $save_data = FALSE;
                if (isset($uploaded_files) && isset($uploaded_files[0]) && $uploaded_files[0]['status'] == 1) {
                    $save_data = TRUE;
                    $file_source = $uploaded_files[0]['destination'];
                    $file_source_relative = $uploaded_files[0]['destination_relative'];
                } else if (!$upload_image) {
                    $save_data = TRUE;
                }
                if ($save_data) {
                    $new_data = array(
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'email' => $this->input->post('email_id'),
                        'gender' => $this->input->post('gender'),
                        'mobile_number' => $this->input->post('mobile_number'),
                        'address' => $this->input->post('address'),
                        'proof_method' => $this->input->post('proof_method'),
                        'proof_number' => $this->input->post('proof_number'),
                        'proof_month' => $this->input->post('proof_month'),
                        'proof_year' => $this->input->post('proof_year'),
                        'proof_country' => $this->input->post('proof_country'),
                        'proof' => (isset($file_source)) ? $file_source : NULL,
                        'proof_relative' => (isset($file_source_relative)) ? $file_source_relative : NULL,
                        'role' => ADMINS,
                        'status' => $this->input->post('status'),
                    );
                    if ($this->input->post('password')) {
                        $new_data['password'] = md5($this->input->post('password'));
                    }
                    if ($id) {
                        $new_data['modified_date'] = date('Y-m-d H:i:s');
                        $where = array('id' => $id);
                        $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                        if (isset($affected_records)) {
                            $this->session->set_flashdata('success_msg', 'Admin saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    } else {
                        $inserted_id = $this->BM->insert(TBL_USERS, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Admin saved.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    }
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
                $url = base_url('users/listing/' . ADMINS_STR_KEY);
                redirect($url);
            }
        }
        $this->data['title'] = $this->data['page_header'] = ADMINS_STR_LABEL;
        $this->data['page_title'] = 'Save Admin';
        $this->admin_template->load('admin', 'users/save_admins', $this->data);
    }

    public function delete_user($type, $id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                if (isset($affected_records)) {
                    $success_msg = 'User deleted.';
                    switch ($type) {
                        case CUSTOMERS_STR_KEY:
                            $success_msg = 'Customer deleted.';
                            break;
                        case AGENTS_STR_KEY:
                            $success_msg = 'Agent deleted.';
                            break;
                        case TOURS_STR_KEY:
                            $success_msg = 'Tour deleted.';
                            break;
                        case ADMINS_STR_KEY:
                            $success_msg = 'Admin deleted.';
                            break;
                        default:
                            $success_msg = 'User deleted.';
                            break;
                    }
                    $this->session->set_flashdata('success_msg', $success_msg);
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('users/listing/' . $type);
        redirect($url);
    }

    public function recover_user($type, $id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                if (isset($affected_records)) {
                    $success_msg = 'User recovered.';
                    switch ($type) {
                        case CUSTOMERS_STR_KEY:
                            $success_msg = 'Customer recovered.';
                            break;
                        case AGENTS_STR_KEY:
                            $success_msg = 'Agent recovered.';
                            break;
                        case TOURS_STR_KEY:
                            $success_msg = 'Tour recovered.';
                            break;
                        case ADMINS_STR_KEY:
                            $success_msg = 'Admin recovered.';
                            break;
                        default:
                            $success_msg = 'User recovered.';
                            break;
                    }
                    $this->session->set_flashdata('success_msg', $success_msg);
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('users/listing/' . $type);
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_USERS,
            'fields' => array(
                'id', 'first_name', 'last_name', 'email', 'gender', 'mobile_number',
                'address', 'proof_method', 'proof_number', 'proof_month', 'proof_year', 'proof_country',
                'proof', 'proof_relative', 'capture_image', 'role', 'status',
                'is_deleted', 'created_date', 'modified_date'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    function _validate_form($validate_fields) {
        $validation_rules = array();

        if (in_array('first_name', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'first_name',
                'label' => 'First name',
                'rules' => 'trim|required|min_length[2]|max_length[15]'
            );
        }
        if (in_array('last_name', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'last_name',
                'label' => 'Last name',
                'rules' => 'trim|min_length[2]|max_length[15]'
            );
        }
        if (in_array('mobile_number', $validate_fields)) {
            if (in_array('mobile_unique', $validate_fields)) {
                $mobile_rules = 'trim|is_unique[' . TBL_USERS . '.mobile_number]|callback__regex[' . REGEX_MOBILE_NUMBER . ']';
            } else {
                $mobile_rules = 'trim|callback__regex[' . REGEX_MOBILE_NUMBER . ']';
            }
            $validation_rules[] = array(
                'field' => 'mobile_number',
                'label' => 'Mobile no.',
                'rules' => $mobile_rules
            );
        }
        if (in_array('proof', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof',
                'label' => 'Proof',
                'rules' => 'callback__validate_proof'
            );
        }
        if (in_array('email', $validate_fields)) {
            $email_rules = '';
            if (in_array('email_required', $validate_fields)) {
                $email_rules = 'required';
            }
            if (isset($email_rules) && $email_rules) {
                $email_rules .= '|';
            }
            if (in_array('email_unique', $validate_fields)) {
                $email_rules .= 'trim|is_unique[' . TBL_USERS . '.email]|callback__validate_email';
            } else {
                $email_rules .= 'trim|callback__validate_email';
            }
            $validation_rules[] = array(
                'field' => 'email_id',
                'label' => 'Email',
                'rules' => $email_rules
            );
        }
        if (in_array('password', $validate_fields)) {
            $password_rules = 'trim|min_length[8]|max_length[32]';
            if (in_array('password_required', $validate_fields)) {
                $password_rules = 'trim|required';
            }
            $validation_rules[] = array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => $password_rules
            );
        }
        if (in_array('confirm_password', $validate_fields)) {
            $password_rules = 'trim|min_length[8]|max_length[32]|matches[password]';
            if (in_array('confirm_password_required', $validate_fields)) {
                $password_rules = 'trim|required';
            }
            $validation_rules[] = array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => $password_rules
            );
        }
        if (in_array('gender', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required|callback__validate_gender'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('address', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'trim|max_length[250]'
            );
        }
        if (in_array('proof_method', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof_method',
                'label' => 'Proof Method',
                'rules' => 'trim|required|callback__validate_proof_method'
            );
        }
        if (in_array('proof_number', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof_number',
                'label' => 'Proof Number',
                'rules' => 'trim|required|min_length[2]|max_length[32]'
            );
        }
        if (in_array('proof_month', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof_month',
                'label' => 'Proof Month',
                'rules' => 'trim|required|greater_than_equal_to[1]|less_than_equal_to[12]'
            );
        }
        if (in_array('proof_year', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof_year',
                'label' => 'Proof Year',
                'rules' => 'trim|required'
            );
        }
        if (in_array('proof_country', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'proof_country',
                'label' => 'Proof Country',
                'rules' => 'trim|required'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _regex($value, $regex) {
        if ($value) {
            if (preg_match($regex, $value)) {
                return TRUE;
            }
            $this->form_validation->set_message('_regex', 'The %s field contains invalid value');
            return FALSE;
        }
    }

    function _validate_email($value) {
        if ($value) {
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_email', 'The %s field contains invalid value');
            return FALSE;
        }
    }

    function _validate_gender($value) {
        if ($value) {
            if ($value == MALE_INT || $value == FEMALE_INT) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_gender', 'The %s field contains invalid value');
            return FALSE;
        }
    }

    function _validate_proof() {
        if (isset($_FILES) && isset($_FILES['proof']) && isset($_FILES['proof']['name']) && $_FILES['proof']['name'] != '') {
            if ($_FILES['proof']['error'] == '0') {
                if ($_FILES['proof']['size'] >= MIN_IMAGE_SIZE && $_FILES['proof']['size'] <= MAX_IMAGE_SIZE) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('_validate_proof', 'The %s file size must be between ' . MIN_IMAGE_SIZE_STR . ' to ' . MAX_IMAGE_SIZE_STR . '.');
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('_validate_proof', 'The %s field contains invalid file or file might be corrupted.');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('_validate_proof', 'The %s field is required.');
            return FALSE;
        }
    }

    function _validate_proof_method($value) {
        if ($value) {
            if ($value == PROOF_ID_CARD || $value == PROOF_PASSPORT || $value == PROOF_DRIVING_LIC) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_proof_method', 'The %s field contains invalid value');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

}
