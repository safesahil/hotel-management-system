<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants_items_category extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Restaurants Items category';
    }

    public function index() {
        $this->data['page_title'] = 'Restaurants Items category listing';
        $this->admin_template->load('admin', 'restaurants_items_category/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_records = $this->BM->get_filtered_records(TBL_RESTAURANTS_ITEMS_CATEGORY, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_RESTAURANTS_ITEMS_CATEGORY, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_RESTAURANTS_ITEMS_CATEGORY)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = '') {
        if ($id) {
            
            $this->data['prev_data'] = $this->_get_data_by_id($id);
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'category_name',
                'status',
                'description'
            );
            if ($this->_validate_form($validate_fields)) {
                $new_data = array(
                    'name' => $this->input->post('category_name'),
                    'status' => $this->input->post('status'),
                    'details' => $this->input->post('description'),
                );
                if ($id) {
                    $new_data['modified_date'] = date('Y-m-d H:i:s');
                    $where = array('id' => $id);
                    $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS_CATEGORY, $new_data, $where);
                    if (isset($affected_records)) {
                        $this->session->set_flashdata('success_msg', 'Item category saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                } else {
                    $inserted_id = $this->BM->insert(TBL_RESTAURANTS_ITEMS_CATEGORY, $new_data);
                    if (isset($inserted_id)) {
                        $this->session->set_flashdata('success_msg', 'Item category saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                }
                $url = base_url('restaurants_items_category');
                redirect($url);
            }
        }
        $this->data['page_title'] = 'Save items category';
        $this->admin_template->load('admin', 'restaurants_items_category/save', $this->data);
    }

    public function delete($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS_CATEGORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item category deleted.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('restaurants_items_category');
        redirect($url);
    }

    public function recover($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS_CATEGORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item category recovered.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('restaurants_items_category');
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_RESTAURANTS_ITEMS_CATEGORY,
            'fields' => array(
                'id', 'name', 'details', 'status'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    function _validate_form($validate_fields) {
        $validation_rules = array();
        if (in_array('category_name', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'category_name',
                'label' => 'Name',
                'rules' => 'trim|required|min_length[2]|max_length[50]'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

}
