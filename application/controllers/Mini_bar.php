<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mini_bar extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Mini bar stock in';
    }

    public function index() {
        $this->data['page_title'] = 'Mini bar stock in listing';
        $this->admin_template->load('admin', 'mini_bar/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['where_in'][TBL_ITEMS_INVENTORY . '.type'] = array(INVENTORY_TYPE_OUT, INVENTORY_TYPE_MINI_BAR_OUT);
        $filter_array['where'][TBL_ITEMS_INVENTORY . '.managed_for'] = INVENTORY_MANAGED_FOR_MINI_BAR;
        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS_CATEGORY,
                'condition' => TBL_ITEMS_INVENTORY . '.item_category_id = ' . TBL_ITEMS_CATEGORY . '.id'
            ),
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS,
                'condition' => TBL_ITEMS_INVENTORY . '.item_id = ' . TBL_ITEMS . '.id'
            ),
            array(
                'join_type' => 'left',
                'table' => TBL_ROOMS,
                'condition' => TBL_ITEMS_INVENTORY . '.room_id = ' . TBL_ROOMS . '.id'
            ),
        );
        $filter_array['order'][TBL_ITEMS_INVENTORY . '.id'] = 'desc';

        $filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ITEMS_INVENTORY)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $log_date = date_create_from_format('Y-m-d', $prev_data['log_date']);
            $prev_data['log_date'] = date_format($log_date, 'm/d/Y');
            $this->data['prev_data'] = $prev_data;
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'room_id',
                'item_id',
                'log_date',
                'quantity',
                'unit',
                'price',
                'status',
                'description',
            );
            $meta = array(
                'id' => $id,
            );
            if ($this->_validate_form($validate_fields, $meta)) {
                $item_id = $this->input->post('item_id');
                $item = $this->get_item_by_id($item_id);
                $unit = $this->input->post('unit');
                $base_unit = get_base_measurement_unit($unit);
                $quantity = $this->input->post('quantity');
                $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                $posted_log_date = $this->input->post('log_date');
                $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                $log_date = date_format($new_log_date, "Y-m-d");
                $new_data = array(
                    'item_category_id' => $item['category_id'],
                    'item_id' => $item['id'],
                    'room_id' => $this->input->post('room_id'),
                    'quantity' => $quantity,
                    'unit' => $unit,
                    'price' => $this->input->post('price'),
                    'base_quantity' => $base_quantity,
                    'base_unit' => $base_unit,
                    'log_date' => $log_date,
                    'type' => INVENTORY_TYPE_OUT,
                    'managed_for' => INVENTORY_MANAGED_FOR_MINI_BAR,
                    'status' => $this->input->post('status'),
                    'details' => $this->input->post('description'),
                );
                if ($id) {
                    $new_data['modified_date'] = date('Y-m-d H:i:s');
                    $where = array('id' => $id);
                    $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                    if (isset($affected_records)) {
                        $this->session->set_flashdata('success_msg', 'Item saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                } else {
                    $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                    if (isset($inserted_id)) {
                        $this->session->set_flashdata('success_msg', 'Item saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                }
                $url = base_url('mini_bar');
                redirect($url);
            }
        }
        $select_condition = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'CONCAT(items.name, " [", item_cat.name, "] ") as item_name'
            ),
            'where' => array(
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as item_cat',
                    'condition' => 'item_cat.id = items.category_id',
                ),
            ),
        );
        $room_select = array(
            'table' => TBL_ROOMS . ' as rooms',
            'fields' => array(
                'rooms.id', 'rooms.name'
            ),
            'where' => array(
                'rooms.status' => ENABLE,
                'rooms.is_deleted' => DISABLE,
            ),
        );
        $items = $this->BM->get($select_condition);
        $rooms = $this->BM->get($room_select);
        $this->data['items_options'] = prepare_data_for_dropdown($items, 'item_name', 'id', true, 'Select Item', '');
        $this->data['rooms_options'] = prepare_data_for_dropdown($rooms, 'name', 'id', true, 'Select Room', '');
        $this->data['page_title'] = 'Save item';
        $this->admin_template->load('admin', 'mini_bar/save', $this->data);
    }

    public function delete($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item deleted.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('mini_bar');
        redirect($url);
    }

    public function recover($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_ITEMS_INVENTORY, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item recovered.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('mini_bar');
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_ITEMS_INVENTORY,
            'fields' => array(
                'id', 'item_category_id', 'item_id', 'room_id', 'quantity', 'unit',
                'price', 'log_date', 'details', 'status', 'base_quantity', 'base_unit', 'managed_for'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    public function get_items_units($item_id = '') {
        $response = array(
            'status' => 0,
            'message' => 'Something went wrong! please try again later.',
        );
        if (isset($item_id)) {
            $select_item_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'unit_category', 'purchase_price', 'sale_price'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_item_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $select_inventory_cond = array(
                    'table' => TBL_ITEMS_INVENTORY,
                    'fields' => array(
                        'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                    ),
                    'where' => array(
                        'item_id' => $item_id,
                        'status' => ENABLE,
                        'is_deleted' => DISABLE,
                    ),
                    'group_by' => array(
                        'item_id'
                    ),
                );
                $inventory = $this->BM->get_one($select_inventory_cond);
                $units_options = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                $base_stock_qty = $inventory['stock_qty'];
                $base_unit = get_base_measurement_unit_by_category($item['unit_category']);
                $default_unit = get_default_measurement_unit_by_category($item['unit_category']);
                $stock_qty = convert_measurement_units($base_unit, $default_unit, $base_stock_qty, true, 2);
                $response_data = array(
                    'item' => $item,
                    'units_options' => $units_options,
                    'base_stock_qty' => $base_stock_qty,
                    'base_stock_unit' => $base_unit,
                    'stock_qty' => $stock_qty,
                    'stock_unit' => $default_unit,
                );
                $response = array(
                    'status' => 1,
                    'message' => 'Success',
                    'data' => $response_data,
                );
            } else {
                $response['message'] = "Invalid item";
            }
        } else {
            $response['message'] = "Item id is required";
        }
        echo json_encode($response);
    }

    function get_item_by_id($item_id) {
        if (isset($item_id)) {
            $cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'category_id', 'name', 'unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE
                ),
            );
            $item = $this->BM->get_one($cond);
            if (isset($item)) {
                return $item;
            }
        }
        return NULL;
    }

    function _validate_form($validate_fields, $meta) {
        $validation_rules = array();

        if (in_array('room_id', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'room_id',
                'label' => 'Room',
                'rules' => 'trim|required|callback__validate_room'
            );
        }
        if (in_array('item_id', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'item_id',
                'label' => 'Item',
                'rules' => 'trim|required|callback__validate_item'
            );
        }
        if (in_array('log_date', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'log_date',
                'label' => 'In Date',
                'rules' => 'trim|required'
            );
        }
        if (in_array('quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|greater_than_equal_to[1]|less_than_equal_to[100000]|callback__check_stock[' . json_encode($meta) . ']'
            );
        }
        if (in_array('unit', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit',
                'label' => 'Units',
                'rules' => 'trim|required|callback__validate_unit'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000]'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_room($value) {
        if ($value) {
            $select_cond = array(
                'table' => TBL_ROOMS,
                'fields' => array('id'),
                'where' => array(
                    'id' => $value,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $cnt = $this->BM->count($select_cond);
            if (isset($cnt) && $cnt == 1) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_room', 'Invalid room.');
            return FALSE;
        }
    }

    function _validate_item($value) {
        if ($value) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id'),
                'where' => array(
                    'id' => $value,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $cnt = $this->BM->count($select_cond);
            if (isset($cnt) && $cnt == 1) {
                return TRUE;
            }
            $this->form_validation->set_message('_validate_item', 'Invalid item.');
            return FALSE;
        }
    }

    function _validate_unit($value) {
        $item_id = $this->input->post('item_id');
        if ($value && $item_id) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                if (isset($units) && isset($units[$value])) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit', 'Invalid unit.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

    function _check_stock($value, $encode_meta) {
        if ($value) {
            $item_id = $this->input->post('item_id');
            $select_inventory_cond = array(
                'table' => TBL_ITEMS_INVENTORY,
                'fields' => array(
                    'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                ),
                'where' => array(
                    'item_id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
                'group_by' => array(
                    'item_id'
                ),
            );
            $inventory = $this->BM->get_one($select_inventory_cond);
            $unit = $this->input->post('unit');
            $base_unit = get_base_measurement_unit($unit);
            $quantity = $this->input->post('quantity');
            $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
            $prev_stock_qty = $inventory['stock_qty'];
            $meta = json_decode($encode_meta, true);
            $id = $meta['id'];
            if ($id) {
                $prev_item_inventory = $this->_get_data_by_id($id);
                $prev_stock_qty += $prev_item_inventory['base_quantity'];
            }
            if ($base_quantity <= $prev_stock_qty) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_stock', '%s can not be more than stock.');
            return FALSE;
        }
    }

}
