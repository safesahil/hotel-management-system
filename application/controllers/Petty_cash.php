<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Petty_cash extends Admin_controller {

    public function index($room_cat_id = '') {
        $this->data['title'] = $this->data['page_title'] = $this->data['page_header'] = 'Petty Cash';
        $this->data['total_sum_amount'] = $total_sum_amount=$this->total_amount_expense(DEBIT,PAYMENT_MANAGED_FOR_GENERAL,PAYMENT_MANAGED_FOR_STORE);
        $this->data['total_sum_amount_income']=$total_sum_amount_income=$this->total_amount(CREDIT);
        $this->data['total_remainig_bal']=$total_sum_amount_income-$total_sum_amount;
        $this->admin_template->load('admin', 'petty_cash/index', $this->data);
    }
    public function total_amount($type) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

    public function total_amount_expense($type, $manage_for = 0, $manage_for1 = 0) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type,
                'managed_for' => $manage_for,
            ),
            'or_where' => array(
                'managed_for' => $manage_for1
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

}
