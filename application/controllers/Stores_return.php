<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stores_return extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Stock in';
    }

    public function save() {
        $logged_user = $this->session->userdata(ADMINS_STR_KEY);
        $item_id = $this->input->post('item_id');
        $item = $this->get_item_by_id($item_id);
        $quantity = $this->input->post('quantity');
        $description = $this->input->post('description');
        $purchase_price = $this->input->post('purchase_price');
        $unit = $this->input->post('unit');
        $base_unit = get_base_measurement_unit($unit);
        $posted_log_date = $this->input->post('log_date');
        $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
        $log_date = date_format($new_log_date, "Y-m-d");
        $total_bal_return = $quantity * $purchase_price;
        $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);

        $new_data_inventory = array(
            'item_category_id' => $item['category_id'],
            'item_id' => $item['id'],
            'quantity' => $quantity,
            'price' => $purchase_price,
            'unit' => $unit,
            'base_quantity' => $base_quantity,
            'base_unit' => $base_unit,
            'log_date' => $log_date,
            'type' => INVENTORY_TYPE_RETURN,
            'managed_for' => INVENTORY_MANAGED_FOR_GENERAL,
            'status' => ENABLE,
            'details' => $description,
        );
        $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data_inventory);

        $new_data = array(
            'amount' => $total_bal_return,
            'type' => DEBIT,
            'managed_for' => PAYMENT_MANAGED_FOR_STORE_RETURN,
            'description' => $this->input->post('description'),
            'created_transaction_by' => $logged_user['id'],
            'item_inventory_id' => $inserted_id
        );
        $this->BM->insert(TBL_PAYMENTS, $new_data);

        redirect('stores');
    }

    public function index($id = '', $remove = false) {
        $select_condition = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'CONCAT(items.name, " [", item_cat.name, "] ") as item_name'
            ),
            'where' => array(
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as item_cat',
                    'condition' => 'item_cat.id = items.category_id',
                ),
            ),
        );
        $items = $this->BM->get($select_condition);
        $total_balance = $this->get_store_balance($id);
        $this->data['total_balance'] = number_format((float) $total_balance['total_balance'], 2, '.', '');
        $this->data['items_options'] = prepare_data_for_dropdown($items, 'item_name', 'id', true, 'Select Item', '');
        $this->data['page_title'] = 'Stock in store';
        $this->admin_template->load('admin', 'stores/store_return', $this->data);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_ITEMS_INVENTORY,
            'fields' => array(
                'id', 'item_category_id', 'item_id', 'quantity', 'unit', 'price', 'log_date', 'details', 'status'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    function _get_data_by_id_return($id) {
        $condition = array(
            'table' => TBL_ITEMS_INVENTORY,
            'fields' => array(
                'item_category_id',
                'item_id',
                'quantity',
                'unit',
                'SUM(CASE items_inventory.type WHEN 1 THEN items_inventory.base_quantity WHEN 2 THEN -items_inventory.base_quantity WHEN 4 THEN -items_inventory.base_quantity ELSE 0 END) as stock_qty',
                TBL_ITEMS . '.unit_category',
            ),
            'where' => array(
                'item_id' => $id,
                TBL_ITEMS_INVENTORY . '.is_deleted' => 0,
                TBL_ITEMS_INVENTORY . '.status' => 1
            ),
            'group_by' => 'items_inventory.item_id',
            'join' => array(
                array(
                    'join_type' => 'left',
                    'table' => TBL_ITEMS_CATEGORY,
                    'condition' => TBL_ITEMS_INVENTORY . '.item_category_id = ' . TBL_ITEMS_CATEGORY . '.id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_ITEMS,
                    'condition' => TBL_ITEMS_INVENTORY . '.item_id = ' . TBL_ITEMS . '.id'
                ),
            ),
        );

        $rec = $this->BM->get($condition);
        foreach ($rec as $value) {
            $new_value = $value;
            if (isset($value['stock_qty'])) {
                $base_unit = get_base_measurement_unit_by_category($value['unit_category']);
                $default_unit = get_default_measurement_unit_by_category($value['unit_category']);
                $qty = convert_measurement_units($base_unit, $default_unit, $value['stock_qty'], false, 2);
                $new_value['stock_qty'] = $qty;
                $new_value['item_inventory_converted_unit'] = $default_unit;
            }
            $new_filter_records[] = $new_value;
        }
        return $new_filter_records[0];
    }

    public function get_items_units($item_id = '') {
        $response = array(
            'status' => 0,
            'message' => 'Something went wrong! please try again later.',
        );
        if (isset($item_id)) {
            $select_item_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'unit_category', 'purchase_price', 'sale_price'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_item_cond);
            $item_inventory = $this->_get_data_by_id_return($item['id']);
            if (isset($item) && isset($item['unit_category'])) {
                $units_options = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                $response_data = array(
                    'item' => $item,
                    'units_options' => $units_options,
                    'default_unit' => get_default_measurement_unit_by_category($item['unit_category']),
                    'inventory_rec' => $item_inventory
                );
                $response = array(
                    'status' => 1,
                    'message' => 'Success',
                    'data' => $response_data,
                );
            } else {
                $response['message'] = "Invalid item";
            }
        } else {
            $response['message'] = "Item id is required";
        }
        echo json_encode($response);
    }

    function get_item_by_id($item_id) {
        if (isset($item_id)) {
            $cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('id', 'category_id', 'name', 'unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE
                ),
            );
            $item = $this->BM->get_one($cond);
            if (isset($item)) {
                return $item;
            }
        }
        return NULL;
    }

    function get_store_balance($id = '') {
        $condition = array(
            'table' => TBL_PAYMENTS . ' as payments',
            'fields' => array(
                'SUM(CASE WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_RETURN . ' THEN payments.amount WHEN payments.managed_for = ' . PAYMENT_MANAGED_FOR_STORE_EXPENSE . ' THEN -payments.amount ELSE 0 END) as total_balance'
            ),
            'where' => array(
                'payments.type' => DEBIT
            ),
            'where_in' => array(
                'payments.managed_for' => array(PAYMENT_MANAGED_FOR_STORE, PAYMENT_MANAGED_FOR_STORE_EXPENSE, PAYMENT_MANAGED_FOR_STORE_RETURN)
            )
        );
        $store_balance = $this->BM->get_one($condition);
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            $prev_price = $prev_data['price'];
            $store_balance['total_balance'] += $prev_price;
        }
        return $store_balance;
    }

}
