<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Checkout Form';
    }

    public function index() {
        $this->data['page_title'] = 'Checkout Form';
        $this->data['url_new_record'] = base_url('admin/checking/checkout');
        $this->data['url_refresh'] = base_url('admin/checking/checkout');
        $rooms = $this->list_room();
        $this->data['room_lists'] = prepare_data_for_dropdown($rooms, 'name', 'id', true, 'Select Room', '');
        $this->admin_template->load('admin', 'checkout/save', $this->data);
    }
    function list_room() 
    {
        $condition = array(
            'table' => TBL_ROOMS,
            'fields' => array(
                'id', 'name'
            ),
            'where' => array(
                'status' => ENABLE,
                'availability_status' => ROOM_OCCUPIED
            )
        );
        return $this->BM->get($condition);
    }
    function check_transaction(){
       
        $room_id=$_GET['room_id'];
        $condition = array(
            'table' => TBL_TRANSACTIONS,
            'where' => array(
                TBL_TRANSACTIONS.'.room_id'=>$room_id,
                TBL_TRANSACTIONS.'.type' => DEBIT
            ),
            'join'=>array(
                array(
                    'join_type' => 'left',
                    'table' => TBL_ROOMS,
                    'condition' => TBL_TRANSACTIONS. '.room_id= ' . TBL_ROOMS  . '.id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_USERS,
                    'condition' => TBL_TRANSACTIONS. '.user_id= ' . TBL_USERS . '.id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_ROOMS_CATEGORY,
                    'condition' => TBL_ROOMS. '.type= ' . TBL_ROOMS_CATEGORY. '.id'
                ),
            )
        );
         
        $rec=$this->BM->get_one($condition);
        
        $bill_condition = array(
            'table' => TBL_TRANSACTIONS_MANAGEMENT,
            'where' => array(
                TBL_TRANSACTIONS_MANAGEMENT.'.room_id'=>$room_id,
                TBL_TRANSACTIONS_MANAGEMENT.'.type' => DEBIT
            ),            
        );
         
        $bill_rec=$this->BM->get($bill_condition);
         if ($rec && $bill_rec) {
            echo json_encode(array("type" => 'found', 'record' => $rec,'bill_rec'=>$bill_rec));
        } else {
            echo json_encode(array("type" => 'notfound'));
        }        
    }
    
    function save() {
        $room_no = $this->input->post('room_no');
        $transaction_id = $this->input->post('bill_no');
        $final_receive_amount = $this->input->post('final_receive_amount');
         $logged_user = $this->session->userdata(ADMINS_STR_KEY);
        if ($room_no) {
            $new_data_arr['availability_status'] = ROOM_AVAILABLE;
            $room_where_arr = array('id' => $room_no);
            $affected_records = $this->BM->update(TBL_ROOMS, $new_data_arr, $room_where_arr);
        }
        
        if ($transaction_id) {
          
            $transaction_data_arr['type'] = CREDIT;
            $transaction_data_arr['updated'] = DB_DATE_FORMAT;
           // $transaction_data_arr['checkout_date'] = $this->input->post('checkoutdate');
            
            /*
             * Update Transaction Table
             */
            
            $transaction_where_arr = array('bill_no' => $transaction_id);
            
            $this->BM->update(TBL_TRANSACTIONS, $transaction_data_arr, $transaction_where_arr);
            
            /*
             * Update Transaction Child Table
             */
            $transaction_data_arr_chlid['type'] = CREDIT;
            $transaction_child_where_arr = array('transaction_id' => $transaction_id);
            $this->BM->update(TBL_TRANSACTIONS_MANAGEMENT, $transaction_child_where_arr, $transaction_data_arr_chlid);
        }
        if($final_receive_amount){
            $checking_data = array(
                'user_id' => $this->session->userdata(ADMINS_STR_KEY)['id'],
                'room_id' => $room_no,
                'room_category_id' =>0,
                'agent_id' => 0,
                'tour_id' => 0,
                'checkin_date' => date(DB_DATE_FORMAT),
                'checkout_date' => date(DB_DATE_FORMAT),
                'type'=>INCOME,
            );
            $checking_inserted_id = $this->BM->insert(TBL_TRANSACTIONS, $checking_data);
            $transaction_insert_arr = array(
                'transaction_id' => $checking_inserted_id,
                'price' => $final_receive_amount,
                'description' => "SR.No. ".$transaction_id." Checkout Amount Recive",
                'create_transaction_by' => $this->session->userdata(ADMINS_STR_KEY)['id'],
                'type' => INCOME,
                'room_id' =>$room_no,
            );
            $this->BM->insert(TBL_TRANSACTIONS_MANAGEMENT, $transaction_insert_arr);
            
            if ($final_receive_amount > 0) {
                $payment_type=CREDIT;
                $desc='Checkout amount of SR. NO.' . $this->input->post('bill_no');
            }
            else{
                $payment_type=DEBIT;
                $desc='Pay checkout amount of SR. NO.' . $this->input->post('bill_no');
            }
            $payments_data = array(
                'transaction_id'=>$transaction_id,
                'amount' => $final_receive_amount,
                'description' => $desc,
                'type' => $payment_type,
                'created_transaction_by' => $logged_user['id'],
            );
            $this->BM->insert(TBL_PAYMENTS, $payments_data);
        }
        redirect(base_url('checkout'));
    }

}
