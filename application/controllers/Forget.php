<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forget extends CI_Controller {

    public function index() {

        $this->load->view('forget/index');
    }

    public function ForgotPassword() {
        $validate_fields = array(
            'email_id',
        );
        if ($this->_validate_login_form($validate_fields)) {
            
        }
        $email = $this->input->post('email_id');

        $select_cond = array(
            'table' => TBL_USERS,
            'fields' => array(
                'email','first_name'
            ),
            'where' => array(
                'email' => $this->input->post('email_id'),
                'status' => ENABLE,
                'is_deleted' => DISABLE,
                'role' => ADMINS,
            ),
        );
        $user = $this->BM->get_one($select_cond);

        $findemail = '';
        $name= '';
        if (isset($user['email']) && !empty($user['email'])) {
            $findemail = $user['email'];
            $name = $user['first_name'];
        }
        if ($findemail) {
            $this->sendpassword($findemail,$name);
        } else {
           $this->session->set_flashdata("email_error","<strong> $email </strong> not found,<br> Please enter correct email id");
            redirect('forget');
        }
    }

    function _validate_login_form($validate_fields) {
        $validation_rules = array();
        if (in_array('email_id', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'email_id',
                'label' => 'Email',
                'rules' => 'trim|required|min_length[5]|max_length[32]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    public function sendpassword($email,$name) {
       
        if ($email) {
            $passwordplain = "";
            $passwordplain = rand(MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER);
            
            $mail_message = 'Dear ' . $name . ',' . "\r\n";
            $mail_message .= 'Thanks for contacting regarding to forgot password,<br> Your <b>Password</b> is <b>' . $passwordplain . '</b>' . "\r\n";
            $mail_message .= '<br>Please Update your password.';
            $mail_message .= '<br>Thanks & Regards';
            $mail_message .= '<br>Lake view hotel';
             $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => 465,
                'smtp_user' => 'sarfaraj.kazi00@gmail.com', // change it to yours
                'smtp_pass' => '90332005400', // change it to yours
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
              );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $this->email->from('info@whitedemo.cf'); // change it to yours
            $this->email->to($email);
            $this->email->subject('Forget password request');
            $this->email->message($mail_message);
            
            
            if ($this->email->send()) {
                $new_data['password'] = md5($passwordplain);
                $new_data['modified_date'] = date('Y-m-d H:i:s');
                $where = array('email' => $email);
                $this->BM->update(TBL_USERS, $new_data, $where);
                $this->session->set_flashdata("email_msg","Password sent to your email!");
            } else {
                show_error($this->email->print_debugger());                
                die();
                $this->session->set_flashdata("email_error","Failed to send password, please try again!");
            }
            redirect('forget');
        }
    }

}
