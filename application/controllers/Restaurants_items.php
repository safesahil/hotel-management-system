<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Restaurants_items extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Restaurants Items';
    }

    public function index() {
        $this->data['page_title'] = 'Restaurants Items listing';
        $this->data['unit_category_options'] = MEASUREMENT_UNITS_CATEGORIES;
        $this->admin_template->load('admin', 'restaurants_items/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());

        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_RESTAURANTS_ITEMS_CATEGORY,
                'condition' => TBL_RESTAURANTS_ITEMS . '.category_id = ' . TBL_RESTAURANTS_ITEMS_CATEGORY . '.id'
            )
        );

        $filter_records = $this->BM->get_filtered_records(TBL_RESTAURANTS_ITEMS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_RESTAURANTS_ITEMS, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_RESTAURANTS_ITEMS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = '') {
        if ($id) {
            $this->data['prev_data'] = $this->_get_data_by_id($id);
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'item_name',
                'item_category',
                'sale_price',
                'unit_category',
                'status'
            );
            if ($this->_validate_form($validate_fields)) {
                $new_data = array(
                    'name' => $this->input->post('item_name'),
                    'category_id' => $this->input->post('restaurants_item_category'),
                    'sale_price' => $this->input->post('sale_price'),
                    'unit_category' => $this->input->post('unit_category'),
                    'status' => $this->input->post('status'),
                );
                if ($id) {
                    $new_data['modified_date'] = date('Y-m-d H:i:s');
                    $where = array('id' => $id);
                    $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS, $new_data, $where);
                    if (isset($affected_records)) {
                        $this->session->set_flashdata('success_msg', 'Item saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                } else {
                    $inserted_id = $this->BM->insert(TBL_RESTAURANTS_ITEMS, $new_data);
                    if (isset($inserted_id)) {
                        $this->session->set_flashdata('success_msg', 'Item saved.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                }
                $url = base_url('restaurants_items');
                redirect($url);
            }
        }
        $this->data['page_title'] = 'Add items';
        $item_categories = $this->_get_item_categories();
        $this->data['item_categories'] = prepare_data_for_dropdown($item_categories, 'name', 'id', true, 'Select category', '');
        $restaurants_items_decode = json_decode(MEASUREMENT_UNITS_CATEGORIES, true);
        $this->data['unit_category_options'] = array_merge(array('' => 'Select unit category'), $restaurants_items_decode);
        $this->admin_template->load('admin', 'restaurants_items/save', $this->data);
    }

    public function delete($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 1, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item deleted.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('restaurants_items');
        redirect($url);
    }

    public function recover($id = '') {
        if ($id) {
            $prev_data = $this->_get_data_by_id($id);
            if (isset($prev_data)) {
                $new_data = array('is_deleted' => 0, 'modified_date' => date('Y-m-d H:i:s'));
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_RESTAURANTS_ITEMS, $new_data, $where);
                if (isset($affected_records)) {
                    $this->session->set_flashdata('success_msg', 'Item recovered.');
                } else {
                    $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                }
            } else {
                $this->session->set_flashdata('error_msg', 'Invalid request!');
            }
        } else {
            $this->session->set_flashdata('error_msg', 'Invalid url! please check url.');
        }
        $url = base_url('restaurants_items');
        redirect($url);
    }

    function _get_data_by_id($id) {
        $condition = array(
            'table' => TBL_RESTAURANTS_ITEMS,
            'fields' => array(
                'id', 'category_id', 'name', 'sale_price', 'unit_category', 'status'
            ),
            'where' => array(
                'id' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

    function _get_item_categories() {
        $condition = array(
            'table' => TBL_RESTAURANTS_ITEMS_CATEGORY,
            'fields' => array(
                'id', 'name'
            )
        );
        return $this->BM->get($condition);
    }

    function _validate_form($validate_fields) {
        $validation_rules = array();
        if (in_array('item_name', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'item_name',
                'label' => 'Name',
                'rules' => 'trim|required|min_length[2]|max_length[50]'
            );
        }
        if (in_array('restaurants_item_category', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'restaurants_item_category',
                'label' => 'Item category',
                'rules' => 'trim|required|callback__validate_restaurants_item_category'
            );
        }
        if (in_array('sale_price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'sale_price',
                'label' => 'Sale price',
                'rules' => 'trim|required|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000]'
            );
        }
        if (in_array('unit_category', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit_category',
                'label' => 'Unit category',
                'rules' => 'trim|required|callback__validate_unit_category'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_restaurants_item_category($value) {
        if ($value) {
            $item_categories = $this->_get_item_categories();
            foreach ($item_categories as $k => $v) {
                if ($value == $v['id']) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_restaurants_item_category', 'Invalid item category.');
            return FALSE;
        }
    }

    function _validate_unit_category($value) {
        if ($value) {
            foreach (json_decode(MEASUREMENT_UNITS_CATEGORIES, true) as $k => $v) {
                if ($value === $k) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit_category', 'Invalid unit category.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

}
