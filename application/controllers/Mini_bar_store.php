<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mini_bar_store extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Mini bar store';
    }

    public function index() {
        $this->data['page_title'] = 'Rooms listing';
        $this->admin_template->load('admin', 'mini_bar_store/index', $this->data);
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['where'][TBL_ROOMS . '.status'] = ENABLE;
        $filter_array['where'][TBL_ROOMS . '.is_deleted'] = DISABLE;

        $filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ROOMS, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ROOMS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function details($room_id) {
        $select_cond = array(
            'table' => TBL_ROOMS,
            'fields' => array('id', 'name', 'details', 'type', 'price', 'pax', 'availability_status'),
            'where' => array(
                'id' => $room_id,
                'status' => ENABLE,
                'is_deleted' => DISABLE,
            ),
        );
        $room = $this->BM->get_one($select_cond);
        if (isset($room)) {
            $this->data['room'] = $room;
            $this->data['page_title'] = $room['name'] . ' Bar items';
            $this->admin_template->load('admin', 'mini_bar_store/details', $this->data);
        } else {
            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
            $url = base_url('mini_bar_store');
            redirect($url);
        }
    }

    public function filter_items($room_id) {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['fields'][] = 'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_OUT . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_MINI_BAR_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty';
        $filter_array['fields'][] = TBL_ITEMS . '.unit_category';
        $filter_array['where'][TBL_ITEMS_INVENTORY . '.room_id'] = $room_id;
        $filter_array['where'][TBL_ITEMS_INVENTORY . '.managed_for'] = INVENTORY_MANAGED_FOR_MINI_BAR;

        $filter_array['group_by'][] = TBL_ITEMS_INVENTORY . '.item_id';

        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS_CATEGORY,
                'condition' => TBL_ITEMS_INVENTORY . '.item_category_id = ' . TBL_ITEMS_CATEGORY . '.id'
            ),
            array(
                'join_type' => 'left',
                'table' => TBL_ITEMS,
                'condition' => TBL_ITEMS_INVENTORY . '.item_id = ' . TBL_ITEMS . '.id'
            ),
        );

        $filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ITEMS_INVENTORY, $filter_array, 1);
        $new_filter_records = [];
        foreach ($filter_records as $value) {
            $new_value = $value;
            if (isset($value['stock_qty'])) {
                $base_unit = get_base_measurement_unit_by_category($value['unit_category']);
                $default_unit = get_default_measurement_unit_by_category($value['unit_category']);
                $qty = convert_measurement_units($base_unit, $default_unit, $value['stock_qty'], true, 2);
                $new_value['stock_qty'] = $qty;
                $new_value['item_inventory_converted_unit'] = $default_unit;
            }
            $new_filter_records[] = $new_value;
        }

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array("table" => TBL_ITEMS_INVENTORY)),
            "recordsFiltered" => $total_filter_records,
            "data" => $new_filter_records,
        );
        echo json_encode($output);
    }

    public function add($room_id, $item_id) {
        $room = $this->get_room_by_id($room_id);
        $item = $this->get_item_by_id($item_id);
        if (isset($room) && isset($item)) {
            if ($this->input->post()) {
                $validate_fields = array(
                    'log_date',
                    'quantity',
                    'unit',
                    'price',
                    'status',
                    'description',
                );
                $meta = array('item_id' => $item_id, 'room_id' => $room_id);
                if ($this->_validate_form($validate_fields, $meta)) {
                    $unit = $this->input->post('unit');
                    $base_unit = get_base_measurement_unit($unit);
                    $quantity = $this->input->post('quantity');
                    $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                    $posted_log_date = $this->input->post('log_date');
                    $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                    $log_date = date_format($new_log_date, "Y-m-d");
                    $new_data = array(
                        'item_category_id' => $item['category_id'],
                        'item_id' => $item['id'],
                        'room_id' => $room['id'],
                        'quantity' => $quantity,
                        'unit' => $unit,
                        'price' => $this->input->post('price'),
                        'base_quantity' => $base_quantity,
                        'base_unit' => $base_unit,
                        'log_date' => $log_date,
                        'type' => INVENTORY_TYPE_OUT,
                        'managed_for' => INVENTORY_MANAGED_FOR_MINI_BAR,
                        'status' => $this->input->post('status'),
                        'details' => $this->input->post('description'),
                    );
                    $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                    if (isset($inserted_id)) {
                        $this->session->set_flashdata('success_msg', 'Item quantity added.');
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                    }
                    $url = base_url('mini_bar_store/details/' . $room_id);
                    redirect($url);
                }
            }
            $unit_category = $item['unit_category'];
            $units_options = json_decode(MEASUREMENT_UNITS, true)[$unit_category];
            $base_item_unit = get_base_measurement_unit_by_category($unit_category);
            $base_item_stock = $this->get_item_stock($item_id);
            $default_item_unit = get_default_measurement_unit_by_category($unit_category);
            $item_stock = convert_measurement_units($base_item_unit, $default_item_unit, $base_item_stock['stock_qty'], true, 2);
            $this->data['room'] = $room;
            $this->data['item'] = $item;
            $this->data['units_options'] = $units_options;
            $this->data['item_stock'] = $item_stock;
            $this->data['item_unit'] = $default_item_unit;
            $this->data['page_title'] = 'Add quantity to bar';
            $this->admin_template->load('admin', 'mini_bar_store/add', $this->data);
        } else {
            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
            $url = base_url('mini_bar_store/details/' . $room_id);
            redirect($url);
        }
    }

    public function consume($room_id, $item_id) {
        $room = $this->get_room_by_id($room_id);
        $item = $this->get_item_by_id($item_id);
        $last_transaction_data = $this->_get_last_transaction_data_by_room_id($room_id);
        $last_transaction_count = $this->_get_last_transactions_count_by_room_id($room_id);
        if (isset($room) && isset($item) && isset($last_transaction_data)) {
            if ($this->input->post()) {
                $validate_fields = array(
                    'log_date',
                    'consume_quantity',
                    'unit',
                    'price',
                    'status',
                    'description',
                );
                $meta = array('item_id' => $item_id, 'room_id' => $room_id);
                if ($this->_validate_form($validate_fields, $meta)) {
                    $logged_user = $this->session->userdata(ADMINS_STR_KEY);
                    $price = $this->input->post('price');
                    $unit = $this->input->post('unit');
                    $base_unit = get_base_measurement_unit($unit);
                    $quantity = $this->input->post('quantity');
                    $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
                    $posted_log_date = $this->input->post('log_date');
                    $new_log_date = date_create_from_format('m/d/Y', $posted_log_date);
                    $log_date = date_format($new_log_date, "Y-m-d");
                    $new_data = array(
                        'item_category_id' => $item['category_id'],
                        'item_id' => $item['id'],
                        'room_id' => $room['id'],
                        'quantity' => $quantity,
                        'unit' => $unit,
                        'price' => $price,
                        'base_quantity' => $base_quantity,
                        'base_unit' => $base_unit,
                        'log_date' => $log_date,
                        'type' => INVENTORY_TYPE_MINI_BAR_OUT,
                        'managed_for' => INVENTORY_MANAGED_FOR_MINI_BAR,
                        'status' => $this->input->post('status'),
                        'details' => $this->input->post('description'),
                    );
                    $inserted_id = $this->BM->insert(TBL_ITEMS_INVENTORY, $new_data);
                    if (isset($inserted_id)) {
                        $new_data = array(
                            'transaction_id' => $last_transaction_data['id'],
                            'item_inventory_id' => $inserted_id,
                            'price' => $price,
                            'type' => DEBIT,
                            'create_transaction_by' => isset($logged_user['id']) ? $logged_user['id'] : '0',
                            'room_id' => $room_id,
                            'description' => $item['item_name'],
                            'priority' => $last_transaction_count,
                        );
                        $inserted_id = $this->BM->insert(TBL_TRANSACTIONS_MANAGEMENT, $new_data);
                        if (isset($inserted_id)) {
                            $this->session->set_flashdata('success_msg', 'Item quantity added.');
                        } else {
                            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
                        }
                    } else {
                        $this->session->set_flashdata('error_msg', 'Something went wrong while transaction! please try again later.');
                    }
                    $url = base_url('mini_bar_store/details/' . $room_id);
                    redirect($url);
                }
            }
            $unit_category = $item['unit_category'];
            $units_options = json_decode(MEASUREMENT_UNITS, true)[$unit_category];
            $base_item_unit = get_base_measurement_unit_by_category($unit_category);
            $base_item_stock = $this->get_mini_bar_stock($room_id, $item_id);
            $default_item_unit = get_default_measurement_unit_by_category($unit_category);
            $item_stock = convert_measurement_units($base_item_unit, $default_item_unit, $base_item_stock['stock_qty'], true, 2);
            $this->data['room'] = $room;
            $this->data['item'] = $item;
            $this->data['units_options'] = $units_options;
            $this->data['item_stock'] = $item_stock;
            $this->data['item_unit'] = $default_item_unit;
            $this->data['page_title'] = 'Consume from bar';
            $this->admin_template->load('admin', 'mini_bar_store/consume', $this->data);
        } else {
            $this->session->set_flashdata('error_msg', 'Something went wrong! please try again later.');
            $url = base_url('mini_bar_store/details/' . $room_id);
            redirect($url);
        }
    }

    function get_room_by_id($room_id) {
        $select = array(
            'table' => TBL_ROOMS,
            'fields' => array(
                'id', 'name'
            ),
            'where' => array(
                'id' => $room_id,
                'status' => ENABLE,
                'is_deleted' => DISABLE,
            )
        );
        return $this->BM->get_one($select);
    }

    function get_item_by_id($item_id) {
        $select = array(
            'table' => TBL_ITEMS . ' as items',
            'fields' => array(
                'items.id', 'CONCAT(items.name , " [", items_category.name, "] ") as item_name',
                'items.purchase_price', 'items.sale_price', 'items.unit_category', 'items.category_id'
            ),
            'where' => array(
                'items.id' => $item_id,
                'items.status' => ENABLE,
                'items.is_deleted' => DISABLE,
            ),
            'join' => array(
                array(
                    'table' => TBL_ITEMS_CATEGORY . ' as items_category',
                    'condition' => 'items_category.id = items.category_id'
                )
            )
        );
        return $this->BM->get_one($select);
    }

    function get_item_stock($item_id) {
        $select = array(
            'table' => TBL_ITEMS_INVENTORY . ' as items_inventory',
            'fields' => array(
                'SUM(CASE items_inventory.type WHEN ' . INVENTORY_TYPE_IN . ' THEN items_inventory.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -items_inventory.base_quantity ELSE 0 END) as stock_qty'
            ),
            'where' => array(
                'items_inventory.item_id' => $item_id,
                'items_inventory.status' => ENABLE,
                'items_inventory.is_deleted' => DISABLE,
            ),
            'group_by' => array(
                'items_inventory.item_id'
            ),
        );
        return $this->BM->get_one($select);
    }

    function get_mini_bar_stock($room_id, $item_id) {
        $select = array(
            'table' => TBL_ITEMS_INVENTORY . ' as items_inventory',
            'fields' => array(
                'SUM(CASE items_inventory.type WHEN ' . INVENTORY_TYPE_OUT . ' THEN items_inventory.base_quantity WHEN ' . INVENTORY_TYPE_MINI_BAR_OUT . ' THEN -items_inventory.base_quantity ELSE 0 END) as stock_qty'
            ),
            'where' => array(
                'items_inventory.room_id' => $room_id,
                'items_inventory.item_id' => $item_id,
                'items_inventory.status' => ENABLE,
                'items_inventory.is_deleted' => DISABLE,
                'items_inventory.managed_for' => INVENTORY_MANAGED_FOR_MINI_BAR,
            ),
            'group_by' => array(
                'items_inventory.item_id'
            ),
        );
        return $this->BM->get_one($select);
    }

    function _get_last_transaction_data_by_room_id($id) {
        $condition = array(
            'table' => TBL_TRANSACTIONS . ' as transactions',
            'fields' => array(
                'transactions.id', 'transactions.user_id',
                'transactions.room_id', 'transactions.room_category_id', 'transactions.agent_id',
                'transactions.tour_id', 'transactions.checkin_date', 'transactions.checkout_date', 'transactions.narration'
            ),
            'where' => array(
                'transactions.room_id' => $id,
                'transactions.is_deleted' => DISABLE,
            ),
            'order_by' => array(
                'transactions.id' => 'desc'
            )
        );
        return $this->BM->get_one($condition);
    }

    function _get_last_transactions_count_by_room_id($room_id) {
        $bill_condition = array(
            'table' => TBL_TRANSACTIONS_MANAGEMENT,
            'where' => array(
                TBL_TRANSACTIONS_MANAGEMENT . '.room_id' => $room_id,
                TBL_TRANSACTIONS_MANAGEMENT . '.type' => DEBIT
            ),
        );
        $count = $this->BM->count($bill_condition);
        $count +=1;
        return $count;
    }

    function _validate_form($validate_fields, $meta) {
        $validation_rules = array();
        if (in_array('log_date', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'log_date',
                'label' => 'In Date',
                'rules' => 'trim|required'
            );
        }
        if (in_array('quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|numeric|greater_than_equal_to[1]|less_than_equal_to[100000]|callback__check_stock[' . $meta['item_id'] . ']'
            );
        }
        if (in_array('consume_quantity', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'quantity',
                'label' => 'Quantity',
                'rules' => 'trim|required|numeric|greater_than_equal_to[1]|less_than_equal_to[100000]|callback__check_mini_bar_stock[' . json_encode($meta) . ']'
            );
        }
        if (in_array('unit', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'unit',
                'label' => 'Units',
                'rules' => 'trim|required|callback__validate_unit[' . $meta['item_id'] . ']'
            );
        }
        if (in_array('price', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'price',
                'label' => 'Price',
                'rules' => 'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[1000000]'
            );
        }
        if (in_array('status', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'trim|required|callback__validate_status'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _validate_unit($value, $item_id) {
        if ($value && $item_id) {
            $select_cond = array(
                'table' => TBL_ITEMS,
                'fields' => array('unit_category'),
                'where' => array(
                    'id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
            );
            $item = $this->BM->get_one($select_cond);
            if (isset($item) && isset($item['unit_category'])) {
                $units = json_decode(MEASUREMENT_UNITS, true)[$item['unit_category']];
                if (isset($units) && isset($units[$value])) {
                    return TRUE;
                }
            }
            $this->form_validation->set_message('_validate_unit', 'Invalid unit.');
            return FALSE;
        }
    }

    function _validate_status($value) {
        if ($value === '1' || $value === '0') {
            return TRUE;
        }
        $this->form_validation->set_message('_validate_status', 'Invalid status.');
        return FALSE;
    }

    function _check_stock($value, $item_id) {
        if ($value) {
            $select_inventory_cond = array(
                'table' => TBL_ITEMS_INVENTORY,
                'fields' => array(
                    'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_IN . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                ),
                'where' => array(
                    'item_id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                ),
                'group_by' => array(
                    'item_id'
                ),
            );
            $inventory = $this->BM->get_one($select_inventory_cond);
            $unit = $this->input->post('unit');
            $base_unit = get_base_measurement_unit($unit);
            $quantity = $this->input->post('quantity');
            $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
            $prev_stock_qty = $inventory['stock_qty'];
            if ($base_quantity <= $prev_stock_qty) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_stock', '%s can not be more than stock.');
            return FALSE;
        }
    }

    function _check_mini_bar_stock($value, $encoded_meta) {
        if ($value) {
            $meta = json_decode($encoded_meta, true);
            $room_id = $meta['room_id'];
            $item_id = $meta['item_id'];
            $select_inventory_cond = array(
                'table' => TBL_ITEMS_INVENTORY,
                'fields' => array(
                    'SUM(CASE ' . TBL_ITEMS_INVENTORY . '.type WHEN ' . INVENTORY_TYPE_OUT . ' THEN ' . TBL_ITEMS_INVENTORY . '.base_quantity WHEN ' . INVENTORY_TYPE_MINI_BAR_OUT . ' THEN -' . TBL_ITEMS_INVENTORY . '.base_quantity ELSE 0 END) as stock_qty'
                ),
                'where' => array(
                    'room_id' => $room_id,
                    'item_id' => $item_id,
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                    'managed_for' => INVENTORY_MANAGED_FOR_MINI_BAR,
                ),
                'group_by' => array(
                    'item_id'
                ),
            );
            $inventory = $this->BM->get_one($select_inventory_cond);
            $unit = $this->input->post('unit');
            $base_unit = get_base_measurement_unit($unit);
            $quantity = $this->input->post('quantity');
            $base_quantity = convert_measurement_units($unit, $base_unit, $quantity);
            $prev_stock_qty = $inventory['stock_qty'];
            if ($base_quantity <= $prev_stock_qty) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_mini_bar_stock', '%s can not be more than stock.');
            return FALSE;
        }
    }

}
