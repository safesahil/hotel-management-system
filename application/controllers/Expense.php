<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Expense extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Expense';
    }

    public function index() {

        $this->data['page_title'] = 'Expense';
        $this->data['url_new_record'] = base_url('expense/');
        $this->data['url_refresh'] = base_url('expense');
        $total_sum_amount_income = $this->total_amount(CREDIT);
        $this->data['total_sum_amount'] = $this->total_amount_expense(DEBIT, PAYMENT_MANAGED_FOR_GENERAL, PAYMENT_MANAGED_FOR_STORE);
        $this->data['total_sum_amount_income'] = $total_sum_amount_income - $this->data['total_sum_amount'];
        $this->admin_template->load('admin', 'expense/index', $this->data);
    }

    public function total_amount($type) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

    public function total_amount_expense($type, $manage_for = 0, $manage_for1 = 0) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type,
                'managed_for' => $manage_for,
            ),
            'or_where' => array(
                'managed_for' => $manage_for1
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

    public function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['custom_or_where'] = array(TBL_PAYMENTS . '.type' => array(DEBIT), TBL_PAYMENTS . '.managed_for' => array(PAYMENT_MANAGED_FOR_GENERAL, PAYMENT_MANAGED_FOR_STORE, PAYMENT_MANAGED_FOR_STORE_RETURN));
        $filter_array['join'] = array(
            array(
                'join_type' => 'left',
                'table' => TBL_BANK,
                'condition' => TBL_PAYMENTS . '.payment_mode = ' . TBL_BANK . '.id'
            )
        );
        $filter_array['order'][TBL_PAYMENTS . '.created'] = 'desc';
        $filter_records = $this->BM->get_filtered_records(TBL_PAYMENTS, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_PAYMENTS, $filter_array, 1);
        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array('table' => TBL_PAYMENTS)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );
        echo json_encode($output);
    }

    public function save($id = null) {
        $total_sum_amount_income = $this->total_amount(CREDIT);
        $logged_user = $this->session->userdata(ADMINS_STR_KEY);
        $this->data['total_sum_amount'] = $this->total_amount_expense(DEBIT, PAYMENT_MANAGED_FOR_GENERAL, PAYMENT_MANAGED_FOR_STORE);
        $this->data['total_sum_amount_income'] = $total_sum_amount_income - $this->data['total_sum_amount'];
        if ($this->input->post()) {
            $validate_fields = array(
                'amount',
                'description'
            );
            $transefer_bank_id = 0;
            if (isset($_REQUEST['transfer']) && $_REQUEST['transfer'] == 'bank') {
                $transefer_bank_id = $this->input->post('bank_name');
            }

            if ($this->_validate_form($validate_fields)) {
                $new_data = array(
                    'amount' => $this->input->post('amount'),
                    'description' => $this->input->post('description'),
                    'type' => DEBIT,
                    'created_transaction_by' => $logged_user['id'],
                    'managed_for' => PAYMENT_MANAGED_FOR_GENERAL,
                    'payment_mode' => $transefer_bank_id,
                );
                $inserted_id = $this->BM->insert(TBL_PAYMENTS, $new_data);
                $url = base_url('expense');
                if ($this->input->post('saveClose') == 1) {
                    $url = base_url('expense/save');
                }

                redirect($url);
            }
        }

        $banks = $this->_get_banks();
        $this->data['banks'] = prepare_data_for_dropdown($banks, 'name', 'id', true, 'Select bank', '');
        $this->data['back_url'] = 'expense';
        $this->data['post_url'] = 'expense/save/' . $id;
        $this->data['page_title'] = 'Save';
        $this->admin_template->load('admin', 'expense/save', $this->data);
    }

    public function store() {
        $total_sum_amount_income = $this->total_amount(CREDIT);
        $this->data['total_sum_amount'] = $this->total_amount(DEBIT);
        $this->data['total_sum_amount_income'] = $total_sum_amount_income - $this->data['total_sum_amount'];
        if ($this->input->post()) {
            $validate_fields = array(
                'amount',
                'description'
            );
            if ($this->_validate_form($validate_fields)) {
                $new_data = array(
                    'amount' => $this->input->post('amount'),
                    'description' => $this->input->post('description'),
                    'type' => DEBIT,
                    'managed_for' => PAYMENT_MANAGED_FOR_STORE
                );
                $inserted_id = $this->BM->insert(TBL_PAYMENTS, $new_data);

                $url = base_url('expense');
                if ($this->input->post('saveClose') == 1) {
                    $url = base_url('expense/store');
                }

                redirect($url);
            }
        }

        $this->data['back_url'] = 'expense';
        $this->data['post_url'] = 'expense/store';
        $this->data['page_title'] = 'Transfer to store';
        $this->admin_template->load('admin', 'expense/store', $this->data);
    }

    function _validate_form($validate_fields) {
        $validation_rules = array();
        if (in_array('amount', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'amount',
                'label' => 'Amount',
                'rules' => 'trim|required'
            );
        }
        if (in_array('description', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'description',
                'label' => 'Description',
                'rules' => 'trim|max_length[250]'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _get_banks() {
        $condition = array(
            'table' => TBL_BANK,
            'fields' => array(
                'id', 'name'
            )
        );
        return $this->BM->get($condition);
    }

}
