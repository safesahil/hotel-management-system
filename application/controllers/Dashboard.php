<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_controller {

    public function index($room_cat_id = '') {
        $this->data['title'] = $this->data['page_title'] = $this->data['page_header'] = 'Dashboard';
        $rooms = [];
        $select_room_cat = array(
            'table' => TBL_ROOMS_CATEGORY . ' as rc',
            'fields' => array('rc.id', 'rc.name', 'rc.status'),
            'where' => array('rc.is_deleted' => DISABLE),
            'order_by' => array('rc.name' => 'asc')
        );
        $room_cats = $this->BM->get($select_room_cat);
        if (isset($room_cats)) {
            if (!$room_cat_id) {
                $room_cat_id = $room_cats[0]['id'];
            }
            $select_rooms = array(
                'table' => TBL_ROOMS . ' as r',
                'fields' => array(
                    'r.id', 'r.name', 'r.type', 'r.price', 'r.pax', 'r.availability_status', 'r.status',
                ),
                'where' => array('r.type' => $room_cat_id),
                'order_by' => array('r.name' => 'asc'),
            );
            $rooms = $this->BM->get($select_rooms);
        }
        $this->data['room_cat_id'] = $room_cat_id;
        $this->data['room_cats'] = isset($room_cats) ? $room_cats : [];
        $this->data['rooms'] = isset($rooms) ? $rooms : [];
        $tab_content = $this->load->view('dashboard/list_rooms', $this->data, true);
        $this->data['tab_content'] = $tab_content;
        $this->data['total_sum_amount'] = $total_sum_amount=$this->total_amount_expense(DEBIT,PAYMENT_MANAGED_FOR_GENERAL,PAYMENT_MANAGED_FOR_STORE);
        $this->data['total_sum_amount_income']=$total_sum_amount_income=$this->total_amount(CREDIT);
        $this->data['total_remainig_bal']=$total_sum_amount_income-$total_sum_amount;
        $this->admin_template->load('admin', 'dashboard/index', $this->data);
    }
    public function total_amount($type) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

    public function total_amount_expense($type, $manage_for = 0, $manage_for1 = 0) {
        $condition = array(
            'table' => TBL_PAYMENTS,
            'fields' => array(
                'SUM(amount) as total_amount'
            ),
            'where' => array(
                'type' => $type,
                'managed_for' => $manage_for,
            ),
            'or_where' => array(
                'managed_for' => $manage_for1
            )
        );
        $fetch_rec = $this->BM->get_one($condition);
        if (isset($fetch_rec['total_amount'])) {
            return $fetch_rec['total_amount'];
        } else {
            return 0;
        }
    }

}
