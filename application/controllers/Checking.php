<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checking extends Admin_controller {

    public function __construct() {
        parent::__construct();
        $this->data['title'] = $this->data['page_header'] = 'Checking Form';
    }

    public function index() {

        $this->data['page_title'] = 'Checking Form';
        $this->data['url_new_record'] = base_url('admin/checking/');
        $this->data['url_refresh'] = base_url('admin/checking');
        $room_categories = $this->_get_room_categories(TBL_ROOMS_CATEGORY);
        $agents_rec = $this->_get_users_rec(TBL_USERS, AGENTS);
        $tour_rec = $this->_get_users_rec(TBL_USERS, TOURS);
        $this->data['rooms'] = prepare_data_for_dropdown($room_categories, 'name', 'id', true, 'Select Room Type', '');
        $this->data['agents'] = prepare_data_for_dropdown($agents_rec, 'name', 'id', true, 'Select Agent', '');
        $this->data['tours'] = prepare_data_for_dropdown($agents_rec, 'name', 'id', true, 'Select Tour Company', '');
        $this->admin_template->load('admin', 'checking/save', $this->data);
    }

    function _get_room_categories($table_name) {
        $condition = array(
            'table' => $table_name,
            'fields' => array(
                'id', 'name'
            )
        );
        return $this->BM->get($condition);
    }

    function _get_users_rec($table_name, $type) {
        $condition = array(
            'table' => $table_name,
            'fields' => array(
                'id', 'first_name', 'last_name'
            ),
            'where' => array(
                'role' => $type,
            ),
        );
        return $this->BM->get($condition);
    }

    function findRoom() {
        $room_cat_id = $_POST['room_cat_id'];
        if ($room_cat_id) {
            $return_html = '';
            $condition = array(
                'table' => TBL_ROOMS,
                'fields' => array(
                    'id', 'name'
                ),
                'where' => array(
                    'type' => $room_cat_id,
                    'status' => 1,
                    'availability_status' => 1,
                    'is_deleted' => 0,
                )
            );
            $fetch_rec = $this->BM->get($condition);
            if ($fetch_rec) {
                $return_html = '<option hidden value="">Select Room No.</option>';
                foreach ($fetch_rec as $single_arr) {
                    $return_html .= '<option value="' . $single_arr['id'] . '">' . $single_arr['name'] . '</option>';
                }
            } else {
                $return_html = "<option hidden>Room Not Found</option>";
            }
            echo json_encode($return_html);
        }
    }

    public function genrate_bill_no() {
        $data = $this->BM->get_bill_no();
        echo json_encode($data);
    }

    public function check_user_details() {

        $mobile_no = $_POST['user_mobile_no'];
        $condition = array(
            'table' => TBL_USERS,
            'fields' => array(
                'id', 'first_name', 'last_name', 'email', 'mobile_number',
                'address', 'proof', 'proof_relative', 'capture_image','gender', 'role', 'status',
                'is_deleted', 'created_date', 'modified_date'
            ),
            'where' => array(
                'mobile_number' => $mobile_no,
                'status'=>ENABLE,
                'is_deleted'=>DISABLE,
            ),
        );
        $row_data = $this->BM->get_one($condition);
        if ($row_data) {
            echo json_encode(array("type" => 'found', 'prev_rec' => $row_data));
        } else {
            echo json_encode(array("type" => 'notfound'));
        }
    }

    public function room_deatails() {
        $room_id = $_POST['room_id'];
        $condition = array(
            'table' => TBL_ROOMS,
            'where' => array(
                'id' => $room_id
            ),
        );
        $row_data = $this->BM->get_one($condition);
        if ($row_data) {
            echo json_encode(array("type" => 'found', 'prev_rec' => $row_data));
        } else {
            echo json_encode(array("type" => 'notfound'));
        }
    }

    public function save() {

        $prev_data = $this->_get_data_by_mobile($this->input->post('mobile_number'));
        $id = $prev_data['id'];
        $upload_image = TRUE;
        $file_source = NULL;
        $file_source_relative = NULL;
        $uploaded_files = [];
         $logged_user = $this->session->userdata(ADMINS_STR_KEY);
        if ($id) {
            if (isset($_FILES['proof']) && isset($_FILES['proof']['name']) && isset($_FILES['proof']['error']) && $_FILES['proof']['error'] == '0') {
                if (isset($prev_data['proof_relative'])) {
                    delete_upload_files(array($prev_data['proof_relative']));
                }
                $upload_image = TRUE;
            } else {
                $file_source = (isset($prev_data['proof'])) ? $prev_data['proof'] : NULL;
                $file_source_relative = (isset($prev_data['proof_relative'])) ? $prev_data['proof_relative'] : NULL;
                $upload_image = FALSE;
            }
        }
        if ($upload_image) {
            $uploaded_files = upload_files($_FILES, 'proof', 'users');
        }
        $save_data = FALSE;
        if (isset($uploaded_files) && isset($uploaded_files[0]) && $uploaded_files[0]['status'] == 1) {
            $save_data = TRUE;
            $file_source = $uploaded_files[0]['destination'];
            $file_source_relative = $uploaded_files[0]['destination_relative'];
        } else if (!$upload_image) {
            $save_data = TRUE;
        }
        $userdata = $_POST['guset_info'];
        if ($save_data) {
            $new_data = array(
                'first_name' => $userdata['first_name'],
                'last_name' => $userdata['last_name'],
                'mobile_number' => $this->input->post('mobile_number'),
                'address' => $userdata['address'],
                'gender' => $userdata['gender'],
                'proof' => (isset($file_source)) ? $file_source : NULL,
                'proof_relative' => (isset($file_source_relative)) ? $file_source_relative : NULL,
                'role' => CUSTOMERS,
                'status'=>ENABLE,
            );
            if ($id) {
                $new_data['modified_date'] = date('Y-m-d H:i:s');
                $where = array('id' => $id);
                $affected_records = $this->BM->update(TBL_USERS, $new_data, $where);
                $user_inserted_id = $id;
            } else {
                $user_inserted_id = $this->BM->insert(TBL_USERS, $new_data);
            }
            $checking_data = array(
                'bill_no'=>$this->input->post('bill_no'),
                'user_id' => $user_inserted_id,
                'room_id' => $this->input->post('room_id'),
                'room_category_id' => $this->input->post('room_type_id'),
                'agent_id' => $this->input->post('agent_id') ? $this->input->post('agent_id') : 0,
                'tour_id' => $this->input->post('tour_id') ? $this->input->post('tour_id') : 0,
                'checkin_date' => $this->input->post('checking_date'),
                'checkout_date' => $this->input->post('checkout_date'),
                'type'=>DEBIT,
            );
            $checking_inserted_id = $this->BM->insert(TBL_TRANSACTIONS, $checking_data);
            if (isset($_POST['user_pax']) && !empty($_POST['user_pax'])) {
                foreach ($_POST['user_pax'] as $single_pax_arr) {
                    $pax_insert_arr = array(
                        'user_id' => $id,
                        'first_name' => $single_pax_arr['first_name'],
                        'last_name' => $single_pax_arr['last_name'],
                        'gender' => $single_pax_arr['gender'],
                        'age' => $single_pax_arr['age'],
                        'type'=>DEBIT,
                        'transaction_id'=>$this->input->post('bill_no'),
                    );
                    $this->BM->insert(TBL_USER_PAX, $pax_insert_arr);
                }
            }
            $transaction_array = array();
            $extra_bed_lbl = $this->input->post('extra_bed') . ' Extra Bed Rent';
            $transaction_array[EXTRA_BED_PRIORITY] = array($extra_bed_lbl => $this->input->post('extra_bed_rent'));
            $transaction_array[DISCOUNT_PRIORITY] = array('Discount Per Day' => $this->input->post('discount_per_day'));
            $transaction_array[NET_RENT_PRIORITY] = array('Net Rent' => $this->input->post('net_amt'));
            $transaction_array[ADVANCE_PRIORITY] = array('Advance' => $this->input->post('advance'));
            
            if ($transaction_array) {
                foreach ($transaction_array as $priority=>$transaction_single_arr) {
                    if($priority==ADVANCE_PRIORITY){
                        $type=CREDIT;
                    }
                    else
                    {
                        $type=DEBIT;
                    }
                    foreach ($transaction_single_arr as $details => $single_entry) {
                        $transaction_insert_arr = array(
                            'transaction_id' => $this->input->post('bill_no'),
                            'price' => $single_entry,
                            'description' => $details,
                            'create_transaction_by' => $logged_user['id'],
                            'type' => $type,
                            'room_id' => $this->input->post('room_id'),
                            'priority'=>$priority,
                        );
                        $this->BM->insert(TBL_TRANSACTIONS_MANAGEMENT, $transaction_insert_arr);
                    }
                }
            }
            if ($this->input->post('advance') > 0) {
                
                $payments_data = array(
                    'transaction_id'=>$this->input->post('bill_no'),
                    'amount' => $this->input->post('advance'),
                    'description' => 'Advance amount of SR. NO.' . $this->input->post('bill_no'),
                    'type' => CREDIT,
                    'created_transaction_by' => $logged_user['id'],
                );
                $this->BM->insert(TBL_PAYMENTS, $payments_data);
            }


            if ($this->input->post('room_id')) {
                $new_data_arr['availability_status'] = ROOM_OCCUPIED;
                $room_where_arr = array('id' => $this->input->post('room_id'));
                $affected_records = $this->BM->update(TBL_ROOMS, $new_data_arr, $room_where_arr);
            }
        }
        $url = base_url('checking');
        redirect($url);
    }

    function _get_data_by_mobile($id) {
        $condition = array(
            'table' => TBL_USERS,
            'where' => array(
                'mobile_number' => $id
            ),
        );
        return $this->BM->get_one($condition);
    }

}
