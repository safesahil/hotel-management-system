<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        if ($this->session->userdata('is_logged_in')) {
            $url = base_url('dashboard');
            redirect($url);
        }
        if ($this->input->post()) {
            $validate_fields = array(
                'email_id',
                'password',
                'authenticate'
            );
            $meta = array(
                'password' => $this->input->post('password'),
            );
            if ($this->_validate_login_form($validate_fields, $meta)) {
                $select_cond = array(
                    'table' => TBL_USERS,
                    'fields' => array(
                        'id', 'first_name', 'last_name', 'email', 'mobile_number', 'address', 'proof', 'proof_relative', 'capture_image', 'role'
                    ),
                    'where' => array(
                        'email' => $this->input->post('email_id'),
                        'password' => md5($this->input->post('password')),
                        'status' => ENABLE,
                        'is_deleted' => DISABLE,
                        'role' => ADMINS,
                    ),
                );
                $user = $this->BM->get_one($select_cond);
                $arr = array(ADMINS_STR_KEY => $user, 'is_logged_in' => TRUE);
                $this->session->set_userdata($arr);
                $url = base_url('dashboard');
                redirect($url);
            }
        }
        $this->load->view('login/index');
    }

    public function logout() {
        $this->session->unset_userdata('is_logged_in');
        $url = base_url();
        redirect($url);
    }

    function _validate_login_form($validate_fields, $meta) {
        $validation_rules = array();
        if (in_array('email_id', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'email_id',
                'label' => 'Email',
                'rules' => 'trim|required|min_length[5]|max_length[32]'
            );
        }
        if (in_array('password', $validate_fields)) {
            $validation_rules[] = array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required|min_length[8]|max_length[32]'
            );
        }
        if (in_array('authenticate', $validate_fields)) {
            $password = $meta['password'];
            $validation_rules[] = array(
                'field' => 'email_id',
                'label' => 'Username',
                'rules' => 'callback__check_authenticate[' . $password . ']'
            );
        }
        $this->form_validation->set_rules($validation_rules);
        return $this->form_validation->run();
    }

    function _check_authenticate($email_id, $password) {
        if ($email_id && $password) {
            $select_cond = array(
                'table' => TBL_USERS,
                'fields' => array('id'),
                'where' => array(
                    'email' => $email_id,
                    'password' => md5($password),
                    'status' => ENABLE,
                    'is_deleted' => DISABLE,
                    'role' => ADMINS,
                ),
            );
            $count = $this->BM->count($select_cond);
            if ($count == 1) {
                return TRUE;
            }
            $this->form_validation->set_message('_check_authenticate', 'Invalid email or password.');
            return FALSE;
        }
    }

}
