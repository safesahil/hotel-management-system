<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['title'] = $this->data['page_header'] = 'Orders';
        $this->data['page_title'] = 'Recent Orders';
        $this->admin_template->load('admin', 'orders/index', $this->data);
    }

    public function save($id = false) {
        $this->data['title'] = $this->data['page_header'] = 'Orders';
        $this->data['page_title'] = 'Add Orders';
        $fields = array(
            'id', 'name'
        );
        $items_categories = $this->_get_records(TBL_RESTAURANTS_ITEMS_CATEGORY, $fields);
        $items = $this->_get_records(TBL_RESTAURANTS_ITEMS, $fields);

        $this->data['order_categories'] = prepare_data_for_dropdown($items_categories, 'name', 'id', true, 'Select category', '');

        $this->data['order_item'] = prepare_data_for_dropdown($items, 'name', 'id', true, 'Select item', '');

        if ($id) {
            $this->data['orders'] = $this->_get_order_records($id);
            $this->data['edit_id'] = $id;
        }
        $this->admin_template->load('admin', 'orders/save', $this->data);
    }

    function _get_order_records($id = false) {
        $condition = array(
            'table' => TBL_ORDERS,
            'fields' => array(
                TBL_ORDER_MASTER . '.customer_name', TBL_ORDERS . '.order_id', TBL_ORDERS . '.category_id', TBL_ORDERS . '.item_id', TBL_RESTAURANTS_ITEMS_CATEGORY . '.name as category_name', TBL_RESTAURANTS_ITEMS . '.name as item_name', TBL_ORDERS . '.qty', TBL_ORDERS . '.status', TBL_ORDERS . '.price', TBL_ORDERS . '.created'
            ),
            'join' => array(
                array(
                    'join_type' => 'left',
                    'table' => TBL_RESTAURANTS_ITEMS_CATEGORY,
                    'condition' => TBL_RESTAURANTS_ITEMS_CATEGORY . '.id = ' . TBL_ORDERS . '.category_id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_ORDER_MASTER,
                    'condition' => TBL_ORDER_MASTER . '.id = ' . TBL_ORDERS . '.order_id'
                ),
                array(
                    'join_type' => 'left',
                    'table' => TBL_RESTAURANTS_ITEMS,
                    'condition' => TBL_RESTAURANTS_ITEMS . '.id = ' . TBL_ORDERS . '.item_id'
                ),
            ),
            'where' => array(
                TBL_ORDERS . '.order_id' => $id
            ),
        );
        return $this->BM->get($condition);
    }

    function _get_records($table_name, $fields) {
        $condition = array(
            'table' => $table_name,
            'fields' => $fields,
        );
        return $this->BM->get($condition);
    }

    function _get_records_bill($table_name, $fields) {
        $condition = array(
            'table' => $table_name,
            'fields' => $fields,
            'where' => array('type' => 1),
        );
        return $this->BM->get($condition);
    }

    function _get_data_by_id($field, $id, $table_name, $select_field, $single = false) {
        $condition = array(
            'table' => $table_name,
            'fields' => $select_field,
            'where' => array(
                $field => $id
            ),
        );
        if ($single) {
            return $this->BM->get_one($condition);
        } else {
            return $this->BM->get($condition);
        }
    }

    function get_details() {
        if (isset($_POST['category_id']) && $_POST['category_id']) {
            $select_field = array(
                'id', 'name',
            );
            $records = $this->_get_data_by_id('category_id', $_POST['category_id'], TBL_RESTAURANTS_ITEMS, $select_field);

            if ($records) {
                $return = '<option value="">Select item</option>';
                foreach ($records as $single_rec) {
                    $return .= '<option value="' . $single_rec["id"] . '">' . $single_rec["name"] . '</option>';
                }
            }
            echo json_encode($return);
        }
    }

    function get_order_price() {
        if (isset($_POST['item_id']) && $_POST['item_id']) {
            $select_field = array(
                'sale_price',
            );
            $records = $this->_get_data_by_id('id', $_POST['item_id'], TBL_RESTAURANTS_ITEMS, $select_field, true);
            if ($records) {
                echo json_encode($records['sale_price']);
            } else {
                echo '';
            }
        }
    }

    function save_order($edit_id = false) {
        $order_details = isset($_POST['order_details']) ? $_POST['order_details'] : array();
        $customer_name = isset($_POST['customer_name']) ? $_POST['customer_name'] : array();
        $master_tbl_insert = array('customer_name' => $customer_name);
        if ($edit_id) {
            $order_id = $edit_id;
        } else {
            $order_id = $this->BM->insert(TBL_ORDER_MASTER, $master_tbl_insert);
        }
        $child_insert_array = array();
        if ($order_details) {
            $child_insert_array = array();
            foreach ($order_details as $parent_key => $single_array) {
                foreach ($single_array as $child_key => $single_val) {
                    $child_insert_array[$child_key][$parent_key] = $single_val;
                    $child_insert_array[$child_key]['order_id'] = $order_id;
                }
            }
        }
        if ($child_insert_array) {
            if ($edit_id) {
                $this->BM->delete(TBL_ORDERS, array('order_id' => $edit_id));
            }
            foreach ($child_insert_array as $single_rec) {
                $this->BM->insert(TBL_ORDERS, $single_rec);
            }
        }
        redirect('orders');
    }

    function filter() {
        $filter_array = create_datatable_request($this->input->post());
        $filter_array['order'][TBL_ORDER_MASTER . '.created'] = 'desc';
//        $filter_array['custom_or_where'] = array(TBL_ORDER_MASTER . '.type' => array(1));
        $filter_array['join'] = array(
            array(
                'join_type' => 'right',
                'table' => TBL_ORDERS,
                'condition' => TBL_ORDERS . '.order_id = ' . TBL_ORDER_MASTER . '.id'
            )
        );
        $filter_array['group_by'][] = TBL_ORDERS . '.order_id';
//        $filter_array['fields'][] = 'SUM('.TBL_ORDERS.'.price) as orders_price_rec';
        $filter_records = $this->BM->get_filtered_records(TBL_ORDER_MASTER, $filter_array);
        $total_filter_records = $this->BM->get_filtered_records(TBL_ORDER_MASTER, $filter_array, 1);

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->BM->count(array('table' => TBL_ORDER_MASTER)),
            "recordsFiltered" => $total_filter_records,
            "data" => $filter_records,
        );

        echo json_encode($output);
    }

    function print_bill() {
        $this->data['page_title'] = 'Print bill';
        $this->data['title'] = $this->data['page_header'] = 'Print bill';
        $fields = array('id');
        $prev_orders = $this->_get_records_bill(TBL_ORDER_MASTER, $fields);
        $this->data['orders'] = prepare_data_for_dropdown($prev_orders, 'id', 'id', true, 'Select Order', '');
        $this->admin_template->load('admin', 'orders/print_bill', $this->data);
    }

    function get_order_details() {
        if (isset($_POST['order_id']) && !empty($_POST['order_id'])) {
            $records = $this->_get_order_records($_POST['order_id']);
            $return_html = '';
            if ($records) {
                if (isset($records) && !empty($records)) {
                    $i = 1;
                    $price_arr = array();
                    $print_price = $return_html_price = 0;
                    foreach ($records as $order) {
                        $price_arr[] = $order['price'];
                        $return_html.=' <tr>
                            <td>
                              ' . $i++ . '
                            </td>
                            <td>
                               ' . $order['item_name'] . '
                            </td>
                            <td>
                                ' . $order['qty'] . '
                            </td>
                            <td>
                            ' . $order['price'] . '
                            </td>
                            </tr>';
                        ?>

                        <?php

                    }
                    if ($price_arr) {
                        $print_price = array_sum($price_arr);
                    }
                    $return_html_price = '<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><input type="hidden" value="' . $print_price . '" name="total_amount"> <label class="total_print "><label class="total_amount ">' . $print_price . '</label></label></td></tr>';
                }
            }
            echo json_encode(array('return_html' => $return_html, 'return_html_price' => $return_html_price));
        }
    }

    function print_bill_save() {
        if (isset($_POST['order_id']) && !empty($_POST['order_id'])) {
            $logged_user = $this->session->userdata(ADMINS_STR_KEY);
            $order_id = $_POST['order_id'];
            $total_amount = $_POST['total_amount'];
            $payments_data = array(
                'amount' => $total_amount,
                'description' => 'Restaurant Bill Payment of  order no. ' . $order_id,
                'type' => CREDIT,
                'created_transaction_by' => $logged_user['id'],
            );
            $this->BM->insert(TBL_PAYMENTS, $payments_data);

            $new_data['type'] = 0;
            $where = array('id' => $order_id);
            $this->BM->update(TBL_ORDER_MASTER, $new_data, $where);
        }
        redirect('orders/print_bill');
    }

}
