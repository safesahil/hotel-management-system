<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
// <editor-fold defaultstate="collapsed" desc="table names">
define('TBL_USERS', 'users');
define('TBL_ROOMS_CATEGORY', 'rooms_category');
define('TBL_ROOMS', 'rooms');
define('TBL_BANK', 'bank');
define('TBL_PAYMENTS', 'payments');
define('TBL_ITEMS_CATEGORY', 'items_category');
define('TBL_ITEMS', 'items');
define('TBL_ORDERS', 'orders');
define('TBL_ORDER_MASTER', 'order_master');
define('TBL_ITEMS_INVENTORY', 'items_inventory');
define('TBL_RESTAURANTS_ITEMS', 'restaurants_items');
define('TBL_RESTAURANTS_ITEMS_CATEGORY', 'restaurants_items_category');
define('TBL_TRANSACTIONS', 'transactions');
define('TBL_TRANSACTIONS_MANAGEMENT', 'transaction_management');
define('TBL_USER_PAX', 'user_pax');

// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="User Roles">
define('CUSTOMERS', 1);
define('AGENTS', 2);
define('TOURS', 3);
define('ADMINS', 99);
// </editor-fold>


/**
 * Extra Helper Constants
 */
define('IS_DELETED_YES', '1');
define('IS_DELETED_NO', '0');
define('ENABLE', '1');
define('ENABLE_STR', 'Enable');
define('DISABLE', '0');
define('DISABLE_STR', 'Disable');
define('ROOM_OCCUPIED', '0');
define('ROOM_AVAILABLE', '1');
define('ROOM_OCCUPIED_STR', 'Occupied');
define('ROOM_AVAILABLE_STR', 'Available');
define('DB_DATE_FORMAT', 'Y-m-d h:i:a');
define('PROJECT_FOLDER', 'hotel-management');
$project_root=$_SERVER['SCRIPT_FILENAME'];
$project_root= str_replace('/index.php', '', $project_root);
define('PROJECT_ROOT', $project_root);
define('UPLOADS_FOLDER', 'uploads');
define('MIN_RANDOM_NUMBER', 0);
define('MAX_RANDOM_NUMBER', 999999999);
define('EXTRA_BED_PRIORITY',1);
define('DISCOUNT_PRIORITY',2);
define('NET_RENT_PRIORITY',3);
define('ADVANCE_PRIORITY',4);
define("INCOME",3);
define("EXPENCE",4);


define('MEASUREMENT_UNITS_CATEGORY_MASS', 'mass');
define('MEASUREMENT_UNITS_CATEGORY_VOLUME', 'volume');
define('MEASUREMENT_UNITS_CATEGORY_COUNTABLE', 'qty');
define('MEASUREMENT_UNITS_CATEGORY_MASS_STR', 'Mass');
define('MEASUREMENT_UNITS_CATEGORY_VOLUME_STR', 'Volume');
define('MEASUREMENT_UNITS_CATEGORY_COUNTABLE_STR', 'Qty');
define('MEASUREMENT_UNITS_CATEGORIES', json_encode(array(
    MEASUREMENT_UNITS_CATEGORY_MASS => MEASUREMENT_UNITS_CATEGORY_MASS_STR,
    MEASUREMENT_UNITS_CATEGORY_VOLUME => MEASUREMENT_UNITS_CATEGORY_VOLUME_STR,
    MEASUREMENT_UNITS_CATEGORY_COUNTABLE => MEASUREMENT_UNITS_CATEGORY_COUNTABLE_STR
)));

define('MEASUREMENT_UNIT_KG', 'kg');
define('MEASUREMENT_UNIT_LB', 'lb');
define('MEASUREMENT_UNIT_G', 'g');
define('MEASUREMENT_UNIT_LT', 'lt');
define('MEASUREMENT_UNIT_ML', 'ml');
define('MEASUREMENT_UNIT_COUNT', 'qty');
define('MEASUREMENT_UNIT_KG_STR', 'Kilogram');
define('MEASUREMENT_UNIT_LB_STR', 'Pound');
define('MEASUREMENT_UNIT_G_STR', 'Gram');
define('MEASUREMENT_UNIT_LT_STR', 'Litre');
define('MEASUREMENT_UNIT_ML_STR', 'Mililitre');
define('MEASUREMENT_UNIT_COUNT_STR', 'Qty');
define('MEASUREMENT_UNITS', json_encode(array(
    MEASUREMENT_UNITS_CATEGORY_MASS => array(
        MEASUREMENT_UNIT_KG => MEASUREMENT_UNIT_KG_STR,
        MEASUREMENT_UNIT_LB => MEASUREMENT_UNIT_LB_STR,
        MEASUREMENT_UNIT_G => MEASUREMENT_UNIT_G_STR,
    ),
    MEASUREMENT_UNITS_CATEGORY_VOLUME => array(
        MEASUREMENT_UNIT_LT => MEASUREMENT_UNIT_LT_STR,
        MEASUREMENT_UNIT_ML => MEASUREMENT_UNIT_ML_STR
    ),
    MEASUREMENT_UNITS_CATEGORY_COUNTABLE => array(
        MEASUREMENT_UNIT_COUNT => MEASUREMENT_UNIT_COUNT_STR
    )
)));

define('INVENTORY_TYPE_IN', 1);
define('INVENTORY_TYPE_OUT', 2);
define('INVENTORY_TYPE_MINI_BAR_OUT', 3);
define('INVENTORY_TYPE_RETURN', 4);
define('INVENTORY_MANAGED_FOR_GENERAL', 1);
define('INVENTORY_MANAGED_FOR_MINI_BAR', 2);

define('DEBIT', 0);
define('CREDIT', 1);
define('DEBIT_STR', 'Dr');
define('CREDIT_STR', 'Cr');

define('PAYMENT_MANAGED_FOR_GENERAL', 0);
define('PAYMENT_MANAGED_FOR_STORE', 1);
define('PAYMENT_MANAGED_FOR_STORE_EXPENSE', 2);
define('PAYMENT_MANAGED_FOR_STORE_RETURN', 3);

/*
 * Constance for Salutation
 */

define('MR', 'MR');
define('MRS', 'Mrs');
define('MS', 'Ms');
define('MISS', 'Miss');
define('MALE', 'Male');
define('FEMALE', 'Female');
define('MALE_INT', '1');
define('FEMALE_INT', '2');

define('CUSTOMERS_STR_KEY', 'customers');
define('TOURS_STR_KEY', 'tours');
define('AGENTS_STR_KEY', 'agents');
define('ADMINS_STR_KEY', 'admins');
define('ALL_STR_KEY', 'all');

define('CUSTOMERS_STR_LABEL', 'Customers');
define('TOURS_STR_LABEL', 'Tours');
define('AGENTS_STR_LABEL', 'Agents');
define('ADMINS_STR_LABEL', 'Admins');
define('ALL_STR_LABEL', 'All');

define('PROOF_ID_CARD', 'id_card');
define('PROOF_PASSPORT', 'passport');
define('PROOF_DRIVING_LIC', 'driving_lic');

define('PROOF_ID_CARD_STR', 'ID Card');
define('PROOF_PASSPORT_STR', 'Passport');
define('PROOF_DRIVING_LIC_STR', 'Driving License');

// <editor-fold defaultstate="collapsed" desc="Regex Constants">
// 
define('REGEX_MOBILE_NUMBER', '/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/');
// 
// </editor-fold>
// <editor-fold defaultstate="collapsed" desc="files sizes">
// 
define("MIN_IMAGE_SIZE", 1024);
define("MIN_IMAGE_SIZE_STR", '1 KB');
define("MAX_IMAGE_SIZE", (2 * 1024 * 1024));
define("MAX_IMAGE_SIZE_STR", '2 MB');
// 
// </editor-fold>


