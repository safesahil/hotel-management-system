<?php

function pr($data, $exit = 0) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";
    if ($exit) {
        die();
    }
}

function query($exit = 0) {
    $CI = & get_instance();
    echo $CI->db->last_query();
    if ($exit == 1)
        exit();
}

function create_datatable_request($post_data) {
    $filter_array = array();

    $filter_array['start'] = $post_data['start'];
    $filter_array['length'] = $post_data['length'];
    $filter_array['search'] = $post_data['search']['value'];
    $filter_array['col_eq'] = isset($post_data['col_eq']) ? $post_data['col_eq'] : array();
    $filter_array['extra_fields_select'] = isset($post_data['extra_fields_select']) ? $post_data['extra_fields_select'] : array();
    $datatable_date_range = (isset($post_data['datatable_date_range'])) ? $post_data['datatable_date_range'] : array();


    $column_search = array();
    $columns = array();
    $column_date_ranges = array();
    $filter_array['searchable_columns'] = array();
    $filter_array['select'] = '';

    $posted_column = $post_data['columns'];
    foreach ($posted_column as $col_index => $column) {
        if ($column['search']['value'] != '') {
            $do_include_column_in_search = TRUE;
            if (!empty($datatable_date_range)) {
                foreach ($datatable_date_range as $date_range_data) {
                    if ($date_range_data['column'] == $column['name']) {
                        $do_include_column_in_search = FALSE;
                        $explode_dates = explode($date_range_data['range_deliminator'], $column['search']['value']);
                        if (isset($explode_dates[0]) && isset($explode_dates[1])) {
                            $column_date_ranges[] = array(
                                'column' => $date_range_data['column'],
                                'start_date' => date($date_range_data['filter_format'], strtotime(trim($explode_dates[0]))),
                                'end_date' => date($date_range_data['filter_format'], strtotime(trim($explode_dates[1]))),
                            );
                        }
                    }
                }
            }
            if ($do_include_column_in_search) {
                $column_search[$column['name']] = str_replace('\\', '', $column['search']['value']);
            }
        }

        if ($column['searchable'] == 'true') {
            if (strpos($column['name'], '|') > -1) {
                $cols = explode('|', $column['name']);
                foreach ($cols as $col) {
                    $filter_array['searchable_columns'][] = $col;
                }
            } else {
                $filter_array['searchable_columns'][] = $column['name'];
            }
        }

        if ($column['name'] != '') {
            if (strpos($column['name'], '|') > -1) {
                $cols = explode('|', $column['name']);
                $cols_str = implode(",' ',", $cols);
                $columns[] = 'CONCAT(' . $cols_str . ')' . " as '" . str_replace('.', "_", $column['name']) . "'";
            } else {
                $columns[] = $column['name'] . " as '" . str_replace('.', "_", $column['name']) . "'";
            }
        }
    }

    if (isset($filter_array['extra_fields_select']) && !empty($filter_array['extra_fields_select'])) {
        foreach ($filter_array['extra_fields_select'] as $column) {
            $columns[] = $column . " as '" . str_replace('.', "_", $column) . "'";
        }
    }

    $filter_array['select'] = implode(',', $columns);

    if (isset($post_data['order'])) {
        $orders = $post_data['order'];
        foreach ($orders as $col_index => $order) {
            $filter_array['order'][$posted_column[$order['column']]['name']] = $order['dir'];
        }
    }

    $filter_array['column_search'] = $column_search;
    $filter_array['column_date_ranges'] = $column_date_ranges;

    return $filter_array;
}

function prepare_data_for_dropdown($data, $label_key, $value_key, $add_default = false, $default_label = 'Select', $default_value = '') {
    $result = array();
    if ($add_default) {
        $result[$default_value] = $default_label;
    }
    if (isset($data) && count($data) > 0) {
        foreach ($data as $v) {
            $result[$v[$value_key]] = $v[$label_key];
        }
    }
    return $result;
}

function define_chk($name, $value) {
    if (!defined($name)) {
        define($name, $value);
    }
}

function get_base_measurement_unit($unit) {
    if ($unit === MEASUREMENT_UNIT_KG || $unit === MEASUREMENT_UNIT_LB || $unit === MEASUREMENT_UNIT_G) {
        return MEASUREMENT_UNIT_G;
    } else if ($unit === MEASUREMENT_UNIT_LT || $unit === MEASUREMENT_UNIT_ML) {
        return MEASUREMENT_UNIT_ML;
    } else if ($unit === MEASUREMENT_UNIT_COUNT) {
        return MEASUREMENT_UNIT_COUNT;
    }
    return $unit;
}

function get_base_measurement_unit_by_category($category) {
    if ($category === MEASUREMENT_UNITS_CATEGORY_MASS) {
        return MEASUREMENT_UNIT_G;
    } else if ($category === MEASUREMENT_UNITS_CATEGORY_VOLUME) {
        return MEASUREMENT_UNIT_ML;
    } else if ($category === MEASUREMENT_UNITS_CATEGORY_COUNTABLE) {
        return MEASUREMENT_UNIT_COUNT;
    }
    return $category;
}

function get_default_measurement_unit_by_category($category) {
    if ($category === MEASUREMENT_UNITS_CATEGORY_MASS) {
        return MEASUREMENT_UNIT_KG;
    } else if ($category === MEASUREMENT_UNITS_CATEGORY_VOLUME) {
        return MEASUREMENT_UNIT_LT;
    } else if ($category === MEASUREMENT_UNITS_CATEGORY_COUNTABLE) {
        return MEASUREMENT_UNIT_COUNT;
    }
    return $category;
}

function convert_measurement_units($from, $to, $value, $show_deci = false, $deci = 2) {
    $result = $value;
    switch ($from) {
        case MEASUREMENT_UNIT_KG:
            switch ($to) {
                case MEASUREMENT_UNIT_LB:
                    $result = ($value * 2.20462);
                    break;
                case MEASUREMENT_UNIT_G:
                    $result = ($value * 1000);
                    break;
                default:
                    $result = $value;
                    break;
            }
            break;
        case MEASUREMENT_UNIT_LB:
            switch ($to) {
                case MEASUREMENT_UNIT_KG:
                    $result = ($value * 0.453592);
                    break;
                case MEASUREMENT_UNIT_G:
                    $result = ($value * 453.592);
                    break;
                default:
                    $result = $value;
                    break;
            }
            break;
        case MEASUREMENT_UNIT_G:
            switch ($to) {
                case MEASUREMENT_UNIT_KG:
                    $result = ($value * 0.001);
                    break;
                case MEASUREMENT_UNIT_LB:
                    $result = ($value * 0.00220462);
                    break;
                default:
                    $result = $value;
                    break;
            }
            break;
        case MEASUREMENT_UNIT_LT:
            switch ($to) {
                case MEASUREMENT_UNIT_ML:
                    $result = ($value * 1000);
                    break;
                default:
                    $result = $value;
                    break;
            }
            break;
        case MEASUREMENT_UNIT_ML:
            switch ($to) {
                case MEASUREMENT_UNIT_LT:
                    $result = ($value * 0.001);
                    break;
                default:
                    $result = $value;
                    break;
            }
            break;
        case MEASUREMENT_UNIT_COUNT:
            $result = $value;
            break;
        default:
            $result = $value;
            break;
    }
    if ($show_deci) {
        $result = number_format((float) $result, $deci, '.', '');
    }
    return $result;
}

function generate_random_string() {
    $time = time();
    $random_number = mt_rand(MIN_RANDOM_NUMBER, MAX_RANDOM_NUMBER);
    return $time . $random_number;
}

function upload_files($files_obj, $field_name, $upload_folder) {
    $response = [];
    if (isset($files_obj) && isset($files_obj[$field_name])) {
        $dest_folder = PROJECT_ROOT . '/' . UPLOADS_FOLDER . '/' . $upload_folder;
        if (!file_exists($dest_folder)) {
            mkdir($dest_folder, 0777);
        }
        $new_files_arr = [];
        if (is_array($files_obj[$field_name]['name'])) {
            foreach ($files_obj[$field_name]['name'] as $i => $val) {
                $new_files_arr[] = array(
                    'name' => $files_obj[$field_name]['name'][$i],
                    'type' => $files_obj[$field_name]['type'][$i],
                    'tmp_name' => $files_obj[$field_name]['tmp_name'][$i],
                    'error' => $files_obj[$field_name]['error'][$i],
                    'size' => $files_obj[$field_name]['size'][$i]
                );
            }
        } else {
            $new_files_arr[] = $files_obj[$field_name];
        }
        foreach ($new_files_arr as $key => $value) {
            if (isset($value['tmp_name'])) {
                $new_file_name = generate_random_string() . $value['name'];
                $destination_relative = '/' . $upload_folder . '/' . $new_file_name;
                $destination = base_url('uploads') . $destination_relative;
                $upload_path = $dest_folder . '/' . $new_file_name;
                if (move_uploaded_file($value['tmp_name'], $upload_path)) {
                    $response[] = array(
                        'status' => 1,
                        'message' => 'Success',
                        'filename' => $new_file_name,
                        'destination_folder' => $dest_folder,
                        'destination' => $destination,
                        'destination_relative' => $destination_relative,
                    );
                } else {
                    $response[] = array(
                        'status' => 0,
                        'message' => 'Fail to upload',
                        'filename' => '',
                        'destination_folder' => '',
                        'destination' => '',
                        'destination_relative' => '',
                    );
                }
            }
        }
    }
    return $response;
}

function delete_upload_files($files) {
    $response = [];
    foreach ($files as $value) {
        $dest_folder = PROJECT_ROOT . '/' . UPLOADS_FOLDER . '/' . $value;
        if (unlink($dest_folder)) {
            $response[] = array(
                'status' => 1,
                'message' => 'Success'
            );
        } else {
            $response[] = array(
                'status' => 0,
                'message' => 'Fail to delete'
            );
        }
    }
    return $response;
}

?>